\begin{proof}
  Let $\idp_1\sse \idp_2\sse R$ and $\idq_2\sse R'$ be prime ideals such that $\idp_2 = \idq_2\cap R$. We want to find a prime ideal $\idq_1\in R'$ such that $\idp_1 = \idq_1\cap R$ and $\idq_1\sse \idq_2$:
  \[
  \begin{tikzcd}[column sep = small]
    R' &
    \exists \idq_1 \ar[dashed,mapsto]{d}& \sse &
    \idq_2\ar[mapsto]{d}\\
    R\ar[hookrightarrow]{u}[left]{\pphi}&
    \idp_1 &
    \sse &
    \idp_2
  \end{tikzcd}
  \]
  Consider now the map
  \[
  \begin{tikzcd}
    \pphi':R\ar[hookrightarrow]{r}[above]{\pphi}&
    R'\ar[hookrightarrow]{r}[above]{\eta}&
    R'_{\idq_2}
  \end{tikzcd}.
  \]
  We are going to show that under $\pphi'$,
  \begin{equation}\label{lec9:going-down-proof-condition}\tag{$\ast$}
    \left(
    \idp_1R_{\idq_2}'\right)\cap R = \idp_1
  \end{equation}
  holds. Assume for now that this is the case. Then \cref{lec8:prime-ideal-existence-preimage} implies that there is a prime ideal $\idq_1'\sse R_{\idq_2}'$ such that $\idq_1'\cap R = \idp_1$.
  For $\idq_1\defined \idq_1'\cap R'$ we have that $\idq_1\cap R = \idp_1$, and $\idq_1\sse \idq_2$ (by \cref{lec3:rp-is-local-ring}, i)), which shows that $\pphi$ satisfies going down.\par
  We now prove \eqref{lec9:going-down-proof-condition}: Note first that $\idp_1 \sse \left(\idp_1 R_{\idq_2}'\right) \cap R$ is always true. So it suffices to show $\left(\idp_1 R_{\idq_2}'\right) \cap R\sse \idp_1$. We will do this by contradiction:\par
  Let $x\in \left(\idp_1 R_{\idq_2}'\right) \cap R$ be any element and consider $\pphi'(x)\in \idp_1R_{\idq_2}'$, which we will identify with $x$.
  As $\idp_1 R_{\idq_2}' = \left(\idp_1R'\right)R_{\idq_2}'$ there are $y\in \idp_1R'$ and $s\in R'\setminus \idq_2$ such that $x=y/s$ (\cref{3: ideals under localization}, i)).
  Now by \cref{lec8:radical-of-integral-closure}, $y$ is integral over $\idp_1$ (we have $y\in \idp_1R' \sse \sqrt{\idp_1 R'} = \sqrt{\idp_1\overline{R}}$, since $R'$ is integral over $R$.).
  Let $p_y\in \left(\quot\left(R\right)\right)[\polvariable]$ be the minimal polynomial of $y$ over $K\defined \quot(R)$ (c.f. \cref{lec8:integral-induce-algebraic-extension}).
  By \cref{lec8:coefficients-of-minimal-polynomial}, the non-leading coefficients of $p_y$, say $a_0,\hdots,a_{n-1}$, are in $\sqrt{\idp_1} = \idp_1$ (since $\idp_1$ is a prime ideal).\par
  Assume now that $x\notin \idp_1$, so in particular $x\neq 0$ and hence $s$ has the form $y/x\in \quot R'$, and $1/x\in K$. This implies that the minimal polynomial $p_s$ of $s$ over $k$ has the form
  \[
  \polvariable^n+\frac{a_{n-1}}{\polvariable}\polvariable^{n-1}+\hdots + \frac{a_0}{x^n}.
  \]
  Set $\tilde{a}_i\defined a_i/x^{n-i}$ for $(i=0,\hdots,n-1)$. Since $s\in R'$ is integral over $R$ (by assumption on $\pphi$), we have by \cref{lec8:coefficients-of-minimal-polynomial} that the $\tilde{a}_i$ are already in $R$. \par
  Now in $R$, we have $\tilde{a}_ix^{n-1} = a_i\in \idp_1$, and since we assumed $x\notin \idp_1$, this implies $\tilde{a}_i\in\idp_1$ for all $i=0,\hdots,n-1$. But then
  \[
  s^n = -\left(\tilde{a}_{n-1}s^{n-1}+\hdots+\tilde{a}_0\right)\in \idp_1R\sse \idp_2R'\idq_2,
  \]
  contradicting $s\notin \idq_2$.
\end{proof}
\subsection*{More Examples of Going Down}
The proofs for the following propositions will (hopefully) be added in the future.
\begin{mtheorem}
  Let $\pphi:R\to R'$ be flat, i.e. $R'$ is flat as an $R$-module. Then $\pphi$ satisfies going down.
\end{mtheorem}
\begin{mtheorem}
  Let $\pphi:R\to R'$ be a ring homomorphism such that $\spec \pphi:\spec R'\to \spec R$ is open (i.e. maps open sets to open sets.). Then $\pphi$ satisfies going down.
\end{mtheorem}
\section{Noether Normalization Lemma}
\begin{theorem}[Noether Normalization Lemma (NNL)]\label{lec9:nnl}
  Let $k$ be a field, and $A$ a finitely generated $k$-algebra. Let
  \[
  I_1\sse I_2\sse \hdots \sse I_m \ssne A
  \]
  be a chain of ideals in $A$.
  Then there is a $n\geq 0$, $a_1,\hdots,a_n\in A$ and $0\leq h_1\leq \hdots\leq h_m\leq n$ such that:
  \begin{enumerate}
    \item the elements $a_1,\hdots,a_n$ are algebraically independent over $k$;
    \item $k[a_1,\hdots,a_n]\sse A$ is a finite ring extension; and
    \item $I_l \cap k[a_1,\hdots,a_n] = \left(a_1,\hdots,a_{h_l}\right)$.
  \end{enumerate}
\end{theorem}
We will only prove i) and ii). The proof of iii) can be found in \cite{franzen}.
\begin{lem}\label{lec9:substitution-lemma}
  Let $k$ be a field, and $0\neq f\in k[\polvariable_1,\hdots,\polvariable_n]$ a non-zero polynomial. Then there are $r_1,\hdots,r_{n-1}\in \nn$ such that after the substitution $\polvariable_i\defined Y_i+\polvariable_n^{r_i}$ ($1\leq i\leq n-1$), the polynomial $f$ has the form
  \[
  f = c \polvariable_n^m + h_1 \polvariable_n^{m-1}+\hdots+ h_m \in k[Y_1,\hdots,Y_{n-1},t_n],
  \]
  for a $m >0$, $c\in k\unts$ and $h_1,\hdots,h_m\in k[Y_1,\hdots,Y_{n-1}]$.
\end{lem}
\begin{proof}
Assume $f$ has the form
\[
  f = \sum_{\sigma \in \nn^n}b_{\sigma}\polvariable_1^{\sigma_1}\hdots \polvariable_n^{\sigma_n}.
\]
After substituting $\polvariable_i\defined Y_i + \polvariable_n^{r_i}$ for (yet to be determined) $r_i \geq 0$, this becomes
\[
f = \sum_{\sigma \in \nn^n} b_{\sigma}\left(Y_1+\polvariable_n^{r_1}\right)^{\sigma_1}\hdots
 \left(Y_{n-1}+\polvariable_{n}^{r_{n-1}}\right)^{\sigma_{n-1}}\polvariable_n^{\sigma_n}.
\]
Let now $\tau \in \nn^n$ be a specific multi-index. Define
\[
  e\left(\tau\right) \defined \tau_1r_1+\hdots +\tau_{n-1}r_{n-1}+\tau_n
\]
 With this notation, we obtain a factorisation of the $\tau$-summand of $f$ which reads as follows:
\begin{align*}
b_{\tau}\polvariable_n^{\tau_n}\prod_{i=1}^{n-1}
 \left(Y_i+\polvariable_n^{r_i}\right)^{\tau_i}
 &= b_{\tau}\polvariable_{n}^{e(\tau)} +\\& \left(\text{terms where }\polvariable_n\text{ has stricly lower degrees}\right).
\end{align*}

\begin{claim}
  The $r_i$ can be choosen in such a way that for each different pair of multi-indices $\sigma,\tau \in \nn^n$, the associated exponents $e(\sigma),e(\tau)$ are different too:
\end{claim}
\begin{claimproof}
  By definition, there is a $M>0$ such that $b_{\sigma} = 0$ for all multi-indices $\sigma \in \nn^n\setminus \lset 0,\hdots, M-1\rset$. Set now $r_1\defined M,~r_2\defined M^2,~\hdots,r_{n-1}\defined M^{n-1}$, and let $\sigma\in \nn^n$ be a multi-index with $b_{\sigma}\neq 0$.
  Then the value of
  \[
  e(\sigma) = \sigma_n + \sum_{i=1}^{n-1} \sigma_ir_i = \sigma_n + \sigma_1M+\hdots + \sigma_{n-1}M^{n-1}
  \]
  is uniquely determined by the values of the $\sigma_1,\hdots,\sigma_n$ (since the $M$-adic expansion of a natural number is unique, and $M$ was chosen in such a way that $\sigma_i < M$ for all $1\leq i \leq n$).
\end{claimproof}
Now for such a choice of $r_i$, there is a unique multi-index $\sigma\in \nn^n$ such that the corresponding exponent $e(\sigma)$ is maximal and $b_{\sigma} \neq 0$. After re-grouping the expansion of $f$ in decreasing order of powers of $\polvariable_n$, the claim follows with $m\defined e(\sigma)$ and $c\defined b_{\sigma}$.
\end{proof}
\begin{proof}[Proof of NNL (\cref{lec9:nnl})]
Denote by $x_1,\hdots,x_m\in A$ a set of generators of $A$.
We want to show that there are algebraically independent $a_1,\hdots,a_n$ such that the ring homomorphism $k[a_1,\hdots,a_n]\to A$ is injective and $A$ is a finitely-generated $k[a_1,\hdots,a_n]$-module.
We will do this by induction on the number $m$ of generators:\par
The case $m=0$ is trivial.
So assume NNL holds for $m-1$ generatos.
If the $x_1,\hdots,x_m$ are algebraically independent then the canonical map $k[x_1,\hdots,x_m]\to A$ is indeed injective, and $A$ is a finitely generated $k[x_1,\hdots,x_m]$-module.\par
If, however, the $x_i$ are not algebraically independent, then there is a polynomial $0\neq f\in k[\polvariable_1,\hdots,\polvariable_m]$ such that $f(x_1,\hdots,x_m) = 0$. Set $y_i \defined x_i - x_m^{r_i}$ for $1\leq i \leq m-1$ and (yet to be determined) $r_i$. We then have
\[
0 = f\left(y_1 +x_m^{r_1},\hdots, y_{m-1} + x_{m-1}^{r_{m-1}}\right).
\]
But by \cref{lec9:substitution-lemma}, there is a set of exponents $r_i$ such that
\[
0 = f(y_1+x_m^{r_1},\hdots, y_{m-1}+x_{m-1}^{r_{m-1}}) = c x_m^d+ h_1x_{m}^{d-1}+\hdots+h_d
\]
with $h_1,\hdots,h_d\in k[y_1,\hdots,y_{m-1}]$ and $c\in k\unts$. So $x_m$ is integral over $k[y_1,\hdots,y_{m-1}]$ and $k[y_1,\hdots,y_{m-1}][x_m]$ is a finite $k[y_1,\hdots,y_{m-1}]$-module (\cref{lec7:finite-alt-char}).
By induction hypothesis, there are algebraically independent $a_1,\hdots,a_n$ such that $k[a_1,\hdots,a_n]\inarr k[y_1,\hdots,y_{m-1}]$ is finite. Hence
\[
\begin{tikzcd}[sep = small]
k[a_1,\hdots,a_n]\ar[hookrightarrow]{r}&
k[y_1,\hdots,y_{m-1}]\ar[hookrightarrow]{r}&
k[y_1,\hdots,y_{m-1}][x_m]=A
\end{tikzcd}\]
is finite (\cref{lec7:composition-of-algebras}), which proves the claim.
\end{proof}
\lec
