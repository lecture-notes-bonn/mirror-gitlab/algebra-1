
\chapter{Modules and Integral Extensions}
\section{Modules - Basics}
\begin{defn}
  An \emph{$R$-module} $M$\index{module} $(M,+,\cdot)$ is an abelian group $(M,+)$ together with a map
  \begin{eqnarray*}
  R\times M&\longrightarrow&M\\
    (a,x)&\longmapsto&ax
  \end{eqnarray*}
  such that
  \begin{enumerate}
    \item $(a+b)x = ax+bx$,
    \item $a(x+y) = ax+ay$,
    \item $a(bx) = (ab)x$,
    \item $1_Rx = x$
  \end{enumerate}
  for all $x,y\in M$ and $a,b\in R$.
\end{defn}
\begin{example}\label{4: modules examples}
  \leavevmode
  \begin{enumerate}
    \item Let $k$ be a field. Then $k$-modules are precisely $k$-vector spaces.
    \item Let $I\sse R$ be an ideal. Then $I$ can be regarded as an $R$-module, since it is closed under addition and multiplication by elements in $R$.
    \item Consider $R=\zz$. Let $G$ be an abelian group. Then $G$ is a $\zz$-module, by setting
    \[
    nx \defined \underbrace{x+\hdots+x}_{n\text{ times}}
    \] and
    $(-1)x\defined -x$.
    \item\label{4:restriction of scalars} Let $\pphi:R\to R'$ be a ring homomorphism and let $M$ be an $R'$-module. Then $M$ can be regarded as an $R$-module, by setting
    \[
    ay\defined \pphi(a)y
    \]
    for all $a\in R$ and $y\in M$. This is called \emph{restriction of scalars}\index{of scalars! restriction}\index{restriction!of scalars}.
  \end{enumerate}
\end{example}
\begin{defn}
  Let $M,M'$ be $R$-modules and $f:M\to M'$ a map. We say $f$ is an \emph{$R$-linear map}\index{morphism!of modules}\index{linear map} if
  \begin{enumerate}
    \item $f(x+x') = f(x) + f(x')$,
    \item $f(ax)  = af(x)$
  \end{enumerate}
  for all $a\in R$ and $x,x'\in M$.
\end{defn}
\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item The composition of $R$-linear maps is again $R$-linear: If $f:M\to N$ and $g:N\to O$ are $R$-linear maps, then $g\circ f:M\to O$ is $R$-linear too.
    \item For all $R$-modules $M$, the identity map $\id:M\to M,~x\mapsto x$ is $R$-linear.
    \item Let $f:M\to N$ be a bijective $R$-linear map. Then the inverse $f^{-1}:N\to M$ is $R$-linear too. In this case, we say $f$ is an \emph{isomorphism} of $R$-modules\index{isomorphism! of $R$-modules}.
  \end{enumerate}
  So we can construct a category $\rmod$, where objects are $R$-modules and morphisms are $R$-linear maps.
  \begin{enumerate}[resume]
    \item For two $R$-modules $M,N$, the set of $R$-linear maps
    \[
      \homr\left(M,N\right) \defined \lset f:M\to M'\ssp f\text{ is }R\text{-linear}\rset
      \]
      is an $R$-module, by setting $f+g:x\mapsto f(x)+g(x)$ and $af:x\mapsto f(ax)$ for all $f,g\in \homr(M,N)$ and $a\in R$. In the notation, the ring $R$ is sometimes ommited and we just write $\hom(M,N)$ for $\homr(M,N)$. So $\rmod$ is a pre-additive and a pre-$R$-linear category.
    \item For an $R$-module $M$, the set of $R$-linear maps $\End_R(M)\defined\homr(M,M)$ has also \emph{non-commutative} ring structure, by setting $f g:x\mapsto (f\circ g)(x)$ for all $f,g\in \End_R(M)$. We call $\End_R(M)$ the set of \emph{$R$-linear endomorphism}, and $f$ an \emph{($R$-linear) endomorphism}.
  \end{enumerate}
\end{rem}
\begin{mrem}
  Using the restriction of scalars from \cref{4: modules examples}, we obtain a functor $F_{\pphi}:R'$-$\modc\to \rmod$ for all rings $R,R'$ and ring homomorphisms $\pphi:R\to R'$. This functor is \emph{faithful}: for all $R'$-modules $M,N$, the induced map $\hom_{R'}(M,N)\to \hom_R(M,N)$ is injective.
\end{mrem}
\begin{example}
  Let $M$ be a $R$-module. The map
  \begin{align*}
    M&\longrightarrow \homr(R,M)\\
    x&\longmapsto \left[a\mapsto ax\right]
    \intertext{is an isomorphism of $R$-modules, with inverse}
  \homr(R,M)&\longrightarrow M\\
  f&\longmapsto f(1).
  \end{align*}
  Note that dual statement is not necessarily true (e.g. $\hom_{\zz}(\zz/2\zz,\zz) = \lset 0\rset$).
\end{example}

\begin{example}\label{4:r[x]-modules-and-endos}
  Let $R$ be a ring. Then an $R[\polvariable]$-module is \glqq the same\grqq{} as an $R$-module $M$, together with an endomorphism $f:M\to M$.\par
  If $M$ is an $R$-module, we can define the an $R[\polvariable]$-structure on $M$ by setting $\polvariable m \defined f(m)$ for all $m\in M$ and extending linearly:
  \[
  \left(\sum_{i=0}^{n}a_i\polvariable^i\right)\left(m\right)\defined \sum_{i=0}^n a_if^{(i)}(m).
  \]\par
  If, on the other hand, $M$ is an $R[\polvariable]$-module, we can regard $M$ as an $R$-module, by restriction of scalars for the embedding $R\hookrightarrow R[\polvariable]$. We also get an endomorphism $f:M\to M$, defined by $m\mapsto \polvariable m$. \par
  Using more fancy language, this is an equivalence of categories between $R[\polvariable]$-$\modc$ and the category $\ccat$ which has as objects tuples $(M,f)$ where $M$ is an $R$-module and $f:M\to M$ is an $R$-linear endomorphism. Morphisms between $(M,f)$ and $(M',f')$ in $\ccat$ are $R$-linear maps $g:M\to M'$ such that the diagram
  \[
  \begin{tikzcd}
    M\ar{r}[above]{f}\ar{d}[left]{g}
    &M\ar{d}[right]{g}\\
    M'\ar{r}[below]{f'}
    &M'
  \end{tikzcd}
  \]
  commutes.
\end{example}

\begin{defn}
  Let $M$ be an $R$-module. A subset $M'\sse M$ is an \emph{$R$-submodule}\index{submodule} if
  \begin{enumerate}
    \item $M'$ is a subgroup of $(M,+)$ and
    \item for all $a\in R$ and $x\in M'$, it holds that $ax\in M'$.
  \end{enumerate}
  If the ring $R$ is clear, we will often refer to $M'$ just as a \emph{submodule}.
\end{defn}

\begin{mexample}
  \leavevmode
  \begin{enumerate}
  \item Let $k$ be a field. Then $k$-submodules of $k$-modules are precisely $k$-subspaces.
  \item The $R$-submodules of $R$ are precisley the ideals of $R$.
  \item Let $H\sse G$ be a subgroup of an abelian group $G$. Then $H$ is a $\zz$-submodule.
  \end{enumerate}
\end{mexample}


\begin{prop}
  Let $M'\sse M$ be a submodule. Then the quotient group $M/M'$ becomes an $R$-module, by setting
  \begin{align*}
    \cdot: R\times R/M'&\longrightarrow M/M'\\
    r,x+M'&\longrightarrow (rx)+M'.
  \end{align*}
  The quotient map $M\to M/M'$ is $R$-linear.
\end{prop}
\begin{proof}
  Ommited.
\end{proof}

\begin{defn}
   Let $f:M\to N$ be an $R$-linear map.
   \begin{enumerate}
     \item The \emph{kernel}\index{kernel} of $f$ is defined as
     $
     \ker f \defined \lset x\in M\ssp f(x) = 0\rset.
     $
     \item The \emph{image}\index{image} of $f$ is defined as
     $
     \im f \defined \lset f(x)\ssp x\in M\rset .
     $
     \end{enumerate}
\end{defn}
\begin{prop}
  Let $f:M\to N$ be an $R$-linear map. The kernel of $f$ is a submodule of $M$, the image of $f$ is a submodule of $N$.
\end{prop}
\begin{defn}
  Let $f:M\to N$ be an $R$-linear map. The \emph{cokernel}\index{cokernel} of $f$ is defined as $\coker f\defined N/\im f$.
\end{defn}
\begin{mlem}
  Let $f:M\to N$ be an $R$-linear map.
  \begin{enumerate}
    \item The kernel of $f$ is trivial if and only if $f$ is injective.
    \item The cokernel of $f$ is trivial if and only if $f$ is surjective.
  \end{enumerate}
\end{mlem}
\begin{prop}
  Let $f:M\to N$ be an $R$-linear map.
  \begin{enumerate}
    \item Let $M'\sse M$ be a submodule such that $M'\sse \ker f$. Then there is a unique $R$-linear map $\overline{f}:M/M'\to N$ such that the following diagram commutes:
    \[
    \begin{tikzcd}
      M\ar{r}{f}\ar{d}&
      N\\
      M/M'\ar[dashed]{ur}[below right]{\overline{f}}&
    \end{tikzcd}
    \]
    \item There is a unique isomorphism $\tilde{f}:M/\ker f \xrightarrow{\sim} \im f$ such that the following diagram commutes:
    \[
    \begin{tikzcd}
      M\ar{r}[above]{f}\ar{d}&
      N\\
      M/\ker f \ar[dashed]{r}[below]{\tilde{f}}[above]{\sim}&
      \im f \ar[hook]{u}
    \end{tikzcd}
    \]
  \end{enumerate}
\end{prop}
\begin{proof}
  Ommited.
\end{proof}
\begin{prop}\label{lec4:abelian-iso}
  Let $L,N\sse M$ be submodules of an $R$-module $M$. Then there is a canonical $R$-linear isomorphism $L/\left(N\cap L\right) \isomorphism \left(N+L\right)/N$ such that the diagram
  \[
  \begin{tikzcd}
    L\ar[hookrightarrow]{r}\ar[twoheadrightarrow]{d}&
    N+L\ar[twoheadrightarrow]{d}\\
    L/\left(N\cap L\right)\ar{r}[above]{\sim}&
    \left(N+L\right)/N
  \end{tikzcd}
  \]
  commutes.
\end{prop}
\begin{proof}
  Ommited.
\end{proof}
\lec
