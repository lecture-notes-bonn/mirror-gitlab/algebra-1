\begin{nonumnotation}
  Let $s\in R$ and consider the multiplicative set $S\defined \lset 1,s,s^2,\hdots\rset$. Then the localization of an $R$-module $M$ at $S$ is denoted by
  \[
  M[s^{-1}]\defined \sloc M.
  \]
\end{nonumnotation}
\begin{mlem}\label{lec10:localization-and-finite-type}
  Let $R'$ be an $R$-algebra of finite type, and $S\sse R$ a multiplicative subset. Then the localization $\sloc R'$ of $R'$ (as $R$-algebra) is an $\sloc R$-algebra of finite type.
\end{mlem}
\begin{proof}
  Since $R'$ is of finite type, there is surjective ring homomorphism $R[\polvariable_1,\hdots,\polvariable_n]\surarr R'$. Since localization is exact, we get an induced epimorphism
  \[
  \begin{tikzcd}
    \left(\sloc R\right)[\polvariable_1,\hdots,\polvariable_n]\ar{r}[above]{\sim}&\sloc \left(R[\polvariable_1,\hdots,\polvariable_n]\right)\ar[twoheadrightarrow]{r}&
  \sloc R'.
  \end{tikzcd}
  \]
\end{proof}
The following is a generalization of NNL to integral domains:
\begin{prop}\label{lec10:nnl-int-domain}
  Let $R$ be an integral domain and $R'$ an $R$-algebra of finite type.
  \begin{enumerate}
    \item There exists an element $s\in R\setminus \lset 0\rset$ and elements $b_1,\hdots,b_n \in R'$ such that
    \begin{itemize}
      \item the elements $b_1,\hdots,b_n$ are algebraically independent over the fraction field $\quot R$; and
      \item the ring extension
      \[
      \begin{tikzcd}
        R[s^{-1}][b_1,\hdots,b_n]\ar[hookrightarrow]{r}&
        R'[s^{-1}]
      \end{tikzcd}
      \]
      is finite.
    \end{itemize}
    \item For all prime ideals $\idp\sse R[s^{-1}]$ there is a prime ideal $\idq\sse R'[s^{-1}]$ such that $\idq \cap R[s^{-1}] = \idp$.
    \item For all prime ideals $\idp\in \spec R[s^{-1}]$ and the prime ideal $\idq$ which was constructed in ii), it holds that
    \begin{align*}
    \quot\left( R/(\idp\cap R)\right) = \quot \left(R[s^{-1}]/\idp\right) &\sse \quot\left( R'[s^{-1}]/\idq\right) \\&= \quot\left( R'/(\idq\cap R')\right),
    \end{align*}
    and the extension is a finite field extension.
  \end{enumerate}
\end{prop}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Set $S\defined R\setminus \lset 0\rset$. Then the induced extension
    \[
    \begin{tikzcd}
      k\defined \quot R = \sloc R\ar[hookrightarrow]{r}&
      \sloc R'
    \end{tikzcd}
    \]
    is of finite type (\cref{lec10:localization-and-finite-type}) and we can apply Noethers Normalization Lemma (\cref{lec9:nnl}) to get algebraically independent elements $b_1',\hdots,b_n'\in \sloc R'$ such that $k[b_1',\hdots,b_n']\inarr \sloc R'$ is a finite ring extension.
    Choose now representatives $b_i\in R'$ and $s_i\in S$ such that $b_i' = b_i/s_i$.
    Then the $b_i$ are already algebraically independent over $k$ and $k[b_1,\hdots,b_n] = k[b_1',\hdots,b_n']$ (since $1/s_i$ is in $k$ for all $i$).\par
    As $R'$ is an $R$-algebra of finite type, there are $c_1,\hdots,c_m\in R'$ (not necessarily algebraically independent) such that $R[c_1,\hdots,c_m] = R'$.
    As $\sloc R'$ is finite over $k[b_1,\hdots,b_n]$ it is in particular integral over $k[b_1,\hdots,b_n]$ (\cref{lec7:finite-alt-char}) and hence there are monic polynomials $f_i\in k[b_1,\hdots,b_n][\polvariable]$ such that $f_i(c_i/1) = 0$  in $\sloc R'$ (for $1\leq i \leq m$).
    We can now choose a $u\in S$ such that the coefficients of all of the $f_i$ are already in the image of the morphism
    \begin{eqnarray*}
     R[u^{-1}][b_1,\hdots,b_n]& \longrightarrow & k[b_1,\hdots,b_n]
    \end{eqnarray*}
    which is induced by the embedding $R[u^{-1}]\inarr k$. Then there are monic polynomials $g_i\in R[u^{-1}][b_1,\hdots,b_n][t]$ such that $g_i$ gets mapped to $f_i$ for all $1\leq i\leq m$.
    Since $b_i,c_i\in R'$ we have $g_i\in R'[u^{-1}]$. So $g_i \in \ker \left(R'[u^{-1}]\to \sloc R'\right)$, as $g(c_i)$ gets mapped to $f(c_i/1) = 0$ (by construction of the $f_i$). Hence there are $v_i \in R\setminus\lset 0\rset$ such that $v_i g_i(c_i) = 0$ in $R'[u^{-1}]$.
    Define now $v\defined v_1\hdots v_{m}$ and $s\defined vu$.\par
    By the universal property of the localization at $u$ and the fact that $R[u^{-1}][b_1,\hdots,b_n]$ we get a morphism
    \begin{eqnarray*}
    \psi: R[u^{-1}][b_1,\hdots,b_n]&\longrightarrow&R[s^{-1}][b_1,\hdots,b_n],
    \end{eqnarray*}
    which also induces a morphism between the corresponding polynomial rings. Set $h_i\defined \psi(g_i)\in R[s {-1}][b_1,\hdots,b_n][\polvariable]$. Then the $h_i$ are monic, since $\psi$ is a ring homomorphism, and $h_i(c_i) 0$.
    So the $c_i$ are integral over $R[s^{-1}][b_1,\hdots,b_n]$, and thus $R[s^{-1}][b_1,\hdots,b_n]\inarr R'[s^{-1}]$ is finite (note $R'[s^{-1}] = R[s^{-1}][c_1,\hdots,c_m]$ and then apply \cref{lec7:finite-alt-char}).
    \item Let $\idp \in \spec R[s^{-1}]$ be a prime ideal. Define
     \[\idp'\defined \idp R[s^{-1}][b_1,\hdots,b_n]+ \genby{b_1,\hdots,b_n}.\]
    Then $\idp'\cap R[s^{-1}] = \idp$ the map
    \begin{eqnarray*}
      R[s^{-1}][b_1,\hdots,b_n]& \longrightarrow & R[s^{-1}]/\idp \\
      1&\longmapsto&\overline{1}\\
      b_i&\longmapsto&0
    \end{eqnarray*}
    induces an isomorphism
    \begin{eqnarray*}
    R[s^{-1}][b_1,\hdots,b_n]/\idp'&\isomorphism&R[s^{-1}]/\idp.
    \end{eqnarray*}
    (Note that this is well-defined, since the $b_i$ are algebraically independent). As $\idp$ is a prime ideal, we get that $\idp'$ is too.\par
    Using Lying Over (\cref{lec8:lying-over}) for the finite integral extension (by i))
    \[
    \begin{tikzcd}
    R[s^{-1}][b_1,\hdots,b_n]\ar[hookrightarrow]{r}&
    R'[s^{-1}]
    \end{tikzcd}
    \]
    we get that there is a prime ideal $\idq \sin \spec R'[s^{-1}]$ such that $\idq\cap R[s^{-1}][b_1,\hdots,b_n] = \idp'$. Thus
    \[
    \left(
    \idq\cap R[s^{-1}][b_1,\hdots,b_n]\right)\cap R[s^{-1}] =
    \idp.
    \]
    \item
    Consider the following commutative diagram
    \[
    \begin{tikzcd}
      R[s^{-1}][b_1,\hdots,b_n]\ar[hookrightarrow]{rr}[above]{\text{finite}}[below]{\text{integral}}\ar{d}&
      &
      R'[s^{-1}]\ar{d}\\
      R[s^{-1}][b_1,\hdots,b_n]/\idp'\ar[hookrightarrow]{rr}&
      &
      R'[s^{-1}]/\idq
    \end{tikzcd}
    \]
    Then
    \[\begin{tikzcd}
      R[s^{-1}]/\idp\ar[hookrightarrow]{r}&R'[s^{-1}]/\idq
    \end{tikzcd}
    \]
    is an integral extension too (\cref{lec7:prime-quotient-of-integral-extension}) and finite, since
    \[
    \begin{tikzcd}
    R[s^{-1}][b_1,\hdots,b_n]\ar[hookrightarrow]{r} &R'[s^{-1}]
    \end{tikzcd}
    \]
    is. \par
    We also have that
    \[\quot R[s^{-1}]/\idp \inarr \quot R'[s^{-1}]/\idq\]
    is finite, by \cref{lec8:integral-induce-algebraic-extension}.\par
    As $s\notin \idp\cap R$, we have
    \[
    \left. \left(R[s^{-1}]\right)_{\idp} \middle / \left(\idp \cdot \left(R[s^{-1}]\right)_{\idp}\right)\right.
    \cong
    \left. R_{\idp\cap R} \middle / \left(R_{\idp\cap R}\right) \right. ,
    \]
    which implies
    \[
    \quot R[s^{-1}]/\idp \cong \quot R/\idp\cap R,
    \]
    as for any ring $A$ and $\idb\in \spec A$ it holds that $\quot A/\idb\cong A_{\idb}/\left(\idb \cdot A_{\idb}\right)$.
  \end{enumerate}
\end{proof}


\chapter{Hilbert's Nullstellensatz and some Algebraic Geometry}

\section{Jacobson Rings}
\begin{defn}
  A ring $R$ is a \emph{Jacobson ring}\index{ring!jacobson}\index{jacobson!ring} if for all prime ideals $\idp\sse R$ it holds that
  \[
  \idp = \bigcap_{\substack{\idp\sse\idm\\\idm\text{ maximal}}} \idm.
  \]
\end{defn}
\begin{mremdef}
  \leavevmode
  \begin{enumerate}
    \item Since being a Jacobson ring is a property of the spectrum it has a topological analog: Let $X$ be a topological space and denote by $X_0$ the set of closed points of $X$. We say that $X$ is \emph{jacobson} if for every closed subset $Z\sse X$ it holds that $Z$ is the closure of $Z\cap X_0$.
    \item Now a ring $R$ is a Jacobson ring if and only if $\spec R$ is jacobson \cite[00G3]{stacks-project}.
  \end{enumerate}
\end{mremdef}
\begin{lem}\label{lec10:jac-alt-char}
  For a ring $R$, the following are equivalent:
  \begin{enumerate}
    \item $R$ is a Jacobson ring.
    \item For all prime ideals $\idp\sse R$ and $a\in R\setminus \idp$, there is a maximal ideal $\idm$ such that $\idp\sse \idm$ and $a\notin \idm$.
    \item For all ideals $I\sse R$ it holds that
    \[
    \sqrt{I} = \bigcap_{\substack{I\sse \idm\\\idm\text{ maximal}}}\idm.
    \]
  \end{enumerate}
\end{lem}
\begin{proof}
  i) $\iff$ iii) follows from \cref{lec2:radical-as-intersection}.
\end{proof}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item Fields are Jacobson rings.
    \item If $R$ is a local ring, which has only one prime ideal, the $R$ is a Jacobson ring.
  \end{enumerate}
\end{example}
\begin{mexample}
\leavevmode
  \begin{enumerate}
    \item If $R$ be a noetherian domain such that every non-zero prime ideal is maximal and $R$ has infinitely many maximal ideals, then $R$ is Jacobson:
    Since every non-zero ideal is maximal, it suffices to show $\genby{0} = \jacr R$. For that, it suffices that every non-zero $x\in R$ is only contained in finitely many prime ideals, i.e. that $Z(x)$ is finite. But $Z(x)$ is isomorphic to $\spec R/\genby{x}$. Now $R/\genby{x}$ is noetherian and every prime ideal is minimal. It is a general fact that noetherian rings have only finitely many minimal prime ideals so the claim follows.
    \item Claim i) implies that all factorial rings with infinitly many prime ideals are Jacobson (like $\zz$). Note that for a factorial ring the condition $\jacr R = 0$ suffices for being jacobson.
    \item Let on the other hand $R$ be a domain which has only finitely many prime ideals.
    Then $R$ cannot be Jacobson:
    We have
    \[
    0\neq \idm_1\cdot \hdots\cdot \idm_n \sse \idm_1\cap \hdots\cap \idm_n,
    \]
    for the maximal ideals $\idm_1,\hdots,\idm_n$.

  \end{enumerate}
\end{mexample}
\begin{lem}
  Let $\pphi:R\inarr R'$ be an integral extension. Assume $R$ is a Jacobson ring. Then $R'$ is too.
\end{lem}
\begin{proof}
  Let $\idq \in \spec R'$ be a prime ideal and set
  \[
  J \defined \bigcap_{\substack{\idq\sse \idm \\\idm\in \maxspec R}} \idm.
  \]
   We first show that $J \cap R = \idq \cap R \defined \idp$:\par
  Since $R$ is a Jacobson ring $\idp$ is the intersection of all maximal ideals containing it.
  Now for any maximal ideal $\idm\in \maxspec R$ with $\idp \sse \idm$, going up (\cref{lec8:going-up-int-ext}) implies that there is a prime ideal $\idn \in \spec R'$ with $\idn\cap R = \idm$ and $\idq \sse \idn$.
  By \cref{7:integral-domain-extension-field-corr} we get that $\idn$ is maximal too, and $J \cap R = \idp$ follows.\par
  We are now in the following situation:
    \[
    \begin{tikzcd}
      R\ar[hookrightarrow]{r}[above]{\text{integral}}\ar{d}
      &R'\ar{d}
      \\
      R_{\idp}\ar[hookrightarrow]{r}[below]{\text{integral}}&
      R'
    \end{tikzcd}~
    \begin{tikzcd}
      \idp = R \cap \idq &
      \idq \ar[dash]{l}\ar[dash]{d}\\
      &
      \idq R'_{\idp}.
    \end{tikzcd}
    \]
    Since $\idq \cap R = \idp$, we have $\pphi\left(R\setminus \idp\right) \cap \idq = \emptyset$, so $\idq R'_{\idq}$ is prime in $R'_{\idp}$.
    Furthermore, we have $\idq R'_{\idp}\cap R_{\idp} = \idp R_{\idp}$.
    Since $\idp R_{\idp}$ is a maximal ideal (\cref{lec3:rp-is-local-ring}), \cref{7:integral-domain-extension-field-corr} implies that $\idq R'_{\idp}$ is a maximal ideal. \par
    As $\idq \sse J$ we have $\idq R'_{\idp}\sse J R'_{\idp}$. Since $\left(R\cap J\right)\cap \left(R\setminus \idp \right) = \emptyset$ we have that $JR'_{\idp}$ is a proper ideal, so $\idq R'_{\idp} = J R'_{\idq}$ follows.
    But now
    \begin{align*}
    J \sse \left(JR'_{\idp}\right)\cap R'&= \idq R'_{\idq}\cap R'\\
                                         &= \idq,
    \end{align*}
    so $J = \idq$ follows.
\end{proof}
\section{Hilbert's Nullstellensatz}
\begin{theorem}[Generalized Hilbert's Nullstellensatz, GHNS]\index{Hilbert! Nullstellensatz}\index{theorem! Hilbert's Nullstellensatz}\label{lec10:ghns}
  Let $R$ be a Jacobson ring and $R\to R'$ a ring map of finite type.
  \begin{enumerate}
    \item $R'$ is a Jacobson ring too.
    \item For all maximal ideals $\idn\sse R'$ it holds that:
    \begin{itemize}
      \item The ideal $\idm \defined \idn\cap R$ is a maximal ideal.
      \item For this $\idm$, $R/\idm\inarr R'/\idn$ is a finite field extension.
    \end{itemize}
  \end{enumerate}
\end{theorem}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Let $\idq \in \spec R'$ be a prime ideal and $b\in R'\setminus \idq$. By \cref{lec10:jac-alt-char}, we want to show that there is a maximal ideal $\idn \in \maxspec R'$ such that $\idq\sse \idn$ and $b\notin \idn$.\par
    For that, set $\idp \defined \idq \cap R$, $\tilde{R}\defined R/\idp$ and  $\tilde{R}'\defined \left(R'/\idq\right)[b^{-1}]$. Note that both $\tilde{R}$ and $\tilde{R}'$ are integral domains.
    Now the induced map $\tilde{R}\to \tilde{R}'$ is injective (since $\tilde{R}'$ is an integral domain) and of finite type (the localization $R'/\idq \inarr \tilde{R}'$ is of finite type.
    Then apply \cref{lec7:composition-of-algebras} and \cref{lec7:quotient-of-finite-type}).
    So by \cref{lec10:nnl-int-domain}, there is a $\tilde{s}\in \tilde{R}\setminus\lset 0\rset$ such that:
    \begin{flalign}\label{lec10:proof:ghns-cond}\tag{$\ast$}
        \left \{
        \begin{tabular}{@{}l@{}}
          For all $\tilde{\idp}\in \spec \tilde{R}$ with $\tilde{s}\notin \tilde{\idp}$  there is a \\
          $\tilde{\idq}\in \spec \tilde{R}'$ such that $\tilde{\idq}\cap\tilde{R} = \tilde{\idp}$ and \\
          the extension $ \ffield{\tilde{\idp}}\to \ffield{\tilde{\idq}}$ is finite
         \end{tabular}
         \right.
    \end{flalign}
    Let $s\in R$ be a preimage of $\tilde{s}$ under the projection $R\surarr \tilde{R}$, so $s\notin \idq\cap R$. Since $R$ is jacobson there is a maximal ideal $\idm \in \maxspec R$ such that
    \[
    \idq \cap R \sse \idm \text{ and }s\notin \idm.
    \]
    Hence $\tilde{s}$ is not contained in the maximal ideal $\idm \tilde{R}\in \maxspec \tilde{R}$.
    By applying \eqref{lec10:proof:ghns-cond} to $\tilde{\idm}\defined \idm \tilde{R}$ we get a prime ideal $\tilde{\idn}\in \spec \tilde{R}'$ such that $\tilde{\idn}\cap \tilde{R} = \tilde{\idm}$.\par
    Consider the finite field extension $\ffield{\tilde{\idp}}\to \ffield{\tilde{\idq}}$.
    We have $\tilde{R}/\tilde{\idm} = R/\idm$ and $\tilde{R}'/\tilde{\idn} = R'/\idn$ (where $\idn\defined \tilde{\idn}\cap R'$). So we arrive at the following commutative diagram:
    \[
    \begin{tikzcd}
      R\ar[hookrightarrow]{rr}[above]{\text{finite type }}\ar[twoheadrightarrow]{d}&
      &
      R'\ar[twoheadrightarrow]{d}\\
      R/\idm \ar[equal]{d}\ar[hookrightarrow]{rr}&
      &
      R'/\idn\ar[hookrightarrow]{d}\\
      \ffield{\tilde{\idm}}\ar[hookrightarrow]{rr}[above]{\text{finite}}&
      &
      \ffield{\tilde{\idn}}
    \end{tikzcd}
    \]
    This yields that $R/\idm\inarr R'/\idn$ is integral, and hence $\idn$ is a maximal ideal (\cref{7:integral-domain-extension-field-corr}). \par
    Since $\idn$ is the preimage of a prime ideal in quotient $R'/\idq$, it folows that $\idq \sse \idn$. \par
    By the description of prime ideals in the localization (\cref{3: ideals under localization}) we get a bijection:
    \begin{eqnarray*}
      \spec \left(R'/\idq\right)[b^{-1}]& \isomorphism & \lset \idq'\in \spec R'\ssp \idq\sse \idq',~b\notin \idq'\rset\\
      \tilde{\idq}& \longmapsto & \tilde{\idq}\cap R'
    \end{eqnarray*}
    So $b\notin \idn$, which concludes the proof.
    \item Let $\idq \in \maxspec R'$ be a maximal ideal. By applying the construction for $b = 1$, we get a maximal ideal $\idm \in \maxspec R$ with $\idq \cap R \sse \idm$ and a maximal ideal $\idn \in \maxspec R'$ with $\idn \cap R = \idm$ and $\idq \sse \idn$.
    Since $\idq$ is a maximal ideal, $\idq = \idn$ follows. So $\idn \cap R = \idq \cap R= \idm$, which is maximal.\par
    We also have that $\quot \tilde{R}/\tilde{m}\inarr \quot \tilde{R}'/\tilde{\idn}$ is finite. Since $\quot \tilde{R}/\tilde{\idm} = R/\tilde{\idm}$ and $\quot \tilde{B}/\tilde{\idn} = B/\idq$, the claim follows.
  \end{enumerate}
\end{proof}
\lec
