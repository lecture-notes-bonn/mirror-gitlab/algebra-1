\section{Integral Extensions}
\begin{nonumconvention}
  In this and the remaining sections of this chapter we will consider $R$-algebras $R'$, induced by ring homomorphisms $\pphi:R\to R'$. In the notation the map $\pphi$ will be ommited, i.e. for $a\in R$ and $b\in R'$ we set $ab\defined \pphi(a)b$. If $\pphi$ is injective, we will show this pictorially by a hooked arrow $\xhookrightarrow{}$. In this case we further identify $R$ with a subring of $R'$. So for an element $a\in R$ and $\pphi:R\inarr R'$, $a\in R'$ means $\pphi(a)\in R'$.
\end{nonumconvention}
\begin{defn}
  Let $R'$ be an $R$-algebra.
  \begin{enumerate}
    \item An element $a\in R'$ is \emph{integral over $R$}\index{integral!element} if there is a monic polynomial $p\in R[\polvariable]$ such that $p(a)=0$, i.e. there are $c\nmind,\hdots,c_0\in R$ such that
    \[
    a^n+c\nmind a^{n-1}+\hdots + c_0 = 0.
    \]
    \item The ring $R'$ is \emph{integral over $R$}\index{integral!over another ring} if all $a\in R'$ are integral over $R$.
    \item The set
    \[
    \overline{R} \defined \lset
    a\in R'\ssp a\text{ integral over }R
    \rset
    \]
    is the \emph{integral closure}\index{integral!closure}\index{closure!integral} of $R$ in $R'$.
    \item The ring $R$ is \emph{integraly closed in $R'$} if $\overline{R}  = \im\left(\pphi:R\to R'\right)$.
    \item The ring $R'$ is \emph{finite over $R$} if $R'$ is finitely generated as $R$-module.
  \end{enumerate}
\end{defn}
\begin{example}
  Consider the inclusion $\zz \inarr \rationals$: Let $a\in \rationals$ be integral over $\zz$. Then there is a monic polynomial $p\in \zz[\polvariable]$ such that $p(a) = 0$, i.e. there are $c\nmind,\hdots,c_0\in \zz$ such that
  \[
  0 = a^n+c\nmind a^{n-1}+\hdots + c_0.
  \]
  Let now $a = r/s$ where $r,s\in \zz$ are coprime. Then
  \[
  r^n = -s \left(c\nmind r^{n-1}+\hdots + c_0 s^n\right)
  \]
  and hence $s$ divides $r$. So $s\in \zz\unts = \lset \pm 1\rset$, which implies $a\in \zz$ and thus $\overline{\zz} = \zz$.\par
  This argument still holds if we replace $\zz$ by a general factorial ring $R$ and $\rationals$ by $\qfield R$.
\end{example}
\begin{hwarning}
  Unlike in the special case of fields, the condition that a polynomial $p$ is monic is necessary, as it is in general not possible to invert the leading coefficient. For example, $a=1/2\in \rationals$ is a root of $2\polvariable -1 \in \zz[\polvariable]$, but still not integral over $\zz$.
\end{hwarning}
\begin{remdef}
  Let $R'$ be an $R$-algebra and $b_1,\hdots,b_n\in R'$. We denote by
  \[
  R[b_1,\hdots,b_n] \defined \lset
  \sum_{i_1,\hdots,i_n}a_{i_1,\hdots,i_n}b^{i_1}\hdots b^{i_n} \ssp a_{i_1,\hdots,i_n}\in R
  \rset \sse R'
  \]
  the smallest $R$-subalgebra of $R'$ which contains all of the $b_1,\hdots,b_n$.
\end{remdef}

\begin{lem}\label{lec7:integral-alt-char}
  Let $R'$ be an $R$-algebra, $b\in R'$. Then the following are equivalent:
  \begin{enumerate}
    \item $b$ is integral over $R$.
    \item The $R$-subalgebra $R[b]$ is a finitely generated $R$-module.
    \item There is a $R$-subalgebra $\tilde{R}\sse R'$ such that $R[b]\sse \tilde{R}\sse R'$, $\tilde{R}$ is a finitely generated $R$-module and $b\in \tilde{R}$.
  \end{enumerate}
\end{lem}
\begin{proof}
  i) $\implies$ ii): Let $p = \polvariable^n+c\nmind \polvariable^{n-1}+\hdots+c_0\in R[\polvariable]$ be a polynomial such that $p(b) = 0$. Then the set $\lset 1,b,\hdots, b^{n-1}\rset$ generates $R[b]$ as $R$-module.\par
  ii) $\implies$ iii): We can simply choose $\tilde{R}\defined R[b]$.\par
  iii) $\implies$ i): If we regard $\tilde{R}$ as an $R$-module, left-multiplication with $b$ (i.e. the map $f_b:R'\to R'$, $m\mapsto b\cdot m$) is an $R$-linear endomorphism of $R'$.\par
  As $\tilde{R}$ is finitely generated as an $R$-module, Cayley-Hamilton (\cref{5:cayley-hamilton}) implies that there is a monic polynomial $p\in R[\polvariable]$ such that $p(f_b) = 0$. So in particular we have $0 = p(f_b)(1) = p(b)$.
\end{proof}
\begin{lem}\label{lec7:composition-of-algebras}
  Let $\pphi:R\to R'$ and $\pphi':R'\to R''$ be ring homomorphisms.
  \begin{enumerate}
    \item If both $\pphi$ and $\pphi'$ are finite  then $\phi'\circ \phi$ is too.
    \item If both $\pphi$ and $\pphi'$ are integral  then $\phi'\circ \phi$ is too.
    \item If both $\pphi$ and $\pphi'$ are of finite type  then $\phi'\circ \phi$ is too.
    \item If $\pphi$ and the composition $\pphi'\circ \pphi$ are of finite type then $\pphi'$ is too.
  \end{enumerate}
\end{lem}
\begin{proof}
  Ommited.
\end{proof}
\begin{cor}\label{lec7:quotient-of-finite-type}
  Let $\pphi:R\to R$ be a ring map of finite type and $\idq\in \spec R'$ a prime ideal. Set $\idp\defined \idq\cap R$. Then the induced map $R/\idp \to R'\idq$ is also of finite type.
\end{cor}
\begin{proof}
  This follows directly from parts iv) and v) of \cref{lec7:composition-of-algebras} and the fact that finite maps are in particular of finite type.
\end{proof}
\begin{cor}\label{lec7:finite-alt-char}
  Let $R'$ be an $R$-algebra. Then the following are equivalent:
  \begin{enumerate}
    \item $R'$ is finite over $R$.
    \item There are elements $b_1,\hdots,b_n\in R'$ which are integral over $R$ such that $R' = R[b_1,\hdots,b_n]$.
    \item $R'$ is an $R$-algebra of finite type and integral over $R$.
  \end{enumerate}
\end{cor}
\begin{cor}
  Let $R'$ be an $R$-algebra. Then the integral closure $\overline{R}\sse R'$ is an $R$-subalgebra.
\end{cor}
\begin{mrem}
  Let $\pphi:R\to R'$ be a ring homomorphism and let $S\sse R$ be a multiplicative set. Then the localization of $R'$ by $S$ as an $R$-module and the localization of $R'$ by $f(S)$ as a ring are isomorphic as $R$-algebras.
\end{mrem}
\begin{mlem}\label{7:localization-algebras}
  Let $R'$ be an $R$-algebra via the ring homomorphism $\pphi:R\to R'$ and $S\sse R$ a multiplicative set. Then the localization $\sloc R'$ is still a ring and the induced map of $\sloc R$-modules
  \begin{eqnarray*}
  \sloc \left(\pphi\right): \sloc R& \longrightarrow & \sloc R'\\
  \frac{a}{s} & \longmapsto & \frac{\pphi(a)}{s}
  \end{eqnarray*}
  is also a ring homomorphism.
\end{mlem}
\begin{lem}\label{7:integral-under-quotient-and-localization}
  Let $R'$ be an $R$-algebra and $R'$ integral over $R$.
   \begin{enumerate}
     \item Let $I\sse R'$ be an ideal and $J\defined R\cap I$. Then $R'/I$ is integral over $R/J$.
     \item Let $S\sse R$ be a multiplicative set. We can regard the localized $\sloc R$-module $\sloc R'$ as an $\sloc R$ module via the induced map from \cref{7:localization-algebras}. Then $\sloc R'$ is integral over $\sloc R$.
   \end{enumerate}
\end{lem}
\begin{proof}
  Ommited.
\end{proof}
\section{Going Up and Going Down}
\begin{mdefn}
  Let $R'$ be an $R$-algebra via the ring homomorphism $\pphi:R\to R'$. If $\pphi$ is injective and $R'$ integral over $R$, we say that $R'$ is an \emph{integral extension of $R$}\index{integral!extension}.
\end{mdefn}
\begin{lem}\label{7:integral-domain-extension-field-corr}
  Let $R$ and $R'$ be integral domains and $R'$ an integral extension of $R$. Then $R'$ is a field if and only if $R$ is a field.
\end{lem}
\begin{proof}
  Assume that $R'$ is a field and let $a\in R\setminus \lset 0\rset$. We want to show that the inverse $b$ of $a$, which exists in $R'$, is an element of $R$. As $b$ is integral over $R$, there are $c_0,\hdots,c\nmind\in R$ such that $b^n = \sum c_i b^i$. Since $b = a^{n-1}b^n$, we have
  \[
  b = \sum_{i=0}^{n-1} c_i a^{n-1-i}\]
  so $b\in R$. Note that we did not need that $R$ or $R'$ is an integral domain for this direction.\par
  Let now $a\in R'\setminus \lset 0\rset$. As $a$ is integral over $R$, $R[a]$ is a finite dimensional $R$-vector space ($R$ is assumed to be a field). Consider now the map
  \begin{eqnarray*}
    f_a:R[a]&\longrightarrow & R[a]\\
    m& \longmapsto & am.
  \end{eqnarray*}
  This is $R$-linear and injective, as $R'$ is an integral domain, and hence bijective. So there is a $b\in R'$ such that $ab = 1$.
\end{proof}
\begin{lem}\label{lec7:prime-quotient-of-integral-extension}
  Let $R'$ be an $R$-algebra which is integral over $R$ and $\idq \sse R'$ a prime ideal. Set $\idp \defined \idq \cap R$. Then $R/\idp\to R'/\idq$ is an integral extension.
\end{lem}
\begin{proof}
By \cref{7:integral-under-quotient-and-localization} we have that $R'/\idq$ is integral over $R/\idp$. Let $\pphi:R\to R'$ be the ring homomorphism that induces the $R$-algebra structure on $R'$. Then for the composition
\[
\begin{tikzcd}
  \overline{\pphi}:R\ar{r}[above]{\pphi}&
  R'\ar{r}&
  R'/\idq
\end{tikzcd}
\]
we have $\idp = R\cap \idq = \ker \overline{\pphi}$. So we get a factorisation of the form
\[
\begin{tikzcd}
  R\ar{r}[above]{\pphi}\ar{d}&
  R'\ar{r}&
  R'/\idq\\
  R/\idp\ar[dashed, bend right = 20]{urr}[below right]{\overline{\pphi}'}
  &
  &
\end{tikzcd}
\]
and $\overline{\pphi}'$ is injective. So $R'/\idq$ is an integral extension of $R/\idp$.
\end{proof}
\begin{mrem}
In the lecture, the claim of \cref{lec7:prime-quotient-of-integral-extension} was only made for injective ring homomorphisms $\pphi:R\inarr R'$.
But this is not necessary, since the induced map $\overline{\pphi}'$ is injective, even if $\pphi$ was not (this is also the version stated in \cite[Prop. 6.8]{franzen}).
\end{mrem}

\begin{cor}\label{lec7:maximal-under-preimage}
  Let $R'$ be an $R$-algebra which is interal over $R$ and $\idq \sse R'$ a prime ideal. Set $\idp \defined \idq\cap R$. Then $\idq$ is maximal if and only if $\idp$ is maximal.
\end{cor}
\begin{proof}
 By \cref{lec7:prime-quotient-of-integral-extension}, $R/\idp\inarr R'/\idq$ is an integral extension.
 The claim now follwos from \cref{7:integral-domain-extension-field-corr} and \cref{lec1:primmax-alt-char}
\end{proof}
\begin{lem}[3am-Lemma]\label{lec7:3am-lemma}
  Let $R'$ be an integral extension of $R$ and $\idq_1\sse \idq_2 \in \spec R'$ prime ideals with $\idq_1\cap R = \idq_2\cap R$. Then already $\idq_1 = \idq_2$ holds.
\end{lem}
\begin{proof}
  Let $\idp\defined \idq_1\cap R = \idq_2\cap R$ and consider $R'_{\idp}$ as the localized $R$-module or equivalently the localization by $\pphi\left(\idp\right)$. (We have $R'_{\idp}\neq 0$ as $\pphi$ is injective.)
  We then have the following commutative diagram
  \[
  \begin{tikzcd}
    R\ar{rr}[above]{\pphi,~\text{integral}}\ar{d}[left]{\eta}&
    &
    R'\ar{d}[right]{\eta'}\\
    R_{\idp}\ar{rr}[below]{\pphi',~\text{integral}}&
    &
    R'_{\idp}
  \end{tikzcd}
  \]
  with maps
  \[
    \eta':R'\to R'_{\idp},~a\mapsto \frac{a}{1}
    \]
  and
  \begin{eqnarray*}
    \pphi': R_{\idp}&\longrightarrow & R'_{\idp}\\
    \frac{a}{s}&\longmapsto& \frac{\pphi(a)}{\pphi(s)} = \frac{\pphi(a)}{s}
  \end{eqnarray*}
  where we identify $s\in R$ and $\pphi(s)\in R'$.\par
  We now have that $\idq_i'\defined \idq_i R'_{\idp}$ is a prime ideal in $R'_{\idp}$ for $i=1,2$, as $\idq_i\cap\left( \pphi\left(R\setminus \idp\right)\right)=\emptyset$. \par
  By the commutativity of the above diagram we get
  \begin{align*}
    \left(
    \idq_i' \cap R_{\idp} \right) \cap R &=
    \left(\idq_i'\cap R'\right) \cap R\\
    &= \left(\left(
    \idq_i R'_{\idp}\right)\cap R'\right) \cap R.
    \intertext{
      As $\idp$ is prime in $R$, we can use \cref{3: ideals under localization}, v) to get $\left(
      \idq_i R'_{\idp}\right)\cap R' = \idq_i$ and hence
    }
    \left(
    \idq_i' \cap R_{\idp} \right) \cap R &=
    \idq_i \cap R\\
    &= \idp.
    \intertext{So, by \cref{3: ideals under localization}, iii), we have that}
    \idp R_{\idp} &= \left(\left(\idq_i'\cap R_{\idp}\right)\cap R\right) R_{\idp}\\
                  &= \idq_i'\cap R_{\idp}.
  \end{align*}
  Now by \cref{lec3:rp-is-local-ring}, ii) we have that $R_{\idp}$ is a local ring with maximal ideal $\idp R_{\idp}$, so by \cref{lec7:maximal-under-preimage} we have that both $\idq_1'$ and $\idq_2$ are maximal in $R'_{\idp}$.
  But since we assumed $\idq_1\sse \idq_2$ this implies $\idq_1' = \idq_2'$.\par
  Using \cref{3: ideals under localization} one last time, we get
  \begin{align*}
    \idq_1 &= \left(\idq_1\cdot R'_{\idp}\right)\cap R'\\
          &= \idq_1'\cap R'\\
          &= \idq_2'\cap R'\\
          &= \idq_2,
  \end{align*}
  which finishes the proof.
\end{proof}
\begin{mrem}
The nickname \grqq 3am-Lemma\grqq{} comes from a characterization of its proof Dr. Heidersdorf gave in the lecture. A more suitable description is that for the induced map $\pphi^{\#}:\spec R'\to \spec R$ there is no proper inclusion in thefibres.
\end{mrem}
\begin{mrem}
  The 3am-lemma should also be true if $\pphi:R\to R'$ is not assumed to be injective, c.f. \cite[00GT]{stacks-project}. One way of seeing this should be the following :\par
  Let $\idq_1\sse \idq_2\sse R'$ be the prime ideals in question, and assume only that $R'$ is integral over $R$.
  By \cref{lec7:prime-quotient-of-integral-extension}, we get that $\overline{\pphi}:R/\idp \to R'/\idq_1$ is an integral extension.
  Denote by $\pi':R'\to R'/\idq_1$ the canonical projection.
  We now can apply our version of the 3am-lemma (\cref{lec7:3am-lemma}) to $\pphi$ to get that $\pi'(\idq_1) = \pi'(\idq_2)$. So by \cref{lec1:prime-ideals-in-quotient}, we get $\idq_1 = \idq_2$.
\end{mrem}
\lec
