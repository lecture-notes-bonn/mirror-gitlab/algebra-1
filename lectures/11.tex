\begin{cor}\label{lec11:ghns-for-fields}
  Let $k$ be a field and $A$ a $k$-algebra of finite type. Then
  \begin{enumerate}
    \item $A$ is a Jacobson ring.
    \item For all maximal ideals $\idm\in \maxspec A$ the map $k\to A\to A/\idm$ is a finite field extension.
    \item The maximal ideals of $A$ are given by
    \[
    \maxspec A =
    \lset
    \idp\in \spec A\ssp
    k\to \quot A/\idp~\text{ is finite}\rset.
    \]
    \item Let $f:A\to B$ be a homomorphism of $k$-algebras of finite type and $\idm\in \maxspec B$ a maximal ideal. Then $\idm \cap A$ is maximal too.
  \end{enumerate}
\end{cor}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item This is just  i) of GHNS (\cref{lec10:ghns}).
    \item By ii) of GHNS, $\idm\cap k$ is a maximal ideal in $k$. But since $k$ is a field, this implies that $\idm \cap k = \genby{0}$. So by the second part of ii), $k\inarr A/\idm$ is a finite field extension.
    \item The inclusion \glqq $\sse$\grqq{} is just ii). For the other direction let $\idp\sse A$ be a prime ideal and assume that in
    \[
    \begin{tikzcd}
      k\ar[hookrightarrow]{r}&
      A/\idp\ar[hookrightarrow]{r}&
      \quot A/\idp
    \end{tikzcd}
    \]
    the composition $k\inarr \quot A/\idp$ is a finite field extension. Then $A/\idp\inarr \quot A/\idp$ is necessarily finite, and so in particular integral. Now by \cref{7:integral-domain-extension-field-corr}, this implies that $A/\idp$ is a field, and hence $\idp$ is a maximal ideal.
    \item Since $B$ is of finite type, it is in particular an $A$-algebra of finite type. By applying ii) of GHNS to $A\to B$, the claim follows.
  \end{enumerate}
\end{proof}

\begin{cor}
  Denote by $\kalgft$ the category whose objects are $k$-algebras of finite type and whose morphisms are $k$-algebra homomorphisms maps. Then $\maxspec(-)$ induces a contravariant functor
  \begin{eqnarray*}
    \maxspec(-):\kalgft&\longrightarrow&\topspace\\
    A&\longmapsto&\maxspec A
  \end{eqnarray*}
  which maps $k$-algebra homomorphisms $\pphi:A\to B$ to the restriction of $\pphi^{\#}$ to $\maxspec B$.
\end{cor}
\begin{proof}
  We only show that the restriction of $\pphi^{\#}$ is well-defined, i.e. that we indeed get a map $\pphi^{\#}:\maxspec B\to \maxspec A$. But this is just iv) of \cref{lec11:ghns-for-fields}.
\end{proof}

\begin{theorem}[Weak Nullstellensatz]\label{lec11:weak-nullstellensatz}
  Let $k$ be an algebraically closed field. For a tupel $x = \left(x_1,\hdots,x_n\right)\in k^n$, denote by $\idm_x$ the ideal
  \[
  \idm_x\defined
  \genby{\polvariable_1-x_1,\polvariable_2-x_2,\hdots,\polvariable_n-x_n}\sse
  k[\polvariable_1,\hdots,\polvariable_n].
  \]
  Then $\idm_x$ is a maximal ideal and the assignment
  \begin{eqnarray*}
    k^n&\longrightarrow&\maxspec k[\polvariable_1,\hdots,\polvariable_n]\\
    x & \longmapsto & \idm_x
  \end{eqnarray*}
  is a bijection.
\end{theorem}
\begin{proof}
  As $k[\polvariable_1,\hdots,\polvariable_n]/\idm_x \cong k$ we find that $\idm_x$ is indeed a maximal ideal. We also have $\idm_x\neq \idm_y$ for $x,y\in k^n$ with $x\neq y$.\par
  It remains to show the surjectivity: Let $\idm\in \maxspec k[\polvariable_1,\hdots,\polvariable_n]$ be a maximal ideal. Then by \cref{lec11:ghns-for-fields}, we have that $k\inarr A/\idm$ is a finite field extension. As $k$ is algebraically closed, there are no non-trivial algebraic field extensions of $k$, so in particular there are no finite extensions. So we have $k\cong a/\idm$.\par
  Denote by $\pi$ the map $\pi:A\surarr A/\idm\isomorphism k$ and set $x_i\defined \pi(\polvariable_i)$. Then $t_i-x_i\in \ker \pi$, and so $\idm_x = \genby{\polvariable_1-x_1,\hdots,\polvariable_n-x_n}\sse \ker \pi = \idm$. But as $\idm_x$ is maximal and $\idm\neq A$, it follows that $\idm_x = \idm$.
\end{proof}
\begin{mrem}
  That a field satiesfies the Weak Nullstellensatz is equivalent to $k$ being algebraically closed, since it implies that every irreducible polynomial in $k[\polvariable]$ is of the form $\polvariable-a$ for a $a\in k$.
\end{mrem}

\section{The Dimension of a Ring}
\begin{defn}
  Let $R$ be a ring.
  \begin{enumerate}
    \item The \emph{dimension of $R$}\index{dimension!of a ring} is defined as
    \[
    \dim R \defined
    \sup \lset l\in \nn \;\middle|\;
    \begin{tabular}{@{}l@{}}
      there is an ascending chain of prime ideals\\
      $\idp_0\subsetneq \idp_1\subsetneq\hdots\subsetneq \idp_l$
     \end{tabular}
     \rset
    \]
    if it exists and $\dim R\defined \infty$ otherwise.
    \item Let $\idp\in \spec R$ be a prime ideal. Then the \emph{height}\index{height! of a prime ideal}\index{prime ideal! height} is defined as
    \[
    \rht \idp \defined
    \sup \lset
    l\in \nn \ssp
    \begin{tabular}{@{}l@{}}
      there is an ascending chain of prime ideals\\
      $\idp_0\subsetneq \idp_1\subsetneq\hdots\subsetneq \idp_l\sse \idp$
     \end{tabular}
     \rset
     \]
     if it exists and $\rht \idp \defined \infty $ otherwise.
  \end{enumerate}
\end{defn}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item Let $k$ be a field. Then $\dim k = 0$, as every prime ideal is maximal.
    \item Let $R$ be a prinicipal ideal domain which is not a field. Then every ascending chain of prime ideals in $R$ is of the form $\genby{0}\ssne \genby{p}$, for a prime element $p \in R$. So $\dim R = 1$.
    \item Consider the polynomial ring in $n$ variables over a field $k$. Then
    \[
    0 \ssne \genby{\polvariable_1}\ssne \genby{\polvariable_1,\polvariable_2}\ssne \hdots \ssne
    \genby{\polvariable_1,\hdots,\polvariable_n}
    \]

    is stricly ascending, so $\dim k[\polvariable_1,\hdots,\polvariable_n]\geq n$.
  \end{enumerate}
\end{example}
\begin{lem}\label{lec11:ht-and-dim-locquot}
Let $\idp \in \spec R$ be a prime ideal.
  \begin{enumerate}
    \item It holds that $\rht \idp = \dim R_{\idp}$.
    \item It holds that $\dim R \geq \dim R/\idp+\rht \idp$.
    \item The dimension of $R$ is local in the following sense:
    \begin{align*}
      \dim R &= \sup \lset \dim R_{\idm}\ssp \idm\in \maxspec R\rset\\
             &= \sup \lset \rht \idm \ssp \idm \in \maxspec R\rset.
    \end{align*}
  \end{enumerate}
\end{lem}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item By the classification of prime ideals in $R_{\idp}$ (\cref{lec3:rp-is-local-ring}), there is a order-preserving bijection
    \[
    \begin{tikzcd}
      \spec R_{\idp} \ar[leftrightarrow]{r}[above]{\sim}& \lset \idp'\in \spec R\ssp \idp' \subseteq \idp\rset.
    \end{tikzcd}\]
    Now the statement is now just the definition of height and dimension.
    \item This follows from the classification of prime ideals of $R/\idp$ (\cref{lec1:prime-ideals-in-quotient}) and the definition of height and dimension.
    \item After localising at a maximal ideal, the image of an ascending chain is still an ascending chain. Vice versa, every ascending chain in the localization at a maximal ideal can be lifted to an ascending chain in $R$. This shows the first equality. The second equality follows from i).
  \end{enumerate}
\end{proof}
\begin{mdefn}
  Let $\idp_0\ssne \hdots \ssne \idp_n$ be an ascending chain in $R$. We say that this chain is \emph{maximal} if there is no prime ideal $\idp\in \spec R$ with $\idp\ssne \idp_0$ or $\idp_i\ssne \idp\ssne \idp_{i+1}$ (for $0\leq i\leq n-1$) or $\idp_n\ssne \idp$.
\end{mdefn}
\begin{lem}\label{lec11:max-chains-props}
  Let $R$ be a ring with $\dim R<\infty$. Assume all maximal chains in $R$ have the same length. Let $\idp \in \spec R$ be a prime ideal.
  \begin{enumerate}
    \item All maximal chains in $R/\idp$ have the same length.
    \item It holds that $\dim R = \dim R/\idp + \rht \idp$.
    \item It holds that $\dim R_{\idp} = \dim R$ if $\idp$ is a maximal ideal.
  \end{enumerate}
\end{lem}
\begin{proof}
  Let $\idq_0\ssne \hdots \ssne \idq_r$ be a maximal chain in $R/\idp$. We can lift this to a chain in $R$ and complete it to a maximal chain of the form
  \[
  \idp_0\ssne \hdots \ssne \idp_m = \idp = \idq_0 \ssne \hdots \ssne \idq_r.
  \]
  Now $\rht \idp \geq m$ and $\dim R/\idp \geq r$. By the assumption that every maximal chain in $R$ has the same length, it follows that $m+r = \dim R$. By \cref{lec11:ht-and-dim-locquot} ii), we have
  \[
  \dim R \geq \dim R/\idp + \rht p = m +r = \dim R.
  \]
  So $\rht \idp = m$ and $\dim R/\idp = r$ follows. This shows ii). For i), note that the completion of the lift of the ideal chain in $R$ is independend from $r$, so $r$ has to be necessarily the same for all prime chains in $R/\idp$. Claim iii) now follows from ii), since for a maximal ideal, we have $\dim R/\idp = 0$.
\end{proof}
\begin{prop}\label{lec11:dimension-int-ext}
  Let $\pphi:R\inarr R'$ be an integral ring extension.
  \begin{enumerate}
    \item It holds that $\dim R = \dim R'$.
    \item For all $\idq\in \spec R'$ it holds that $\dim R_{\idq\cap R} \geq \dim R'_{\idq}$.
    \item If $\pphi$ satisfies going down then $\dim R_{\idq\cap R} = \dim R'_{\idq}$.
  \end{enumerate}
\end{prop}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Let $\idq_0 \ssne \hdots \ssne \idq_l$ be an ascending chain in $R'$. Then $\idq_0 \cap R \ssne \hdots \ssne \idq_l\cap R$ is an ascending chain in $R$ (That the inclusions are strict follows from \cref{lec7:3am-lemma}). This shows $\dim R \geq \dim R'$. \par
    Let $\idp_0\ssne \hdots \ssne \idp_d$ be an ascending chain in $R$. Then by Lying Over (\cref{lec8:lying-over}) there is a prime ideal $\idq_0 \in \spec R'$ with $\idq_0\cap R = \idp$.
    Now by \cref{lec8:going-up-int-ext} there is a chain $\idq_0\ssne \hdots \ssne \idq_d$ in $R'$ such that the following diagram is commutative:
    \[
    \begin{tikzcd}[column sep = tiny]
      \idq_0&\ssne&\idq_1&\ssne & \hdots & \ssne & \idq_d\\
      \idp_0\ar[dash]{u}&\ssne&\idp_1\ar[dash]{u}&\ssne & \hdots & \ssne & \idp_d \ar[dash]{u}
    \end{tikzcd}
    \]
    That the inclusions are strict follows from \cref{lec7:3am-lemma}.
    \item  Let $\idq_0\ssne \hdots \idq_l = \idq$ be a chain in $R'$. This gives a chain of the form $\idq_0 \cap R \ssne \hdots \ssne \idq_l\cap R$ in $R$. By applying \cref{lec11:ht-and-dim-locquot} twice, the claim follows.
    \item Let $\idp_0\ssne \hdots \idp_l = \idq \cap R$ be a chain in $R$. By applying going down, we can lift this to a chain of the form
    \[
    \begin{tikzcd}[column sep = tiny]
      \idq_l&\supsetneq&\idq_{l-1}&\supsetneq & \hdots & \supsetneq & \idq_0\\
      \idq\cap R\ar[dash]{u}&\supsetneq&\idp_{l-1}\ar[dash]{u}&\supsetneq & \hdots & \supsetneq & \idp_0 \ar[dash]{u}
    \end{tikzcd}
    \]
    So $\dim R'_{\idq} \geq \dim R_{\idq\cap R}$. The claim now follows from ii).
  \end{enumerate}
\end{proof}
\begin{mlem}[Going Between]\label{lec12:going-between}
  Let $k$ be a field and $R$ a $k$-algebra of finite type. Let $R\inarr R'$ be an integral ring extension.
  Let $\idp_1\ssne \idp_2\ssne \idp_3$ be prime ideals in $k$ and $\idq_1\ssne \idq_3$ prime ideals in $R'$.
  Assume that $\idq_1\cap R = \idp_1$ and $\idq_3\cap R = \idp_3$.
  Then there is a prime ideal $\idq_2\in \spec R'$ satisfying $\idq_1\ssne \idq_2\ssne \idq_3$.
\end{mlem}
\begin{proof}
  This is on the sixth exercise sheet.
\end{proof}
\begin{mrem}
  Note that in the setting of going between, it does not necessarily hold that $\idq_2\cap R = \idp_2$. A counterexample can also be found in the solutions to sheet 6.
\end{mrem}
\begin{theorem}\label{lec11:dim-of-polring}
  Let $k$ be a field.
  \begin{enumerate}
    \item It holds that $\dim k[\polvariable_1,\hdots,\polvariable_n] = n$.
    \item All maximal chains in $k[\polvariable_1,\hdots,\polvariable_n] = n$ have the same length.
  \end{enumerate}
\end{theorem}

\begin{proof}
  We will do this by induction on $n$. In the case $n=1$, $k[\polvariable]$ is an integral domain, and hence has dimension 1. In particular, maximal chains have necessarily the same length.\par
  In the general case, consider a chain of prime ideals $\idp_0\ssne \hdots\ssne \idp_m$ in $k[\polvariable_1,\hdots,\polvariable_n]$.
  Without loss of generality, we can assume that this chain is maximal.
  This implies that $\idp = \genby{0}$ (since $\ktn$ is an integral domain), $\idp_m$ is a maximal ideal and $\idp_1 = \genby{f}$, where $f$ is an irreducible polynomial (since $\ktn$ is factorial).
  Using \cref{lec9:substitution-lemma}, we can assume that $f$ is a monic polynomial in $\polvariable_n$, with non-leading coefficients in $k[\polvariable_1,\hdots,\polvariable_{n-1}]$. So we get an integral extension $k[\polvariable_1,\hdots,\polvariable_{n-1}]\inarr \ktn/\genby{f}$. Consider now the images of the original chain under the following maps:
  \begin{eqnarray*}
  \pi\smap:  \spec \ktn &\longrightarrow & \spec \ktn/\idp_1 \\
  \idp_i & \longmapsto & \idp_i/\idp_1\\
  \pphi\smap:  \spec \ktn/\idp_1 & \longrightarrow & \spec k[\polvariable_1,\hdots,\polvariable_{n-1}]\\
    \idq & \longmapsto & \idq \cap k[\polvariable_1,\hdots,\polvariable_{n-1}],
  \end{eqnarray*}
  for all prime ideals $\idp_i$ in the chain we started with.
  Then both maps preserve prime ideals (the image of a prime ideal under $\pi\smap$ is again prime, since $\pi$ maps into a quotient), strict inclusions of prime ideals ($\pphi\smap$ by \cref{lec7:3am-lemma}). Furthermore, maximal chains are mapped to maximal chains:\par
  In the chain
  \[
  0\ssne \idp_2/\idp_1\cap k[\polvariable_1,\hdots,\polvariable_{n-1}]\ssne \hdots \ssne \idp_m/\idp_1\cap k[\polvariable_1,\hdots,\polvariable_{n-1}]
  \]
  the ideal $\idp_m/\idp_1\cap k[\polvariable_1,\hdots,\polvariable_{n-1}]$ is again maximal, by \cref{lec11:ghns-for-fields} iii). Now by Going Between (\cref{lec12:going-between}), the chain is indeed maximal.
  But by the induction hypothesis, the length of a maximal chain in $k[\polvariable_1,\hdots,\polvariable_{n-1}]$ is exactly $n-1$, for all maximal chains. Using the prime ideal correspondence for the quotient, the claim follows.
\end{proof}
Let $k$ be a field and $A$ a $k$-algebra of finite type. Then by the Noether Normalization Lemma (\cref{lec9:nnl}), there are algebraically independet elements $e_1,\hdots,e_n$ such that $A$ is integral over $k[e_1,\hdots,e_n]$.
\begin{cor}
  In this case, $n = \dim A$ holds.
\end{cor}
\begin{proof}
  By \cref{lec11:dimension-int-ext}, we have $\dim A = \dim k[e_1,\hdots,e_n]$ and $\dim k[e_1,\hdots,e_n] = n$ by \cref{lec11:dim-of-polring}.
\end{proof}
\begin{cor}\label{lec11:ftype-alg-max-length}
  Let $A$ be a $k$-algebra of finite type and assume that $A$ is an integral domain. Then all maximal chains in $A$ have the same length.
\end{cor}
\begin{proof}
  Let $\pphi:k[\polvariable_1,\hdots,\polvariable_n]\surarr A$, then $A \cong k[\polvariable_1,\hdots,\polvariable_n]/\ker \pphi$. Since $A$ is an integral domain it follows that $\ker \pphi$ is a prime ideal.
  Since by \cref{lec11:dim-of-polring} all chains in $k[\polvariable_1,\hdots,\polvariable_n]$ have the same length and $\ker \pphi$ is prime, the same is true for $k[\polvariable_1,\hdots,\polvariable_n]/\ker \pphi$ (by \cref{lec11:max-chains-props}).
\end{proof}
\begin{mcor}
  Let $A$ be a $k$-algebra of finite type and integral domain. Then for every maximal ideal $\idm \in \maxspec A$, we have $\dim A_{\idm} = \dim A$.
\end{mcor}
\begin{proof}
  This now follows from the more general result \cref{lec11:max-chains-props}.
\end{proof}
\lec
