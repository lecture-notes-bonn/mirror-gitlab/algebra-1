\section{Dimension Theory of Noetherian Rings}
\begin{defn}
  Let $\idp\in \spec R$ be a prime ideal and $n\in \nn$. We define $\idp^{(n)}\defined \left(\idp^n R\ploc\right)\cap R$ which is called the \emph{$n$-th symbolic power of $\idp$}\index{symbolic power}.
\end{defn}

\begin{lem}\label{lec16:symp-basic-props}
  Let $\idp\in \spec R$ be a prime ideal.
  \begin{enumerate}
    \item It holds that
    \[
    \idp\symp{n} =
    \lset a\in R\ssp \text{there is a }s\in R\setminus \idp \text{ such that }sa\in \idp^n \rset.
    \]
    \item The chain
    $
    \idp\symp{0}\supseteq \idp\symp{1}\supseteq \hdots
    $
    is descending and $\idp\symp{0} = R$, $\idp\symp{1} = \idp$.
    \item It holds that $\idp^n\sse \idp\symp{n}$ for all $n\in \nn$.
    \item It holds that
    \[
    \idp\symp{n}R_{\idp} = \idp^nR_{\idp} = \left(\idp R_{\idp}\right)^n.
    \]
    \end{enumerate}
\end{lem}

\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item For the inclusion \grqq $\supseteq$\grqq{}, let $a\in R$ and assume that there is a $s\in R\setminus \idp$ such that $sa\in \idp^n$. Then in $R_{\idp}$ we have that $a/1 = sa/s\in \idp^n R\ploc$ and hence $a\in \left(\idp^nR\ploc\right)\cap R$.\par
    For the reverse inclusion, let $a\in \left(\idp^nR\ploc\right)\cap R$.
    Then $a/1\in \idp^nR\ploc$ and so there is a $b\in \idp^n$ and a $t\in R\setminus \idp$ such that $a/1 = b/t$.
    Hence there exists a $u\in R\setminus \idp$ such that $uta = ub \in \idp^n$.
    So $s\defined ut\notin \idp$ (since $\idp$ is prime) and $sa= ub\in \idp^n$.
    \item This is clear.
    \item This follows from i), by setting $s=1$.
    \item The first equality is just \cref{3:ext-cont-basic-properties} applied to $\idp$.
    For the second equality and the direction \grqq$\sse$\grqq, let $b\in \idp^n$. Then there are $b_{ij}\in \idp$ such that $b = \sum_i b_{i,1}\hdots b_{i,n}$. For $s\in R\setminus \idp$ we have
    \[
    \frac{b}{s} = \sum_i \frac{b_{i,1}}{s}\cdot \frac{b_{i,2}}{1}\hdots \frac{b_{i,n}}{1} \in \left(\idp R\ploc\right)^n
    \]\par
    For the inclusion \glqq$\supseteq$\grqq, let $c\in \left(\idp R\ploc\right)^n$.
    Then there are $b_{i,j}\in \idp$ and $s_{i,j}\in R\setminus \idp$ such that
    \[
    c = \sum_i \frac{b_{i,1}}{s_{i,1}}\hdots \frac{b_{i,n}}{s_{i,n}}.
    \]
    Set
    \[
    s\defined \prod_{i,j}s_{i,j}~\text{and}~
    b\defined \sum_i\left(\left(\prod_{\substack{1\leq j\leq n\\ j\neq i}}s_{ij}\right)\left(\prod_{j=1}^nb_{i,j}\right)\right).
    \]
    Then $b\in R\setminus \idp$ and $b\in \idp\symp{n}$ and thus $c = b/s\in \idp^nR\ploc$.
    \end{enumerate}
\end{proof}
\begin{mdefn}
  Let $a\in R$ and $\idp\in \spec R$ be a prime ideal. We say that \emph{$\idp$ is minimal over $a$} if $a\in \idp$ holds and there is no prime ideal $\idq\in \spec R$ such that $\genby{a}\sse \idq \ssne \idp$ holds.
\end{mdefn}
\begin{prop}
  Let $R$ be a noetherian ring. Then the set of inclusion-minimal prime ideals is finite and non-empty.
\end{prop}
\begin{proof}

\end{proof}
\begin{theorem}[Krull's Principal Ideal Theorem]\label{lec16:krull-pid-theorem}
  Let $R$ be a noetherian ring and $\idp\in \spec R$ be a prime ideal. If there is an element $a\in R$ such that $\idp$ is minimal over $a$ then $\rht \idp \geq 1$ holds.
\end{theorem}
\begin{proof}
  Let $\idq'\sse \idq \ssne \idp$ be a chain of prime ideals. We want to show that $\idq = \idq'$ holds. Consider first the following simplifications:
  \[
  R\leadsto R/\idq' \leadsto \left(R/\idq'\right)\ploc.
  \]
  Then $\left(R/\idq'\right)\ploc$ is a local noetherian integral domain and by various prime ideal correspondences, it suffices to show $\idq \left(R/\idq'\right)\ploc = 0$. So we show the following:\\
   \textit{Let $(R,\idp)$ be a local noetherian integral domain such that the unique maximal ideal $\idm$ is minimal over an element $a\in R$. Then $\spec R = \lset 0,\idp\rset$.}
   \begin{claim}
     Let $\idq\in \spec R$ be a prime ideal. Then there is a $n\geq 0$ such that $\idq\symp{n}\sse \idq\symp{n+1}+\genby{a}$.
   \end{claim}
   \begin{claimproof}
     The quotient $R/\genby{a}$ is a noetherian ring and since there are no prime ideals between $\genby{a}$ and $\idp$, we have $\dim R/\genby{a}  =0$.
     So $R/\genby{a}$ is artinian (\cref{lec15:artinian-alt-char}, ii)) which implies that the chain of ideals
     \[
     \left(\idq\symp{0}+\genby{a}\right)/\genby{a} \supseteq
     \left(\idq\symp{1}+\genby{a}\right)/\genby{a}\supseteq \hdots
     \]
     terminates for a $n\geq 0$.
     Then
     $\idq\symp{n}+\genby{a}\sse \idq\symp{n+1}+\genby{a}$ holds
     and so in particular $\idq\symp{n}\sse \idq\symp{n+1}+\genby{a}$.
   \end{claimproof}
   \begin{claim}
      In the above situation, it holds that $\idq\symp{n} = \idq\symp{n+1}+\idp\idq\symp{n}$.
   \end{claim}
   \begin{claimproof}
     The inclusion \enquote{$\supseteq$} is clear ( $\idp \idq,~\idq\symp{n+1}\sse \idq \symp{n}$, the latter by \cref{lec16:symp-basic-props}). For the other inclusion let $b\in \idq\symp{n}$. Then by the above claim %add reference system!
     we have $b = c + ar$ with $c\in \idq\symp{n+1}$ and $r\in R$.
     So $ar = b-c \in \idq\symp{n}$. By \cref{lec16:symp-basic-props} i), there is a  $s\in R\setminus \idq$ such that $s\cdot ar\in \idq^n$.
     As $\idp$ is minimal over $a$ and $(R,\idp)$ is a local ring, we have $a\notin \idq$ and thus $sa\cdot r \in \idq^n$ implies $r\in \idq\symp{n}$ (again by \cref{lec16:symp-basic-props},i)).
     Now this gives $b = c+ar$, with $c\in \idq\symp{n+1}$, $r\in \idq\symp{n}$ and $a\in \idp$ and thus $b\in \idq\symp{n+1}+\idp \idq\symp{n}$.
   \end{claimproof}

   We now apply  the Nakayama \cref{5:submodule-plus-jacobson-is-whole-ring} to $M\defined \idq\symp{n}$, $N\defined \idq\symp{n+1}$ and $I = \idp$ (Note that $I = \jacr R$, since $R$ is a local ring).
   Then as $M = N+IM$ it follows that $\idq\symp{n} = \idq\symp{n+1}$.\par
   Consider the localization $R\qloc$. By applying \cref{lec16:symp-basic-props} iv) twice we have
   \begin{align*}
   \left(\idq R\qloc\right)^n &= \idq\symp{n}R\qloc \\
                              &= \idq\symp{n+1}R\qloc \\
                              &= \left(\idq R\qloc\right)^{n+1}.
    \end{align*}
    Applying the classical Nakayama Lemma (\cref{5:nakayama-classical}) to $M = \left(\idq R\qloc\right)^n$, $I = \idq R\qloc = \jacr R\qloc$, and since
    \[
    M = \left(\idq R\qloc\right)^n = \left(\idq R\qloc\right)^{n+1} = IM
    \]
    this yields $M = 0$. As $R\qloc \neq 0$, this implies $\idq^n = 0$ and as $R$ is a domain, this  ultimately  shows $\idq =  0$.
\end{proof}
\begin{lem}\label{lec16:defideal-art-to-subset}
  Let $R$ be an artinian ring. Then the Jacobson ideal $\jacr R$ is nilpotent, i.e. there is a $k\geq 1$ such that $\left(\jacr R\right)^k  0$.
\end{lem}
{
\newcommand{\nca}{\mathcal{N}}
\begin{proof}
  As $R$ is an artinian ring, \cref{lec15:artinian-alt-char} ii) implies that $\mathcal{N}\defined \nilr R = \jacr R$.
  The chain $\nca\spse \nca^2\spse \hdots$ is decreasing and hence terminates (as $R$ is artinian).
  So there is $k\geq 1$ such that $\nca^k = \nca^{k+1}\eqqcolon \ida$. Assume $\ida\neq 0$. Then the set
  \[
  \Sigma \defined
  \lset
  \idb \sse R \ssp
  \begin{tabular}{@{}l@{}}
    $\idb$ is an ideal\\
    $\idb \cdot \ida \neq 0$
   \end{tabular}
  \rset
  \]
  is not empty as $\ida \in \Sigma$. Now $\Sigma$ is partially ordered by inclusion and $R$ being artinian implies that $\Sigma$ has an inclusion-minimal element $\idc$ (by \cref{lec15:noeth-alt-char}). \par
  Now by the minimality condition $\idc = \genby{x}$ for an element $x\in R$ (as there is an $x\in \idc$ with $x\ida\neq 0$).
  We also have $x\ida \sse \genby{x}$, as $\left(x\ida \right)\ida = x\ida^2 = x \ida \neq 0$ and hence $x\ida \sse \genby{x}$.\par
  So there is a $y\in \ida$ wit $x = xy$. Hence $x = xy^k  = 0$, contradicting $x\neq 0$.
\end{proof}
}
\begin{lem}\label{lec16:defintionideal}
  Let $(R,\idm)$ be a local noetherian ring with unique maximal ideal $\idm$ and $I\ssne R$ a proper ideal. Then the following are equivalent:
  \begin{enumerate}
    \item There is a $n\geq 1$ such that $\idm^n \sse I$.
    \item For all prime ideals $\idq\in \spec R$ with $I\sse \idp$ it already holds that $\idp = \idm$.
    \item It holds that $\dim R/I = 0$.
    \item The ring $R/I$ is artinian.
  \end{enumerate}
\end{lem}
  An ideal satisfying any of the above conditions is called an \emph{ideal of definition}.

\begin{proof}[Proof of \cref{lec16:defintionideal}]\leavevmode  \begin{claim}
    Let $(R,\idm)$ be a local ring such that there is a $k\geq 1$ with $\idm^k = 0$. Then $\spec R = \lset \idm\rset$.
  \end{claim}
  \begin{claimproof}
    For all $\idp \in \spec R$ prime we have $\idp \sse \idm$. Now let $b\in \idm$. Then $b^k = 0\in \idp$ and so $b \in \idp$.
  \end{claimproof}
  Applying this to $R/I$ proves i) $\implies$ ii). Now
  ii) $\implies$ iii) is just the definition of $\dim R/I$.\par
  iii) $\implies$ iv): $R/I$ is still a noetherian ring and $\dim R/I  =0$ is equivalent to all prime ideals of $R/I$ being already maximal. The claim now follows from \cref{lec15:artinian-alt-char}.
  \item[] iv) $\implies$ i): By \cref{lec16:defideal-art-to-subset} we have that $\jacr R/I$ is nilpotent. But since $\jacr R/I = \overline{\idm}$, this just means that there is a $n\geq 1$ with $\idm^n \sse I$.
\end{proof}

\begin{theorem}\label{lec16:ideal-of-def-dimension}
  Let $(R,\idm)$ be a local noetherian ring.
  \begin{enumerate}
    \item If $I = \genby{a_1,\hdots,a_l}$ is an ideal of definition, then $\dim R \leq l$.
    \item Assume $\dim R = d$. Then there is an ideal of definition which is generated by $d$ elements.
  \end{enumerate}
\end{theorem}


\begin{cor}\label{lec16:loc-at-minimal-1}
  Let $R$ be a noetherian ring, $a_1,\hdots,a_l\in R$ and $\idp\in \spec R$ be a minimal prime ideal of $\genby{a_1,\hdots,a_l}$. Then $\dim R\ploc \leq l$.
\end{cor}
\begin{proof}
  After localising at $\idp$ the ring $R\ploc$ is a noetherian local ring. Now the ideal $\genby{a_1,\hdots,a_l}$ is an ideal of definition, by \cref{lec16:defintionideal} ii).
\end{proof}
\begin{cor}
  Let $A$ be a $k$-algebra of finite type and a domain. For an element $0\neq x\in A$, let $\idp$ be a minimal prime ideal of $x$. Then $\dim A/\idp = \dim A - 1$.
\end{cor}
\begin{proof}
  By \cref{lec16:loc-at-minimal-1} we have $\dim A\ploc \leq 1$. Now in the case $\dim A\ploc = 0$ this means that $\idp$ is a minimal prime ideal of $A$ and hence $\idp = \genby{0}$ (as $A$ is an integral domain). But then $x = 0$, contradicting the assumption $x\neq 0$.\par
  So $\dim A\ploc = 1$. Now by \cref{lec11:max-chains-props} and \cref{lec11:ftype-alg-max-length} we have $\dim A = \dim A/\idp + \dim A\ploc$ and hence
  \[\dim A/\idp = \dim A - \dim A\ploc = \dim A - 1.\]
\end{proof}

\lec
