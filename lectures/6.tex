\begin{lem}\label{lec6:nakayama-generators}
  Let $M$ be a finitely-generated $R$-module, $m_1,\hdots,m_n \in M$ and $I\sse \jacr R$ an ideal. Then the following are equivalent:
  \begin{enumerate}
    \item The set $\lset m_i\rset_i$ generates $M$ as an $R$-module.
    \item The set $\lset \qclass{m_i}\rset_i$ generates $M/IM$ as an $R/I$-module.
  \end{enumerate}
\end{lem}
\begin{proof}
  If the set $\lset \qclass{m_i}\rset_i$ generates $M/IM$ as an $R/I$-module, then it also generates $M/IM$ as an $R$-module, since the canonical map $R\to R/I$ is surjective. So
  \[
  \genby{m_1,\hdots,m_n}+ IM = M
  \]
  and by \cref{5:submodule-plus-jacobson-is-whole-ring}, $\genby{m_1,\hdots,m_n} = M$ follows.
\end{proof}

\section{Tensor Products}
At this point, one normally introduces the tensor product of modules. But we only did this on exercise sheet 12. For a more detailed treatment, the reader is refered to \cite[00CV]{stacks-project} or \cite[Chapitre 4.1]{grimoire}.
\begin{defn}
  Let $M$ be an $R$-module. We say that $M$ is flat if the functor
  \begin{eqnarray*}
  -\tensor_R M  \rmod & \longrightarrow & \rmod\\
    N & \longmapsto & N\tensor_R M.
  \end{eqnarray*}
\end{defn}
\begin{prop}\label{lec6:flat-local-global}
  Let $R\to R'$ be an algebra. Let $M$ be an $R'$-module and consider it as an $A$-module via restriction of scalars. Then the following are equivalent:
  \begin{enumerate}
    \item $M$ is a flat $A$-module.
    \item $M\ploc$ is a flat $A$-module for all $\idp\in \spec B$.
    \item $M\mloc$ is a flat $A$-module for all $\idm \in \maxspec B$.
  \end{enumerate}
\end{prop}
\begin{proof}
  Ommited.
\end{proof}
\begin{lem}\label{lec6:flat-factorisation}
  Let $M$ be an $R$-module. Then the following are equivalent:
  \begin{enumerate}
    \item $M$ is a flat $R$-module.
    \item For all $R$-modules of finite presentation $N$ and $R$-linear maps $f:N\to M$ there is a finite-free $R$-module $L$ and $R$-linear maps $g:N\to L,~h:L\to M$ such that the diagram
    \[
    \begin{tikzcd}
      N\ar{rr}[above]{f}\ar{dr}[below left]{g}&&M\\
      &L\ar{ur}[below right]{h}&
    \end{tikzcd}
    \]
    commutes.
  \end{enumerate}
\end{lem}
\begin{proof}
  This seems to be annoying \cite[4.67]{grimoire}.
\end{proof}
\section{Algebras}
\begin{defn}
  Let $R$ be a ring.
  \begin{enumerate}
    \item An \emph{$R$-algebra}\index{algebra} consists of a tuple $(R',\pphi)$, where $R'$ is a ring and $\pphi:R\to R'$ is a ring homomorphism.
    \item Let $(R',\pphi'),(R'',\pphi'')$ be $R$-algebras. A ring homomorphism $f:R'\to R''$ is an \emph{$R$-algebra homomorphism} if the following diagram commutes:
    \[
    \begin{tikzcd}
      R'\ar[dashed]{rr}[above]{f}&
      &
      R''\\
      &
      R\ar{ul}[below left]{\pphi'}\ar{ur}[below right]{\pphi''}&
    \end{tikzcd}
    \]
    \item Let $R'$ be an $R$-algebra. We say $R'$ is \emph{finitely generated as $R$-algebra}\index{finitely generated! as algebra} if there is a $n\geq 0$ and $b_1,\hdots,b_n\in R'$ such that the evaluation map
    \begin{eqnarray*}
          \eval_{b_1,\hdots,b_n}: R[\polvariable_1,\hdots,\polvariable_n]&\longrightarrow & R'\\
          \polvariable_i & \longmapsto & b_i
    \end{eqnarray*}
    is surjective. In this case, we will also sometimes say that $R'$ is \emph{an $R$-algebra of finite type}\index{algebra! of finite type}\index{finite type}.
  \end{enumerate}
\end{defn}
\begin{mrem}
  The morphism $\pphi$ in the definition of an $R$-algebra is often left implicit.
\end{mrem}
\begin{hwarning}
  Being finitely generated as $R$-module implies being finitely generated as an $R$-algebra. The converse is in general \emph{not true}: For example, the polynomial ring $R[\polvariable_1,\hdots,\polvariable_n]$ is not a finitely generated $R$-module.
\end{hwarning}

\section{Localization of Modules}
\begin{remdef}
  Let $M$ be an $R$-module and  $S\sse R$ be a multiplicative set. We can define an equivalence relation on $S\times M$ by
  \[
  (s,x)\erel (t,y)\text{ if there is a }u\in S\text{ such that }u\left(tx-sy\right) = 0.
  \]
  Denote by $\sloc M$ the set of equivalence classes of $\erel$, and by $x/s$ or $\frac{x}{s}$ the equivalence class $\eclas{(s,x)}$.\par
  We can define a $\sloc R$-module structure on $\sloc M$ by setting
  \[
  \frac{x}{s} + \frac{y}{t} \defined \frac{tx+sy}{st}
  \]
  and
  \[
  \frac{a}{s} \cdot \frac{y}{t} \defined \frac{ay}{st}.
  \]
  To show that this is well-defined requires $S$ to be multiplicative and is analogous to the localization of rings.
  \par
  The module $\sloc M$ is called the \emph{localization of $M$ by $S$}\index{module!localization}.
\end{remdef}
\begin{prop}
  Localization is functorial: Given a ring $R$ and a multiplicative subset $S$, we can define a functor
  \begin{eqnarray*}
    \sloc(-): \rmod & \longrightarrow & \slmod\\
    M & \longmapsto & \sloc M
  \end{eqnarray*}
  which sends an $R$-linear map $f:M\to N$ to the induced $\sloc R$-linear map
  \begin{eqnarray*}
    \sloc f:\sloc M& \longrightarrow & \sloc N\\
    \frac{x}{s} & \longmapsto & \frac{f(x)}{s}.
  \end{eqnarray*}
\end{prop}
\begin{proof}
  We need to show that $\sloc f$ is always a well-defined $\sloc R$-linear map. We only show well-definedness: Let $x/s = y/t$ in $\sloc M$. So there exists a $u\in S$ such that $u(tx-sy)=0$. As $f$ is $R$-linear, this implies $u\left(tf(x)-sf(y)\right)=0$ and hence $f(x)/s = f(y)/t$.
\end{proof}
\begin{nonumnotation}
  Let $M$ be an $R$-module, $a\in R$ an element and $\idp\in \spec R$ a prime ideal. Set $S\defined R\setminus \lset a^n\ssp n\geq 1\rset$. We use the following notation:
  \[
  M_{a}\defined \sloc M~\text{and}~M_{\idp}\defined \left( R\setminus \idp\right)^{-1}M.
  \]
\end{nonumnotation}

\begin{mprop}
  Let $M$ be an $R$-module. Then there is an $\sloc R$-linear isomorphism
  \begin{eqnarray*}
    \sloc M & \isomorphism & \sloc R \tensor_R M\\
    \frac{m}{s} & \longmapsto & \frac{1}{s}\tensor m.
  \end{eqnarray*}
\end{mprop}
\begin{proof}
  This is on Exercise Sheet 12.
\end{proof}



\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item A sequence of the form
    \[
    \begin{tikzcd}[sep = small, cramped]
      \hdots \ar{r}[above]{f_{i-1}}&
      M_{i-1}\ar{r}[above]{f_i}&
      M_i\ar{r}[above]{f_{i+1}}&
      M_{i+1}\ar{r}[above]{f_{i+2}}&
      \hdots
    \end{tikzcd},
    \]
    where the $M_i$ are $R$-modules and the $f_i$ are $R$-linear maps
    is called \emph{exact at $i$} if $\im f_i = \ker f_{i+1}$. We say this sequence is \emph{exact}\index{sequence! exact}\index{exact! sequence} if it is exact at every $i$.
    \item An exact sequence of the form
    \[
    \begin{tikzcd}[sep = small,cramped]
      0\ar{r}&
      M'\ar{r}&
      M\ar{r}&
      N\ar{r}&
      0
    \end{tikzcd}
    \]
    is a \emph{short-exact sequence}\index{sequence! short exact}.
  \end{enumerate}
\end{defn}
\begin{lem}
  \leavevmode
  \begin{enumerate}
    \item The sequence $\begin{tikzcd}[cramped, sep = small]0\ar{r}&M'\ar{r}[above]{f}&M\end{tikzcd}$ is exact if and only if $f$ is injective.
    \item The sequence $\begin{tikzcd}[cramped, sep = small]M\ar{r}[above]{g}&N\ar{r}&0\end{tikzcd}$ is exact if and only if $g$ is surjective.
    \item The sequence $\begin{tikzcd}[cramped, sep = small]0\ar{r}&M'\ar{r}[above]{h}&M\ar{r}&0\end{tikzcd}$ is exact if and only if $h$ is an isomorphism.
    \item Let $M'\sse M$ be a submodule. Then the sequence
    \[
    \begin{tikzcd}[sep = small, cramped]
      0\ar{r}&
      M'\ar[hook]{r}&
      M\ar[twoheadrightarrow]{r}&
      M/M'\ar{r}&
      0
    \end{tikzcd}
    \]
    is short-exact.
  \end{enumerate}
\end{lem}
\begin{mlem}\label{lec6:fg-ses}
  Let
  \[
  \begin{tikzcd}[sep = small, cramped]
    0\ar{r}&
    M'\ar{r}&
    M\ar{r}&
    M''\ar{r}&
    0
  \end{tikzcd}
  \]
  be a short-exact sequence of $R$-modules. Then:
  $M$ is finitely generated if and only if both $M'$ and $M''$ are.
\end{mlem}
\begin{proof}
  This is on Exercise Sheet 3.
\end{proof}

\begin{lem}\label{lec6:localization-is-exact}
  Let $S\sse R$ be a multiplicative set. Then the localization-functor $F:\rmod \to \slmod$ is exact.
\end{lem}
\begin{proof}
  $F$ is additive: Let $f,f':M\to N$ be two $R$-linear maps. Then
  \begin{align*}
  F\left(f+f'\right)\left(x/s\right) &= \frac{\left(f+f'\right)(x)}{s}\\
                                     &= \frac{f(x)+f'(x)}{s}\\
                                     &= \frac{f(x)}{s} + \frac{f'(x)}{s}\\
                                     &= F\left(f\right)\left(x/s\right)+ F\left(f'\right)\left(x/s\right).
  \end{align*}
  Let now $y/t\in \ker F(g)$, so $g(y)/t=0$. Now by the definition of the localization there is a $u\in S$ such that $ug(y) = 0$, and hence $uy\in \ker g$. As the original sequence is short exact, there is a $x\in M$ such that $f(x) = uy$. Then $F(f)\left(x/(ut)\right) = (uy)/(ut) = y/t$, so $y/t$ is in the image of $F(f)$.
  \end{proof}

\begin{cor}
  Let $N\sse M$ be a submodule of an $R$-module $M$. Then $\sloc\left(M/N\right) \cong \left(\sloc M\right)/\left(\sloc N\right)$.
\end{cor}
\begin{proof}
  Consider the short-exact sequence
  \[
  \begin{tikzcd}[sep = small, cramped]
    0\ar{r}&
    N\ar{r}&
    M\ar{r}&
    M/N\ar{r}&
    0
  \end{tikzcd}.
  \]
  Since localization is an exact functor, the sequence
  \[
  \begin{tikzcd}[sep = small, cramped]
    0\ar{r}&
    \sloc N\ar{r}&
    \sloc M\ar{r}&
    \sloc \left(M/N\right)\ar{r}&
    0
  \end{tikzcd}
  \]
  is exact too. Hence $\sloc M / \sloc N \cong \sloc \left(M/N\right)$.
\end{proof}

\begin{prop}
  Let $M$ be an $R$-module. Then the following are equivalent:
  \begin{enumerate}
    \item $M$ is the zero-module.
    \item $M_{\idp}$ is the zero-module for all $\idp \in \spec R$.
    \item $M_{\idm}$ is the zero-module for all $\idm \in \maxspec  R$.
  \end{enumerate}
\end{prop}
\begin{proof}
  This was on Exercise Sheet 4.
\end{proof}

\subsection{The Support of a Module}
Motivated by the above proposition, we make the following definition.
\begin{mdefn}
  Let $M$ be an $R$-module. The \emph{support}\index{support} of $M$ is the set
  \[
  \supp M \defined \lset \idp \in \spec R \ssp M_{\idp}\neq 0\rset.
  \]
\end{mdefn}
\begin{mprop}\label{lec6:supp-props}
  Let $R$ be a ring and $M$ an $R$-module. Then the following statements hold:
  \begin{enumerate}
    \item $M$ is non-zero if and only if $\supp M \neq \emptyset$.
    \item For every $a\in R$, we have $V(a)=\supp(R/\genby{a})$.
    \item Of
    \[
    \begin{tikzcd}[sep = small, cramped]
      0\ar{r}&
      M'\ar{r}&
      M\ar{r}&
      M''\ar{r}&
      0
    \end{tikzcd}
    \]
    is a short-exact sequence of $R$-modules, then \[\supp M \supp M' \cup \supp M''\].
    \item If $M = \bigoplus M_i$, then $\supp M = \bigcup \supp M_i$.
    \item If $M$ is finitely generated, then $\supp M = Z\left(\ann(M)\right)$. So in particular, $\supp M$ is a closed subset of $\spec R$.
    \item If $M$ and $N$ are finitely-generated $R$-modules, then \[\supp(M\tensor_R N) = \supp(M)\cap \supp(N)\].
  \end{enumerate}
\end{mprop}
\begin{proof}
  This is Exercise 3.19 in \cite{atiyah}.
\end{proof}

\lec
