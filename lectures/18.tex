\begin{lem}\label{lec18:intersection-of-primary-ideals}
  Let $\idp\in \spec R$ be a prime ideal and $I_1,I_2\sse R$ two $\idp$-primary ideals. Then the intersection $I_1\cap I_2$ is $\idp$-primary too.
\end{lem}
\begin{proof}
  It holds that $\sqrt{I_1\cap I_2} = \sqrt{I_1}\cap \sqrt{I_2} = \idp$.\par
  Furthermore, $I_1\cap I_2$ is a primary ideal: Let $ab\in I_1\cap I_2$. Then $a\in I_1$ or $b\in \idp$ and $a\in I_2$ or $b\in \idp$. So $a\in I_1\cap I_2$ or $b\in \idp$, showing that $I_1\cap I_2$ is indeed primary.
\end{proof}

\begin{defn}
  Let $I=I_1\cap \hdots \cap I_r$ be a primary decomposition, with $\idp_i\defined \sqrt{I_i}$. We say this decomposition is \emph{minimal}\index{primary decomposition! minimal} if the following two conditions are satisfied:
  \begin{enumerate}
    \item none of the $I_i$ is redundant: for all $1\leq i\leq r$ it holds that
    \[
    \bigcap_{i\neq j} I_j \not \subseteq I_i.
    \]
    \item The $\idp_i$ are pairwise different: $\idp_i\neq \idp_j$ holds for $i\neq j$.
  \end{enumerate}
\end{defn}

\begin{prop}
  If an ideal $I\sse R$ in an (arbitrary) ring $R$ has a primary decomposition, then $I$ has also a minmal primary decomposition.
\end{prop}
\begin{proof}
  Part i) is clear, and part ii) follows from \cref{lec18:intersection-of-primary-ideals}.
\end{proof}

\begin{notation}
  Let $N\sse M$ be a submodule and $m\in M$. We write
  \[
  N:m \defined \lset a\in R\ssp am \in N\rset.
  \]
\end{notation}
\begin{lem}
  In the above case, $N:m$ is an ideal of $R$.
\end{lem}
\begin{proof}
  This is immediate.
\end{proof}
\begin{lem}\label{lec18:primary-divisor}
  Let $I$ be a $\idp$-primary ideal. Then for all $a\in R$ it holds that $\sqrt{I:a} = R$ if $a\in I$ and $\sqrt{I:a} = \idp$ if $a\notin I$.
\end{lem}
\begin{proof}
  If $a\in I$, then $I:a = R$ and hence $\sqrt{I:a} = \sqrt{R} = R$.\par
  In the other case, let $b\in I:a$. Since $I$ is primary, $b\in \idp$ follows. Now $I\sse I:a \sse \idp$ and hence
  \[
  \idp = \sqrt{I}\sse \sqrt{I:a} \sse \sqrt{\idp},
  \]
  since the square root is monotonical.
\end{proof}
\begin{defn}
  Let $I\sse R$ be an ideal.
  \begin{enumerate}
    \item A prime ideal $\idp \in \spec R$ is \emph{associated to $I$}\index{prime ideal!associated} if there is an $a\in R$ such that $\idp = \sqrt{I:a}$. The set of prime ideals associated to $I$ is denoted by $\ass(I)$.
    \item The inclusion-minimal prime ideals in $\ass(I)$ are called \emph{isolated prime ideals of $I$}\index{prime ideal!isolated}, all others \emph{embedded prime ideals of $I$}\index{prime ideal!embedded}.
  \end{enumerate}
\end{defn}
\begin{prop}
  Let $I = I_1\cap \hdots \cap I_r$ be a minimal primary decomposition of $I$, with $\idp_i\defined \sqrt{I_i}$. Then $\lset \idp_1,\hdots,\idp_l\rset = \ass(I)$. So in particular, the number of primary ideals in the decomposition does not depend on the decomposition.
\end{prop}

\begin{proof}
  We first show $\idp_i\in \ass(I)$: Since the primary decomposition is minmal, there is an $a\in R$ such that $a\in \bigcap_{i\neq j}I_j$ and $a\notin I_i$ for an $1\leq i \leq r$. Now $I:a = (I_1:a)\cap\hdots\cap(I_r:a)$ and hence
  \begin{align*}
    \sqrt{I:a} &= \sqrt{(I_1:a)\cap \hdots \cap (I_r:a)}\\
               &= \sqrt{I_1:a}\cap \hdots \sqrt{I_r:a}\\
               &\overset{\ref{lec18:primary-divisor}}{=} R\cap \hdots \cap R\cap \sqrt{I_i:a}\cap R\cap \hdots \cap R\\
               &\sqrt{I_i:a} = \idp_i,
  \end{align*}
  so the inclusion \glqq $\sse$ \grqq{} follows.\par
  For the other direction, let $\idp \in \ass(I)$, so there is an $a\in R$ with $\idp  = \sqrt{I:a}$. Now
  \begin{align*}
    \idp &= \sqrt{I:a}\\
         &= \sqrt{I_1:a}\cap \hdots \cap \sqrt{I_r:a}.
  \end{align*}
  Now by prime avoidance (\cref{lec13:prime-avoidance}) we get $\idp = \sqrt{I_i:a}$ for an $1\leq i \leq r$, and hence (again by \cref{lec18:primary-divisor}) $\idp = \idp_i$ (since $\idp = R$ is not possible.)
\end{proof}
\begin{prop}
Let $R$ be a noetherian ring and $I\sse R$ an ideal.
\begin{enumerate}
  \item The isolated prime ideal of $I$ are precisley the minimal prime ideals over $I$.
  \item There are only finitely many minimal prime ideals over $I$.
\end{enumerate}
\end{prop}
\begin{proof}
  This is on Exercise Sheet 10.
\end{proof}

\begin{lem}\label{lec18:primary-ideals-and-localization}
  Let $S\sse R$ be multiplicative closed and $I$ a $\idp$-primary ideal. Denote by $\pphi:R\to \sloc R$ the canonical map into the localization.
  \begin{enumerate}
   \item If $S\cap \idp \neq \emptyset$, then $I\sloc R = \sloc R$.
   \item If $S\cap \idp = \emptyset$, then $(I\sloc R)\cap R = I$ and $I\sloc R$ is $\idp \sloc R$-primary.
  \end{enumerate}
\end{lem}
\begin{proof}
  If $S\cap \idp \neq \emptyset$, then there is a $s\in S$ with $s\in \idp = \sqrt{I}$. So $s^n\in I$ for a $n\geq 1$. Now
  \[
  \frac{1}{1} = \frac{s^n}{s^n} \in I\sloc R\]
  and hence $(I\sloc R)\cap R = R$.\par
  Assume now $S\cap \idp = \emptyset$ and let $a\in (I\sloc R)\cap R$. Then $a/1\in I\sloc R$ and hence there are $q\in I,s,n\in S$ such that $n(q-as) = 0$. Now $ans = nq\in I$.
  Since $I$ is $\idp$-primary, this implies $a\in I$ or $ns \in \idp$, which in this case means $a\in I$.
\end{proof}

\begin{prop}
  Let $I = I_1\cap \hdots \cap I_r$ be a minimal primary decomposition of $I$, with $\idp_i\defined \sqrt{I_i}$. If $\idp_i$ is minimal over $I$, then $\left(IR_{\idp_i}\right)\cap R = I_i$. In particular, the corresponding $I_i$ do not depend on the decomposition.
\end{prop}
\begin{proof}
  Let $S\sse R$ be a multiplicative set, then
  \begin{align*}
    (I_1\cap \hdots \cap I_r) \sloc R &= \left(I_1\sloc R\right)\cap \hdots \cap \left(I_r\sloc R\right),
  \end{align*}
  and hence
  \[
  \left(I\sloc R\right) = \bigcap_{i=1}^r \left(I_i\sloc R\right).
  \]
  Set now $S\defined R\setminus \idp_i$, such that $\idp_i$ is minimal over $I$. Then $S\cap \idp_i = \emptyset$ and $S\cap \idp_j \neq \emptyset$ for $i\neq j$, since $\idp_j\not \subseteq \idp_i$.
  Hence by \cref{lec18:primary-ideals-and-localization} we get
  \[
  \left(IR_{\idp_i}\right)\cap R = \left(I_iR_{\idp_i}\right)\cap R.
  \]
\end{proof}
\begin{rem}
  It is possible to defined primary decomposition in the more general context of modules:
  \begin{enumerate}
    \item Let $M$ be an $R$-module and $\idp\in \spec R$. We say that $\idp$ is associated to $M$ if there is an $m\in M$ such that $\idp = \ann m$ (Note that this does not coincide with the definition of an associated prime ideal for an ideal $I\sse R$, regarded as an $R$-module).
    \item We say that a submodule $N\sse M$ is primary if it has an associated primary ideal. It can be shown that every proper submodule $N$ has a decomposition $N = N_1\cap \hdots \cap N_r$ into primary submodules (if $R$ is noetherian).
    \item The uniqueness results are similar to the ones for ideals. (ToDo: do this in more detail).
  \end{enumerate}
\end{rem}

\chapter{Regular Rings}
\begin{mrem}
  Let $(R,\idm)$ be a regular noetherian ring. Then $R$ has finite Krull-dimension.
\end{mrem}
\begin{proof}
  The maximal ideal $\idm$ is an ideal of definition. Since $R$ is noetherian, $\idm$ is finitely generated, and hence $\dim R \leq \text{number of generators of }\idm$, by \cref{lec16:ideal-of-def-dimension}.
\end{proof}
\begin{nonumnotation}
  Let $(R,\idm)$ be a local ring. If not otherwise mentioned, we denote by $k\defined R/\idm$ the fraction field of $\idm$.
\end{nonumnotation}
\begin{lem}\label{lec18:quotient-regular}
Let $(R,\idm)$ be a local noetherian ring with $d = \dim R$.
\begin{enumerate}
  \item It holds that $d\leq \dim_k \idm/\idm$.
  \item We have $d = \dim_k \idm/\idm^2$ if and only if $\idm$ is generated by $d$ elements.
\end{enumerate}
\end{lem}
\begin{proof}
  This is on exercise sheet 9 (Hint: Use Nakayama.).
\end{proof}
\begin{defn}
\leavevmode
\begin{enumerate}
  \item Let $(R,\idm)$ be a local noetherian ring. We say $R$ is \emph{regular}\index{ring!regular}\index{regular!ring} if $d = \dim_k \idm/\idm^2$ holds.
  \item We say a noetherian ring $R$ is regular if all localization $R_{\idp}$ with $\idp\in \spec R$ are regular in the sense of i).
  \item Let $X$ be a variety. We say a point $a\in X$ is regular if $A(X)_{I(a)}$ is a regular local ring.
\end{enumerate}
\end{defn}
\begin{mrem}
\leavevmode
\begin{enumerate}
  \item Note that regular rings are \emph{by definition} noetherian.
  \item It is not clear that the definitions of regular rings are consistent (i.e. that for a regular local ring (in the sense of i)) the dimension equality is satisfied for all localizations at prime ideals). But this seems to be the case (\cite[00NN]{stacks-project}) or \cite[Page 33, Cor. 1]{franke-hom-alg}
\end{enumerate}

\end{mrem}
\lec
