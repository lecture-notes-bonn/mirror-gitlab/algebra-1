\begin{lem}\label{3: units maximal ideal}
  Let $\idm\subset R$ be a maximal ideal.
  If $1+x$ is a unit in $R$ for every $x\in \idm$, then $R$ is a local ring with maximal ideal $\idm$.
\end{lem}
\begin{proof}
  Let $b\in R\setminus \idm$. As $\idm$ is maximal, $\genby{b}+\idm = R$.
  So there are $a\in R,x\in \idm$ such that $ab+x=1$. Hence $ab = 1-x\in R\unts$, by assumption.
  But then $\genby{b} = 1$, and hence $b$ is a unit in $R$.
  The claim follows now from \cref{2: alternative characterisation of maximal ideals}.
\end{proof}

\begin{mlem}\label{3: maxideal field}
  Let $\pphi:R\to k$ be a surjective ring homomorphism, where $k$ is a field. Then $\ker \pphi$ is a maximal ideal of $R$.
\end{mlem}
\begin{proof}
  By \cref{1: quotient ring uniprop}, there is a unique isomorphism $R/\ker \pphi \cong k$. The claim now follows from \cref{lec1:primmax-alt-char}.
\end{proof}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item For every prime number $p$ and every $n\geq 1$, $R\defined \zz/p^n\zz$ is a local ring: The prime ideals in $R$ are in one-to-one, order preserving correspondence with the prime ideals in $\zz$ that contain $\genby{p^n}$. But the only prime ideal that contains $\genby{p^n}$ is $\genby{p}$. So $R$ has only one prime ideal, given by $\genby{\overline{p}}$, which has to be maximal.\par
    Note that for all $n>1$, $\zz/p^n\zz$ is a finite local ring, which is not an integral domain, so in particular not a field.
    \item Let $R$ be a local ring with maximal ideal $\idm$, and consider the \emph{ring of formal power series}\index{ring! of formal power series}\index{formal power series}
    \[
    R\powerseries \defined \lset \sum\iinf a_it^i\ssp a_i\in R\rset.
    \]
    There is a well-defined evaluation map
    \begin{eqnarray*}
      \eval: R\powerseries & \longrightarrow & R/\idm\\
      \sum\iinf a_i \polvariable^i & \longmapsto & \overline{a_0}
    \end{eqnarray*}
    with kernel $\ker\left(\eval\right) = \idm + \genby{\polvariable}$.
    As $\eval$ is surjective, \cref{3: maxideal field} implies that $\ker \left(\eval\right)$ is a maximal ideal of $R\powerseries$.\par
    It is also the only maximal ideal:
    Let $f\in \ker \left(\eval\right)$, so
    \[
    f = \sum\iinf a_i\polvariable^i
    \]
    with $a_0\in \idm$. Consider now $1+f$. We construct an inverse $g$ for $1+f$, so that the claim follows from \cref{3: units maximal ideal}:
    Let $g\in R\powerseries$ be a polynomial of the form
    \[
    g = \sum \iinf b_i\polvariable^i.
    \]
    The condition $(1+f)g = 1$ is equivalent to requiring $(1+a_0)b_0=1$ and
    \[
    \left(1+a_0\right)b_n+a_1b_{n-1}+ \hdots + a_n b_0 = 0\text{ for all }n>0.
    \]
    By \cref{1: existence of maximals},
     $1+a_0$ is a unit in $R$, so such a $b_0$ exists.
    Assuming that $b_0,\hdots b_{n-1}$ are already constructed, the equation
    \[
    \left(1+a_0\right)b_n+a_1b_{n-1}+ \hdots + a_n b_0 = 0
    \]
    can be re-written as
    \[
    b_n \defined -\left(1+a_0\right)^{-1}\cdot \left( a_nb_0+\hdots + a_1b_{n-1}\right),
    \]
    which is well-defined, as $(1+a_0)$ is a unit. By induction, we obtain an inverse $g$ for $1+f$.
    \item Consider $\rr^n$ with the standard topology, and let $X\sse \rr^n$ be an open subset with $0\in X$. We define an equivalence relation on the set of tuples
    \[
    \lset \left(U,f\right)\;\middle|\;
    \begin{tabular}{@{}l@{}}
      $U\sse X$ an open subset with $0\in U$,\\
      $f:U\to \rr$ continuous.
     \end{tabular}
     \rset,
     \]
     by setting
     $\left(U_1,f\right)\erel \left(U_2,g\right)$ if there is an open subset $W\sse X$ such that $0\in W$, $W\sse U_1\cap U_2$ and $\restrict{f}{W} = \restrict{g}{W}$.
     The equivalence class of $\left(U,f\right)$ is denoted by $\eclas{U,f}$ and is called a \emph{germ}\index{germ} at $0$:
     \begin{figure}[ht]
     \centering
     \begin{tikzpicture}
      \draw (-.5,0) circle (1.2);
      \draw (.5,0) circle (1.2);
      \draw[pattern = north east lines, opacity=.6] (0,0) circle (.45);
      \node at (0,-.7) {$W$};
      \node at (-1,0){$U_1$};
      \node at (1,0){$U_2$};
      \draw[fill = black] (0,0) circle (1.5pt);
     \end{tikzpicture}
     \end{figure}
     \par
     Consider now the set of all germs at $0$
     \[
     \sheaff_0\defined \lset \eclas{U,f}\;\middle|\;
     \begin{tabular}{@{}l@{}}
       $U\sse X$ an open subset with $0\in U$,\\
       $f:U\to \rr$ continuous.
      \end{tabular}
      \rset,
      \]
      which is called the \emph{stalk}\index{stalk} at $0$.
      We can define a ring structure on $\sheaff_0$ by setting
      \[
        \eclas{U_1,f_1} + \eclas{U_2,f_2} \defined \eclas{U_1\cap U_2,f_1+f_2}
      \]
      and
      \[
        \eclas{U_1,f_1} \cdot  \eclas{U_2,f_2} \defined  \eclas{U_1\cap U_2,f_1\cdot f_2},
      \]
      which is inherited from the pointwise ring structure on functions to $\rr$. Note that this is well-defined: If $\eclas{U_1,f_1} = \eclas{U_1',f_1'}$ such that $\restrict{f_1}{W_1} = \restrict{f_1'}{W_1}$ for an open subset $W_1\sse U_1\cap U_1'$ and $\eclas{U_2,f_2} = \eclas{U_2',f_2'}$ such that $\restrict{f_2}{W_2} = \restrict{f_2'}{W_2}$, then $f_1+g_1 = f_2+g_2$ and $f_1\cdot g_1 = f_2\cdot g_2$ on $W_1\cap W_2\sse U_1\cap U_2$.\par
      The stalk $\sheaff_0$ is a local ring:
      Consider the the ring homomorphism
      \begin{eqnarray*}
        \pphi:\sheaff_0&\longrightarrow&\rr\\
        \eclas{U,f}&\longmapsto&f(0).
      \end{eqnarray*}
      This is well-defined, as all functions in the germ $\eclas{U,f}$ agree on $0$.
      As $\pphi\left(\eclas{X,f = c}\right) = c$ for all $c\in \rr$, we see that $\pphi$ is surjective and hence $\ker \pphi$ is a maximal ideal  (\cref{3: maxideal field}). \par Let now $\eclas{U,f}\in \ker \pphi$.
      As $f$ is continuous, there is an open neighbourhood $W$ of $0$ such that $1+f(x)\neq 0$ for all $x\in W$.
      Hence $\eclas{W,1/(1+f)}$ is the unit element of $\sheaff_0$. The claim now follows from \cref{3: units maximal ideal}.
  \end{enumerate}
\end{example}

\begin{mrem}
  Germs and stalks can be defined in the more general context of \emph{(pre-)sheaves}. The example we considered is for the \emph{sheaf of continous functions} $\rr^n\to \rr$. For more, the reader is refered to \cite[Chapter~2]{FOAG}.
\end{mrem}
\begin{mrem}
  Here are some more facts on $R[[\polvariable]]$ for a general ring $R$:
  \begin{enumerate}
      \item As $R$-modules the map
      \[
        R[[t]] \to \prod_{\nn}R,\qquad
        \sum_{i=0}^{\infty}a_it^i  \mapsto  \left(a_i\right)_{i\in \nn}
      \]
      is a well-defined isomorphism. It is a classical (non-trivial) result that the infinite product $\prod_{\nn}\zz$ is not a free.
      \item The units in $R[[\polvariable]]$ are of the form $a_0+\hdots$, where $a_0$ is a unit in $R$. This similar to the case where $R$ is local.
      \item So we can still describe the maximal ideals $\maxspec R[[\polvariable]]$: by ii), every maximal ideal necessarily contains $\polvariable$ and hence corresponds to a maximal ideal of $R[[\polvariable]]/\genby{\polvariable}\cong R$. So we have
      \[
      \maxspec R[[\polvariable]] =
      \lset
      \idm + \genby{\polvariable}\ssp \idm \in \maxspec R
      \rset.
      \]
      Since for any ideal $I\sse R$ the maximal ideals over $I+\genby{\polvariable}$ are precisley of the form $\idm + \genby{t}$ for the maximal ideals $\idm$ over $I\sse \idm$ the assignment
      \begin{eqnarray*}
        \maxspec R[[\polvariable]] & \longleftrightarrow & \maxspec R\\
        \idm & \longmapsto & \idm \cap R\\
        \idm + \genby{\polvariable} & \longmapsfrom & \idm
      \end{eqnarray*}
      is a homeomorphism (where we equip $\maxspec R[[\polvariable]]$ and $\maxspec R$ with the subspace topology).
  \end{enumerate}
\end{mrem}
The following is a recollection of basic facts about rings of fractions. More details can be found in \cite[5.17]{schroer}.

\begin{defn}
  A subset $S\sse R$ is called \emph{multiplicative}\index{subset!multiplicative} if
  \begin{enumerate}
    \item $1\in S$;
    \item for all $a,b\in S$ it holds that $ab\in S$.
  \end{enumerate}
\end{defn}

\begin{remdef}
  Let $S\sse R$ be a multiplicative set. We can define an equivalence relation on $S\times R$ by
  \[
  (s,a)\erel (t,b)\text{ if there is a }u\in S\text{ such that }u\left(ta-sb\right) = 0.
  \]
  Denote by $S^{-1}R$ the set of equivalence classes of $\erel$, and by $a/s$ or $\frac{a}{s}$ the equivalence class $\eclas{(s,a)}$.\par
  We can define a ring structure on $S^{-1}R$ by setting
  \[
  \frac{a}{s} + \frac{b}{t} \defined \frac{at+bs}{st}
  \]
  and
  \[
  \frac{a}{s} \cdot \frac{b}{t} \defined \frac{ab}{st}.
  \]
  To show that this is well-defined requires $S$ to be multiplicative and some work, so we will not do this here.
  \par
  The ring $\sloc R$ is called the \emph{ring of fractions with respect to $S$}\index{ring! of fractions}. Mostly, $S$ will be left implicit, so that we refer to $\sloc R$ only as \emph{the ring of fractions}.
\end{remdef}

\begin{lem}
  \leavevmode
  \begin{enumerate}
  \item The map
  \begin{eqnarray*}
    \locmap: R&
    \longrightarrow &
    \sloc R\\
    a &
    \longmapsto &
    \frac{a}{1}
  \end{eqnarray*}
  is a ring homomorphism. The elements of $S$ are invertible in $\sloc R$: $\locmap\left(S\right) \sse \left(\sloc R\right)\unts$.
  \item If $R$ has no zero-divisors, then $\sloc R$ has no zero-divisors too.
  \item If $S$ has no zero-divisors, then $\locmap$ is injective.
  \end{enumerate}
\end{lem}
\begin{proof}
  Ommited.
\end{proof}

\begin{prop}
  Let $S\sse R$ be a multiplicative subset and $g:R\to R'$ a ring homomorphism such that $g\left(S\right)\sse \left(R'\right)\unts$. Then $g$ factors over the ring of fractions: there is a unique ring homomorphism $g':\sloc R\to R'$ such that
  \[
  \begin{tikzcd}
    R\ar{r}[above]{g}\ar{d}[left]{\locmap}&
    R'\\
    \sloc R\ar[dashed]{ur}[below right]{g'}
  \end{tikzcd}
  \]
  commutes.
\end{prop}


\begin{proof}
  Ommited.
\end{proof}

\begin{example}
  \leavevmode
  \begin{enumerate}
    \item Let $S = \lset 1 \rset$. Then $\sloc R \cong R$.
    \item Let $0\neq a\in R$ be an element and
    \[
    S \defined \lset a^n \ssp n>0\rset.
    \]
    Then $R_a\defined \sloc R$ is called the \emph{localization at $a$}\index{localization! at an element}.
    \item Let $\idp$ be a prime ideal and $S\defined R\setminus \idp$. Then $S$ is a multiplicative subset. Then $R_{\idp} \defined \sloc R$ is the \emph{localization at $\idp$}\index{localization! at a prime ideal}.
  \end{enumerate}
\end{example}
\begin{example}
    Consider the case $R=\zz$. Then for any prime number $p$, the localization at $p$ is given by
    \[
    \zz_p  =
    \lset
    \frac{a}{p^n}\ssp a\in \zz,n\geq 0\rset.
    \]
    The localization at a prime ideal $\idp = \langle p\rangle$ however is given by
    \[
    \zz_{\idp} =
    \lset
    \frac{a}{b} \ssp a,b\in \zz,~p\text{ does not divide }b\rset.
    \]
    Note that $\zz_p$  and $\zz_{\idp}$ are different from each other; both are extremly different from $\zz/p\zz$!
\end{example}
\begin{mlem}
  For an element $x\in R$, the following are equivalent:
  \begin{enumerate}
    \item $x=0$,
    \item $\locmap(x)=0$ for all prime ideals $\idp\in \spec R$,
    \item $\locmap(x)=0$ for all maximal ideals $\idm \in \maxspec R$.
  \end{enumerate}
\end{mlem}
\begin{proof}
  Ommited.
\end{proof}
\begin{defn}
  Let $\pphi:R\to R'$ be a ring homomorphism.
  \begin{enumerate}
    \item Let $J\sse R'$ be an ideal. We denote by
    \[
    J\cap R \defined \pphi\inv (J) \sse R
    \]
    the \emph{contraction} of $J$ by $\pphi$\index{contraction}\index{ideal! contraction of}.
    \item Let $I\sse R$ be an ideal. We denote by
    \[
    IR' \defined \genby{\pphi(I)}\sse R'
    \]
    the \emph{extension} of $I$ by $\pphi$.
  \end{enumerate}
  In both cases, the map $\pphi$ is often left implicit.
\end{defn}

\begin{lem}\label{3:ext-cont-basic-properties}
  Let $\pphi:R\to R'$ be a ring homomorphism.
  \begin{enumerate}
    \item Extensions and contractions of ideals by $\pphi$ are again ideals.
    \item For all ideals $I\sse R$ and $J\sse R'$, $I\sse \left(IR'\right)\cap R = I$ and $J\supseteq \left(J\cap R\right)R' = J$ holds.
  \end{enumerate}
\end{lem}

\begin{theorem}\label{3: ideals under localization}
  Let $S\sse R$ be a multiplicative subset.
  \begin{enumerate}
    \item If $I\sse R$ is an ideal, then
    \[
    I \left(\sloc R \right)= \lset \frac{a}{s}\ssp a\in I,s\in S\rset .
    \]
    \item If $I\sse R$ is an ideal, then
    \[
    I\left(\sloc R\right)\cap R = \lset a\in R \ssp \text{there is a }n\in S\text{ such that }na\in I\rset.\]
    \item If $J\sse \sloc R$ is an ideal, then
    \[
    \left( J\cap R\right) \sloc R = J.
    \]
    \item If $\idp$ is a prime ideal in $R$ with $\idp \cap  S= \emptyset$, then $\idp \left(\sloc R\right)$ is a prime ideal in $\sloc R$.
    \item The maps
    \begin{eqnarray*}
      \spec \sloc R & \longleftrightarrow & \lset \idp\in \spec R \ssp \idp\cap S = \emptyset \rset \\
      \idq & \longmapsto & \idq \cap R\\
      \idp \left(\sloc R\right) & \longmapsfrom & \idp
    \end{eqnarray*}
    are mutually inverse and preseve inclusions.
  \end{enumerate}
\end{theorem}
\begin{mrem}
  The statement in v) is actually stronger: If we consider the set
  \[
  \lset \idp \in \spec R\ssp \idp\cap S = \emptyset\rset
  \]
  with the subspace topology in $\spec R$, then the map $\idq\mapsto \idq \cap R$ is actually a homeomorphism.
\end{mrem}
\begin{proof}
  Ommited.
\end{proof}
\begin{mrem}
  In \cref{3: ideals under localization}, all statements about contractions and extensions of ideals are with respect to the canonical inclusion $\locmap:R\to \sloc R$.
\end{mrem}
\begin{proof}[Proof of \cref{3: ideals under localization}]
\leavevmode
  \begin{enumerate}
    \item Let $a/s\in \sloc R$ with $a\in I$ and $s\in S$. Then
    \begin{align*}
    \frac{a}{s} &= \frac{a}{1}\cdot \frac{1}{s} \\
                &= \locmap(a)\cdot \frac{1}{s},
    \end{align*}
    which is in $I\left(\sloc R\right)$, as $\locmap(a)$ is.\par Let now $x\in I\left(\sloc R\right)$, so there are $a_i \in I,b_i\in R$ and $s_i\in S$ such that
    \[
    x = \sum_i \frac{b_i}{s_i}\cdot\frac{a_i}{1} .
    \]
    Set
    \[
    s\defined \prod_{i}s_i\text{ and }a\defined \sum_i b_i\left(\prod_{i\neq j}s_ja_j\right).
    \]
    Then $s\in S$, as $S$ is a multiplicative set, and $a\in I$, as $I$ is an ideal. As $x = a/s$, the claim follows.
    \item Let $a\in \left(I\sloc R\right)\cap R$. Then $a/1\in I\left(\sloc R\right)$. We now have the following chain of equivalences $a\in I\left(\sloc R\right)$ if and only if
    \begin{align*}
      {}& \text{ there are }b\in I, t\in S, \text{ such that }\frac{a}{1} = \frac{b}{t}\\
                                \iff{}& \text{ there are }b\in I \text{ and } s,t\in S \text{ with } s(ta-b)=0\\
                                \iff{}& \text{ there is a }n\in S\text{ such that }na\in I,
    \end{align*}
    where the first equivalence follows from i), the second from the definition of the localization and the third from the following argument:\par If there are such $b,t$ and $s$, then $(st)a = sb$. But $n\defined st$ is in $S$ (as $s$ and $t$ are) and $sb$ is in $I$, as $b$ is. On the other hand, if there is a $n\in S$ such that $b\defined na\in I$, then for $t\defined n$ and $s\defined 1$ we have $1\cdot(na-na) = 0$.
    \item $\left(J\cap R\right)\sloc R\sse J$ is always true (\cref{3:ext-cont-basic-properties}). For the other inclusion, let $x=a/s\in J$. Then
    \[
    \frac{a}{1} = \frac{s}{1}\cdot \frac{a}{s}
    \]
    and hence $a\in J\cap R$. So $a/s\in \left(J\cap R\right)\sloc R$, by i).
    \item Let $\idp\in \spec R$ be a prime ideal with $\idp\cap S = \emptyset$, and let $a/s,b/t\in \sloc R \setminus \idp \left(\sloc R\right)$. If $(ab)/(st)\in \sloc R \setminus \idp \left(\sloc R\right)$. Then there are $c\in p, u\in S$ such that $(ab)/(st) = c/u$ (by i). By the definition of the localization, there is now a $v\in S$ such that
    \[
    v\left(uab-stc\right) = 0.
    \]
    So $\left(vu\right) ab =  (vst)c\in \idp$, as $\idp$ is an ideal. But since $ab\notin \idp$ this implies $vu\in \idp \cap S$, contradicting $\idp \cap S=\emptyset$.
  \end{enumerate}
\end{proof}

\begin{cor}\label{lec3:rp-is-local-ring}
  Let $\idp \sse R$ be a prime ideal.
  \begin{enumerate}
    \item There is a bijection
    \[
    \begin{tikzcd}
      \spec R_{\idp} \ar{r}& \lset \idp'\in \spec R \ssp \idp' \cap \left(R \setminus \idp\right) = \emptyset \rset = \lset \idp'\in \spec R\ssp \idp' \subseteq \idp\rset.
    \end{tikzcd}\]
    \item $R_{\idp}$ is a local ring with maximal ideal $\idp R_{\idp}$.
  \end{enumerate}
\end{cor}
\begin{cor}
  The map
  \[
  \begin{tikzcd}
    \spec \locmap: \spec \sloc R \ar{r}&\spec R
  \end{tikzcd}\]
  is injective, with image
  \[
  \lset \idp \in \spec R \ssp \idp\cap S = \emptyset \rset.
  \]
\end{cor}

\begin{defn}
  For an element $a\in R$ we denote by $D(a)\defined \spec R \setminus Z\left(\genby{a}\right)$ the \emph{prinicipal open subset associated to $a$}.
\end{defn}
\begin{mcor}
  Let $a\in R$. Then there is a homeomorphism
  \begin{eqnarray*}
    D(a) & \isomorphism & \spec R_a\\
    \idp & \longmapsto & \idp \cdot R_a\\
    \idq & \longmapsfrom & \idq
  \end{eqnarray*}
\end{mcor}
\begin{proof}
  This is just a special case of \cref{3: ideals under localization}.
\end{proof}
\begin{mrem}
  The principal open subsets form a \emph{basis}\index{topology! basis of}\index{basis!of a topology} for the Zariski topology: for every open subset $U\sse \spec R$ and a point $x\in U$ there is a principal open subset $D(a)$ such that $x\in D(a) \sse U$.
\end{mrem}
\lec
