\begin{mlem}\label{lec19:mvecspace-is-zero}
  Let $(R,\idm)$ be a local noetherian ring. Then $R$ is a field if and only if $\dim_k \idm/\idm^2 = 0$.
\end{mlem}
\begin{proof}
  The one direction is clear. If, on the other hand, $\dim_k \idm/\idm^2 = 0$, then this is equivalent to $\idm = \idm^2$.
  By Nakayama (\cref{5:nakayama-classical}), $\idm = 0$ follows and hence $R$ is a field.
\end{proof}
\begin{mlem}\label{lec19:dim-of-quotient}
Let $(R,\idm)$ be a local noetherian ring and $f\in \idm$. Then $\dim R/\genby{f} \geqq \dim R -1$. If $f$ is not contained in any of the minimal prime ideals of $R$ then equality holds.
\end{mlem}
\begin{proof}
  Let $\overline{x_1},\hdots,\overline{x_d}$ be elements in $R/\genby{f}$ such that $\genby{\overline{x_1},\hdots,\overline{x}_d}$ is an ideal of definition and $d=\dim R/\genby{f}$ (These exist, by \cref{lec16:ideal-of-def-dimension}, ii)).
  Then $\genby{f,x_1\hdots,x_{d}}$ is an ideal of definition of $R$, and hence \[\dim R \leq \dim R/\genby{f}+1\] (by \cref{lec16:ideal-of-def-dimension}, i)).\par
  If $f$ is not contained in any minimal prime ideal of $R$, then every chain in $R/\genby{f}$ can be lifted to chain of prime ideals which is at least one prime ideal away from being maximal, and hence equality follows.
\end{proof}
\begin{mlem}\label{lec19:quotient-is-regular}
  Let $(R,\idm)$ be a regular local ring and $f\in \idm \setminus \genby{0}$.
  \begin{enumerate}
    \item Set $\overline{R}\defined R/\genby{f}$, $\overline{\idm} \defined \idm/\genby{f}$ and $\overline{k}\defined \overline{R}/\overline{\idm}$. Then
    \[
    \dim_{\overline{k}} \overline{\idm}/\overline{\idm}^2=
    \begin{cases}
        \dim R, & \text{if }f\in \idm^2\\
        \dim R - 1,
         & \text{if }f\notin \idm^2
    \end{cases}.
  \]
  \item Assume that $\dim \overline{R} = \dim R - 1$. Then $\overline{R}$ is regular if and only if $f\notin \idm^2$.
  \item If $f\notin \idm^2$, then $\dim \overline{R} = \dim R - 1$ and $\overline{R}$ is regular.
  \end{enumerate}
\end{mlem}
\begin{proof}
  The canonical short-exact sequence
  \[
  \begin{tikzcd}[sep = small, cramped]
  0\ar{r}&
  \genby{f}\ar[hook]{r}&
  \idm \ar[twoheadrightarrow]{r}&
  \overline{\idm}\ar{r}&
  0,
  \end{tikzcd}
  \]
  where $\overline{\idm}\defined \idm /\genby{f}$, now induces the following big commutative square:
  \[
  \begin{tikzcd}[sep = small, cramped]
  &
  0\ar{d}&
  0\ar{d}&
  0\ar{d}&\\
  0\ar{r}&
  \genby{f}\cap \idm^2\ar{r}\ar{d}&
  \idm^2 \ar{r}\ar{d}&
  \overline{\idm}^2\ar{r}\ar{d}&
  0\\
  0\ar{r}&
  \genby{f}\ar{r}\ar{d}&
  \idm \ar{r}\ar{d}&
  \overline{\idm}\ar{r}\ar{d}&
  0\\
  0\ar{r}&
  \genby{f}/\left(\genby{f}\cap \idm^2\right)
  \ar{r}\ar{d}&
  \idm /\idm^2\ar{r}\ar{d}&
  \overline{\idm}/\overline{\idm}^2\ar{r}\ar{d}&
  0\\
  &
  0&
  0&
  0&
  \end{tikzcd}
  \]
  Now all three columns and the two upper rows are exact, and hence the lower one is too (by the 9-lemma). So it is in particular exact as sequence of $k$-vector spaces and hence
  \[
  \dim_k \idm/\idm^2 =  \dim_k \genby{f}/\left(\genby{f}\cap \idm^2\right) + \dim_{\overline{k}} \overline{\idm}/\overline{\idm}^2
  \]
  holds (Note that $\idm/\idm^2$ is finite-dimensional, since $R$ is noetherian. Furthermore, $\overline{R}/\overline{\idm} \cong R/\idm$, by the third isomorphism theorem.).\par
  Since $R$ is regular, we have $\dim R = \dim_k \idm/\idm^2$ and hence
  \begin{align*}
    \dim_{\overline{k}} \overline{\idm}/\overline{\idm}^2 &= \dim_k \idm/\idm^2 - \dim_k \genby{f}/\left(\genby{f}\cap \idm^2\right)\\
    &= \dim R - \dim_k \genby{f}/\left(\genby{f}\cap \idm^2\right).
  \end{align*}
  As $\dim_k \genby{f}/\left(\genby{f}\cap \idm^2\right)\leq 1$ and $\dim_k \genby{f}/\left(\genby{f}\cap \idm^2\right) = 0$ if and only if $f \in \idm^2$, then $\dim \overline{R} = \dim R - 1$ implies that $\overline{\idm}/\overline{\idm}^2$ is regular if and only if $f\notin \idm^2$.
  This shows i) and ii).\par

  For iii), note that $\dim \overline{R} \leq \dim_{\overline{k}}\overline{\idm}/\overline{\idm}^2 = \dim R-1$, by i) and \cref{lec18:quotient-regular}.
  Furthermore, by \cref{lec19:dim-of-quotient}, we have $\dim \overline{R}\geq \dim R - 1$. Hence equality follows, and $\overline{R}$ is regular by ii).
\end{proof}
\begin{mcor}
  Let $(R,\idm)$ be a local noetherian integral domain and $f\in \idm \setminus\genby{0}$. Then $(R/\genby{f},\idm)$ is a regular local ring if and only if $f\notin \idm^2$.
\end{mcor}
\begin{proof}
  By \cref{lec19:dim-of-quotient}, $\dim R/\genby{f} = \dim R -1 $ holds (since $f\neq 0$ and $\minspec R = \lset \genby{0} \rset$). The claim now follows directly from \cref{lec19:dim-of-quotient}.
\end{proof}

\begin{prop}\label{lec19:regularlocal-implies-integral}
  Every regular local ring is an integral domain.
\end{prop}
\begin{proof}
  We will do this by induction on $n\defined \dim R$. The case $n = 0$ is \cref{lec19:mvecspace-is-zero}, which applies since $R$ is regular.\par
  In the general case $n>0$, we will show that $\genby{0}$ is prime: Denote by $\idp_1,\hdots,\idp_r$ the minmal prime ideals over $\genby{0}$.
  \begin{claim}
    We have $\idm\not \sse \idm^2\cup\idp_1\cup\hdots\cup\idp_r$.
  \end{claim}
  \begin{claimproof}
    If $\idm \sse \idm^2\cup\hdots\cup\idp_r$, then by Prime Avoidance (\cref{lec13:prime-avoidance}), we have $\idm \sse \idm^2$ or $\idm \sse \idp_i$ for an $1\leq i \leq r$.
    If $\idm^2 = \idm$, then $\dim R = 0$ follows, since $R$ is regular.\par
    So assume $\idm = \idp_i$. Then already $\idm = \idp_j$ for all $1\leq j\leq r$ follows, and hence $\minspec R = \maxspec R = \idm$ follows and hence $\dim R = 0$.
  \end{claimproof}
  So there is a $a\in \idm$ with $a\notin \idm^2,\idp_1,\hdots,\idp_r$.
  By \cref{lec19:quotient-is-regular} iii), we have that $\overline{R}\defined R/\genby{a}$ is regular with $\dim \overline{R} = \dim R-1$.
  The induction hypothesis now implies that $\genby{a}$ is prime and hence there is an $1\leq i \leq r$ such that $\idp_i \sse \genby{a}$.
  As $a\notin \idp_i$ and $a\in \idm$, we have $\idm \idp_i = \idp_i$, which implies $\idp_i = \genby{0}$ (by Nakayama, \cref{5:nakayama-classical}).
\end{proof}


\section{Valuation Rings}

\begin{lem}\label{lec19:dvr-power-in-maximal}
  Let $(R,\idm)$ be a $1$-dimensional regular local ring. Then:
  \begin{enumerate}
    \item The maximal ideal $\idm$ is a principal ideal.
    \item For every non-zero $a\in R$ there is a unique $n\geq 1$ such that $\genby{a} = \idm^n$.
  \end{enumerate}
\end{lem}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Since $\dim_k \idm/\idm^2 = 1$, $\idm$ is generated by one element (\cref{lec18:quotient-regular},ii)).
    \item Since $R$ is an integral domain (\cref{lec19:regularlocal-implies-integral}), $\genby{0}$ is the only minimal prime ideal and $R$ being 1-dimensional implies $\spec R = \lset \genby{0}, \idm \rset$.
    So for every non-zero $a\in R$, $\genby{a}$ is an ideal of definition, and hence there is a minimal $n\geq 1$ such that $\idm^n \sse \genby{a}$ (\cref{lec16:defintionideal}).
    By i), $\idm = \genby{t}$ for a $t\in R$ and hence there is a $b\in R$ such that $t^n=ba$. If $b\in \idm$, then there is a $b'$ such that $b = b't$ and hence $t^n = b'ta$ which would imply $t^{n-1} = b'a$, contradicting the minimality of $n$.
    So $b$ is a unit, and hence $\idm^n = \genby{a}$.
  \end{enumerate}
\end{proof}

\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item A \emph{totally ordered group}\index{group!totally ordered} is an abelian group $(G,+)$ with a total order $\leq$ such that for all pairs $m\leq n$ and $k\in G$ already $m+k\leq n+k$ follows.
    \item Let $G$ be a totally ordered group. We extend the ordering and group strucutre on $G$ to the set $G\cup \lset \infty \rset$ by setting $a\leq \infty$ and $a+\infty\defined \infty + a\defined \infty$  for all $a\in G$.
    \item Let $K$ be a field and $G$ a totally ordered group. A \emph{valution on $K$}\index{valuation} is a group homomorphism $\nu:K\unts\to G$ such that \[\nu(a+b)\geq \min\lset \nu(a),\nu(b)\rset\] if $a+b\neq 0$. We extend $\nu$ to $K$ be setting $\nu(0)\defined \infty$.
    \item Let $\nu:K\unts\to G$ be a valuation on $K$. Then \[R_{\nu}\defined \lset a\in K\ssp \nu(a)\geq 0\rset\] is called the \emph{valuation ring of $\nu$}\index{valution!ring}.
    \item The subgroup $\nu\left(K\unts\right)\sse G$ is called the \emph{valuation subgroup of $\nu$} or \emph{value group}\index{valution!subgroup}.
  \end{enumerate}
\end{defn}
\begin{proof}
  The valuation ring $R_{\nu}$ is indeed a ring: We have $\nu(0)= \infty \geq 0$ (by definition), $\nu(1) = 0$ (since $\nu$ is a group homomorphism) and for all $a,b\in K\unts$ it holds that $\nu(a+b) \geq \min\lset 0,0\rset =0 $ and $\nu(ab) = 0+0=0$.
\end{proof}
\begin{nonumnotation}
  More generally, we say that a ring $R$ is a valuation ring if there is a field $K$ and a valuation $\nu:K\to G$ such that $R = R_{\nu}$.
\end{nonumnotation}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item On every field there is the trivial valuation $K\unts\to \lset 0\rset$.
    \item Let $R$ be a factorial ring, set $K\defined \quot R$ and let $p$ be a prime element of $R$. Now for every element $b\in R$, there is a $a\in R$ and a unique maximal $m\geq 0$ such that $b = ap^m$. So every element $b'\in K$ has a unique decomposition of the form $b' = a'p^n$ with $n\in \zz$, such that $a'$ is quotient of two elements from $R$ that are both not divisible by $p$.
    Define a map $\nu:K\unts \to \zz,~ap^n\mapsto n$. Then this is a valuation. The valuation ring is given by
    \[
    R_{\nu} = \lset ap^n\ssp a\in K\unts,~n\geq 0,~p\nmid a\rset \cup \lset 0\rset = R_{\genby{p}},
    \]
    and the value group is given by $\zz$.
    \item Let $K$ be a field and consider the field
    \[
    L\defined \lset  \sum_{n\in \zz}a_n\polvariable^n\ssp a_n\in K,~\lset n\in \zz\ssp a_n\neq 0\rset\text{ has a lower bound}\rset.
    \]
    Then
    \begin{eqnarray*}
      L\unts & \longrightarrow & \zz \\
      \sum a_n\polvariable^n &\longmapsto & \min\lset n\in \zz\ssp a_n\neq 0\rset
    \end{eqnarray*}
    is a valuation on $L$, with valuation ring $K[[\polvariable]]$. Note that this is a special case of ii), with $R = K[[\polvariable]]$.
  \end{enumerate}
\end{example}

\lec
