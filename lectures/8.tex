\begin{lem}[Lying Over]\label{lec8:lying-over}\index{lying over}
  Let $\pphi:R\inarr R'$ be an integral extension of $R$.
  Then for all prime ideals $\idp\in \spec R$ there is a prime ideal $\idq\in \spec R'$ such that $\idq \cap R = \idp$,
  i.e. the induced morphism $\spec \pphi:\spec R'\to \spec R$ is surjective.
\end{lem}
\begin{proof}
  Let $\idp \in \spec R$ be a prime ideal. Consider the commutative diagram
  \[
  \begin{tikzcd}
    R\ar[hookrightarrow]{rr}[above]{\pphi,~\text{integral}}\ar{d}[left]{\eta}&
    &
    R'\ar{d}[right]{\eta'}\\
    R_{\idp}\ar[hookrightarrow]{rr}{\pphi',~\text{integral}}&
    &
    R'_{\idp}
  \end{tikzcd}
  \]
  We have that $\pphi'$ is injective, as localization is exact (\cref{lec6:localization-is-exact}).
  So in particular, $R'_{\idp}\neq 0$ and there is a maximal ideal $\idn\sse R'_{\idp}$.
  Since $\pphi'$ is an integral extension $\idn\cap R_{\idp}$ is again maximal (\cref{lec7:maximal-under-preimage}).\par
  By \cref{lec3:rp-is-local-ring}, $R_{\idp}$ is a local ring with maximal ideal $\idp R_{\idp}$ and thus $\idn \cap R_{\idp} = \idp R_{\idp}$.
  Set $\idq \defined \idn\cap R'\sse R'$.
  Then:
  \begin{align*}
    \idq \cap R &= \left(\idn\cap R'\right) \cap R\\
                &= \left(\idn \cap R_{\idp}\right) \cap R\\
                &= \left(\idp R_{\idp}\right)\cap R\\
                &= \idp.
  \end{align*}

\end{proof}

\begin{defn}
  Let $\pphi:R\to R'$ be an algebra.
  \begin{enumerate}
    \item We say that $\pphi$ satisfies \emph{going up}\index{going up} if given a chain of prime ideals
    \[
    \idp_1\sse \idp_2\sse\hdots\sse \idp_n
    \]
    in $R$
    and a chain of prime ideals
    \[
    \idq_1\sse \idq_2\sse \hdots \idq_m
    \]
    in $R'$
    with $m\leq n$ and $\idp_i = \idq_i\cap R$ for all $1\leq i\leq m$ there are prime ideals $\idq_{m+1},\hdots,\idq_n$ such that the following holds:
    \begin{itemize}
      \item $\idp_i = \idq_i \cap R$ for all $m+1\leq i \leq n$; and
      \item the ideals $\idq_{m+1},\hdots,\idq_n$ fit into the chain
      \[
      \idq_1\sse \hdots\sse \idq_{m+1}\sse \hdots \sse \idq_n.
      \]
    \end{itemize}
    \item We say that $\pphi$ satisfies \emph{going down}\index{going down} if given a chain of prime ideals
    \[
    \idp_1\supseteq \idp_2\supseteq\hdots\supseteq \idp_n
    \]
    in $R$ and a chain of prime ideals
    \[
    \idq_1\supseteq \idq_2\supseteq \hdots \idq_m
    \]
    with $m\leq n$ and $\idp_i = \idq_i\cap R$ for all $1\leq i\leq m$ there are prime ideals $\idq_{m+1},\hdots,\idq_n$ such that the following holds:
    \begin{itemize}
      \item $\idp_i = \idq_i \cap R$ for all $m+1\leq i \leq n$; and
      \item the ideals $\idq_{m+1},\hdots,\idq_n$ fit into the chain
      \[
      \idq_1\supseteq \hdots\supseteq \idq_{m+1}\supseteq \hdots \supseteq \idq_n.
      \]
    \end{itemize}
  \end{enumerate}
\end{defn}
\begin{lem}
  Let $\pphi:R\to R'$ be a ring homomorphism.
  \begin{enumerate}
    \item The following are equivalent:
    \begin{enumerate}[label = \alph*)]
      \item $\pphi$ satisfies going down.
      \item For all prime ideals $\idq\in \spec R'$ and $\idp \defined \idq \cap R$, the induced map  $\spec R'_{\idq}\to \spec R_{\idp}$ is surjective.
    \end{enumerate}
    \item The following are equivalent:
    \begin{enumerate}[label = \alph*)]
      \item $\pphi$ satisfies going up.
      \item For all prime ideals $\idq\in \spec R'$ and $\idp \defined \idq \cap R$, the induced map $\spec(R/\idp)\to \spec(R'/\idq)$ is surjective.
      \item The induced map $\pphi^{\#}:\spec R'\to \spec R$ is closed: images of closed sets in $\spec R'$ under $\pphi^{\#}$ are closed in $\spec R$.
    \end{enumerate}
  \end{enumerate}
\end{lem}
\begin{proof}
  This will be on the 5th exercise sheet.
\end{proof}

\begin{theorem}[Going Up for Integral Extensions]\label{lec8:going-up-int-ext}
  Let $\pphi:R\inarr R'$ be an integral extension. Then $\pphi$ satisfies going up.
\end{theorem}
\begin{proof}
  Let $\idp_1,\idp_2\in \spec R$ and $\idq_1\in \spec R'$ be prime ideals such that $\idp_1\sse \idp_2$ and $\idq_1\cap R = \idp_1$. We now have the following commutative diagram
  \[
  \begin{tikzcd}
    R\ar[hookrightarrow]{rr}[above]{\pphi,~\text{integral}}\ar{d}[left]{\pi}&&
    R'\ar{d}[right]{\pi'}\\
    R/\idp_1\ar[hookrightarrow]{rr}[below]{\pphi',~\text{integral}}&&
    R'/\idq_1
  \end{tikzcd}
  \]
  where $\pphi'$ is an integral extension by \cref{7:integral-under-quotient-and-localization} and \cref{lec7:prime-quotient-of-integral-extension}.
  The ideal $\idp_2/\idp_1$ is prime in $R/{\idp_1}$ (\cref{lec1:prime-ideals-in-quotient}) and hence Lying Over (\cref{lec8:lying-over}) implies that there is a prime ideal $\idq_2'\in \spec R'/\idq_1$ such that $\idq_2'\cap R/\idp_1 = \idp_2/\idp_1$.
  \par
  Consider now the prime ideal $\idq_2 \defined \idq_2'\cap R'$. Then $\idq_1\sse \idq_2$ and
  \begin{align*}
    \idq_2 \cap R &= \left(\idq_2'\cap R'\right) \cap R\\
                  &= \left(\idp_2/\idp_1\right)\cap R\\
                  &= \idp_2.
  \end{align*}
\end{proof}
\begin{mrem}
There are also extension that are not integral but still satisfy going up: A trivial example is the embedding $\rationals\inarr \rr$ or more generaly any field extension that is not algebraic. \par
As another example, consider the embedding $\zz\inarr \zz[\polvariable]$. This is not integral (because by \cref{lec7:finite-alt-char} this would imply that $\zz[\polvariable]$ is a finite $\zz$-module). However, for every prime ideal $\idq\in \zz$ the image $\idp\zz[\polvariable]$ is prime too.
\end{mrem}
\subsection*{Going Down for Integral over Normal}
\begin{defn}
  Let $\pphi:R\to R'$ be an algebra and $I\sse R$ an ideal.
  \begin{enumerate}
    \item An element $b\in R'$ is \emph{integral over I}\index{integral! over an ideal} if there is a monic polynomial $p\in R[\polvariable]$ such that $p(b)=0$ and the non-leading coefficients of $p$ are in $I$.
    \item The set
    \[
    \overline{I} \defined
    \lset b\in R'\ssp b \text{ is integral over }I\rset .
    \]
    is the \emph{integral closure of $I$ in $R$}\index{integral closure! of an ideal}.
  \end{enumerate}
\end{defn}
\begin{mlem}\label{lec8:radical-of-integral-closure}
  Let $\pphi:R\to R'$ be an algebra and $I\sse R$ an ideal. Then an element $b\in R'$ is integral over $I$ if and only if there is a $n>0$ such that $b^n$ is integral over $I$.
\end{mlem}
\begin{lem}\label{lec8:integral-closure-of-ideal}
Let $\pphi: R\to R'$ be an algebra and $I\sse R$ an ideal. Consider the ideal $I\overline{R}\sse \overline{R}$. Then $\overline{I} = \sqrt{I\overline{R}}\sse \overline{R}$.
\end{lem}
\begin{proof}
  If $b\in \overline{I} \sse \overline{R}$ then there are $a_0,\hdots,a_{n-1}\in I$ such that
  \[
  b^n = a_{n-1}b^{n-1}+\hdots+a_0,
  \]
  so $b^n\in I\overline{R}$ and hence $b\in \sqrt{I\overline{R}}$.\par
  Let now $b\in I\overline{R}$ (this suffices, by \cref{lec8:radical-of-integral-closure}), so there are $a_i,\hdots,a_n\in I$ and $\overline{c}_1,\hdots,\overline{c}_n\in \overline{R}$ such that $b = a_1c_1+\hdots+a_nc_n$.
  Since the $c_i$ are integral over $R$, the module $M\defined R[c_1,\hdots,c_n]$ is finitely generated (by \cref{lec7:integral-alt-char}). Consider now the $R$-linear map
  \begin{eqnarray*}
    f_b:M&\longrightarrow&M\\
    x&\longmapsto xb^n.
  \end{eqnarray*}
  Then $\im f_b \sse IM$, and hence by Cayley-Hamilton (\cref{5:cayley-hamilton}) there is a monic polynomial $p\in R[\polvariable]$ with non-leading coefficients in $I$ such that $p(f_b)=0$.
  So in particular, $p(b^n)=0$, hence $b$ is integral over $I$ (again by \cref{lec8:radical-of-integral-closure}).
\end{proof}
\begin{defn}
  Let $R$ be an integral domain. We say $R$ is \emph{normal}\index{normal! ring} if $R$ is integrally closed in its quotient field.
\end{defn}
\begin{example}
  Every factorial ring is normal.
\end{example}
\begin{mlem}\label{lec8:normal-local}
  Being normal is a local property: For an integral domain, the following are equivalent:
  \begin{enumerate}
    \item $R$ is normal.
    \item $R_{\idp}$ is normal for all $\idp\in \spec R$.
    \item $R_{\idm}$ is normal for all $\idm \in \maxspec R$.
  \end{enumerate}
\end{mlem}
\begin{mlem}\label{lec8:integral-induce-algebraic-extension}
  Let $\pphi:R\inarr R'$ be an injective map, with $R,R'$ integral domains.
  \begin{enumerate}
    \item This induces a field extension $\quot R\inarr \quot R'$.
    \item If $R\inarr R'$ is integral, then $\quot R\inarr \quot R'$ is too,
    \item If $R\inarr R'$ is finite, then $\quot R\inarr \quot R'$ is too.
  \end{enumerate}
\end{mlem}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Since $\pphi$ is injective, every non-zero element in $R$ gets mapped to a unit in $\quot R'$. So by the univesal property of the localization, we get an induced map
    \[
    \begin{tikzcd}
      R\ar[hookrightarrow]{r}[above]{\pphi}\ar{d}&
      R'\ar{d}\\
      \quot R\ar[dashed]{r}&
      \quot R'
    \end{tikzcd}
    \]
    \item Localizing $R,R'$ (as modules) at $S\defined R\setminus \lset 0\rset$ gives an intgegral extension (\cref{7:integral-under-quotient-and-localization}) $\quot R = S^{-1}R\inarr S^{-1}R'$.
    By \cref{7:integral-domain-extension-field-corr}, we get that $S^{-1}R'$ is a field, and hence $S^{-1}R' = \quot R'$. So $\quot R \inarr \quot R'$ is algebraic.
    \item Localizing once again at $S\defined R\setminus \lset 0\rset$ gives a finite (localizing is exact) extension $\quot R = \sloc R\inarr \sloc R'$. Arguing as in ii), $\quot R' = \sloc R'$ follows, so $\sloc R'$ is a finite $\quot R$-vector space.
  \end{enumerate}
\end{proof}

\begin{lem}\label{lec8:coefficients-of-minimal-polynomial}
  Let $\pphi:R\inarr R'$ be an integral extension, with $R,R'$ integral domains and $R$ normal. Let $b\in R'$ be integral over some ideal $I\sse R$. Then $b/1$ is algebraic over $\quot R$ and the non-leading coefficients of its minimal polynomial are already in $\sqrt{I}$.
\end{lem}
\begin{proof}
  By \cref{lec8:integral-induce-algebraic-extension}, there is indeed a minimal polynomial for $b$.\par
  Set $K\defined \quot R$, and consider the intermediate field $K \sse K[b]\sse \quot R'$. Let $L$ be a field extension of $K[b]$ such that the minimal polynomial $p$ of $b$ is a product of linear factors in $L$ (e.g. the algebraic closure of $K[b]$).
  So $p$ has the form
  \[
  p = \left(\polvariable-x_1\right)\hdots \left(\polvariable-x_n\right),
  \]
  where the $x_1,\hdots,x_n$ are elements of $L$.\par
  As $b$ is integral over $R$, there is a monic polynomial $f\in R[\polvariable]\sse K[\polvariable]$ with $f(b)=0$.
  By the minimal property of the minimal polynomial, $p\divd f$ follows and hence all roots of $p$ are also roots of $f$ which shows that the $x_1,\hdots,x_n$ are integral over $I$.
  Since the non-leading coefficients of $p$ are in $K[x_1,\hdots,x_n]$ we get that they are integral over $I$ too (\cref{lec8:radical-of-integral-closure}). \par
  Since we assumed that $R$ is normal, we get that the $a_i$ are already in $R$. By applying \cref{lec8:radical-of-integral-closure} to $R'=K$, we get $\overline{I} = \sqrt{I}$, so $a_i\in \sqrt{I}$.
\end{proof}

\begin{lem}\label{lec8:prime-ideal-existence-preimage}
  Let $\pphi:R\to R'$ be a ring homomorphism and $\idp\in \spec R$ a prime ideal. Then the following are equivalent:
  \begin{enumerate}
    \item There is a prime ideal $\idq\in \spec R'$ such that $\idp = \pphi^{-1}(\idq)$.
    \item It holds that $\pphi^{-1}\left(\pphi(\idp)R'\right) = \idp$.
  \end{enumerate}
\end{lem}
\begin{proof}
  This will be on the 5th exercise sheet.
\end{proof}

\begin{theorem}[Going Down]\label{lec8:going-down-int-domains}
  Let $\pphi:R\to R'$ be an integral extension. Assume that $R,R'$ are integral domains and that $R$ is normal. Then $\pphi$ satisfies going down.
\end{theorem}
\lec
