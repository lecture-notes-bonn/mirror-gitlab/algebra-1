\section{Dedekind Rings}
\begin{defn}
  Let $R$ be a one-dimensional noetherian integral domain such that all localizations at prime ideals $R_{\idp}$ are discrete valuation rings. Then we say that $R$ is a \emph{Dedekind domain}\index{Dedekind domain}.
\end{defn}
\begin{mrem}
  By \cref{lec20:dvr-alt-char-ii}, this is well-defined.
\end{mrem}
\begin{rem}
  A one-dimensional noetherian integral domain is a Dedekind domain if and onyl if it is normal.
\end{rem}
\begin{proof}
  By \cref{lec8:normal-local}, being normal is a local property. By \cref{lec20:dvr-alt-char-ii}, a localization $R_{\idp}$ of $R$ is a discrete valuation ring if and only if it is normal.
\end{proof}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item The ring of integers $\zz$, and more generally, every principal ideal domain is a Dedekind domain.
    \item Let $X$ be a smooth curve. Then the coordinate ring $A(X)$ is a Dedekind domain.
  \end{enumerate}
\end{example}
\begin{mdefn}
  Let $\rationals\inarr K$ be a finite field extension. Then the integral closure of $\zz$ in $K$ is the \emph{ring of integers} and denoted by $\okk$.
\end{mdefn}
\begin{theorem}
  Let $\rationals\inarr K$ be a finite field extension. Then the ring of integers $\okk$ is a Dedekind domain.
\end{theorem}
\begin{proof}
  Since $\okk$ is a subring of $\rationals$, it is an integral domain. By definition of $\okk$, the extension $\zz\inarr \okk$ is integral and since $\dim \zz = 1$ it follows that $\dim \okk = 1$ (by \cref{lec11:dimension-int-ext}). Furthermore, every element $a\in \quot \okk$ is integral over $\zz$ and so by definition already in $\okk$.
  The difficult part is to show that $\okk$ is noetherian \textendash{} we first need two more claims:
  \begin{claim}\label{lec21:proof:quotient-finite}
    Let $m\in\zz$ be an integer. Then $\okk/m\okk$ has only finitely many elements.
  \end{claim}
  \begin{claimproof}
    Consider first the case $m=p$ for a prime number $p$. Now $R/pR$ is a $\fp$-vector space, since $\okk$ is a $\zz$-module. So it suffices to show $\dim_{\fp}\okk/p\okk\leq \dim_{\rationals}K$: Let $\overline{b_1},\hdots,\overline{b_n}\in \okk/p\okk$ be linearly independent. If there are $\lambda_1,\hdots,\lambda_n\in \rationals$ such that
    \[
    \lambda_1b_1+\hdots+\lambda_nb_n = 0,\]
    then (by factoring out the common denominator) we can assume that  $\lambda_1,\hdots,\lambda_n\in \zz$ and that not all $\lambda_i$ are divisible by $p$. But then in $\okk$, we get
    \[
    \overline{\lambda_1}\overline{b_1}+\hdots+\overline{\lambda_n}\overline{b_n} = 0,
    \]
    contradicting that $\overline{b_1},\hdots,\overline{b_n}$ are linearly independent. So each linearly independet subsest of $\okk/m\okk$ lifts to a linearly independet subset of $\okk\sse K$ and hence $\dim_{\fp} \okk/m\okk\leq \dim_{\rationals}\okk\leq \dim_{\rationals}K$.\par
    In the general case, note first that for any ring $R$ a short-exact sequence of $R$-modules
    \[
    \begin{tikzcd}[sep = small, cramped]
      0\ar{r}&
      M'\ar{r}&
      M\ar{r}&
      M''\ar{r}&
      0
    \end{tikzcd}
    \]
    the module $M$ is finite if and only if both $M'$ and $M''$ are finite. Furthermore, for a $\zz$-module $M$ and all $m_1,m_2\in \zz$ the sequence
    \[
    \begin{tikzcd}[sep = small, cramped]
      0\ar{r}&
      M/m_1\ar{r}&
      M/(m_1m_2)M\ar{r}&
      M/m_2M\ar{r}&
      0
    \end{tikzcd}
    \]
    is short-exact.\par
    So if $m = p_1^{k_1},\hdots,p_n^{k_n}$ for prime number $p_1,\hdots,p_n\in\zz$ then the above observations show that for each prime number, $\okk/p_i^{k_i}$ is finite and hence $\okk/(p_1^{n_1}\hdots p_n^{k_n})\okk$ is finite too.
  \end{claimproof}
  \begin{claim}\label{lec21:proof:element-in-z}
    Let $I\sse \okk$ be a non-zero ideal. Then there is a non-zero $m\in \zz$ such that $m\in I$.
  \end{claim}
  \begin{claimproof}
    Assume to the contrary that there is no such $m$, i.e. $I\cap \zz = \lset 0\rset$. Now the morphism $\zz/I\cap \zz \inarr \okk/I$ is integral which implies
    \[\dim \okk/I = \dim \zz/(I\cap\zz) = \dim \zz = 1.\]
    But since $\okk$ is an integral domain and $I\neq 0$, $\dim \okk/I<\dim\okk$ holds. This is a contradiction (we noted earlier that $\dim \okk = \dim \zz = 1$).
  \end{claimproof}
  We now show that $\okk$ is noetherian by showing that every ideal $I\sse \okk$ is finitely generated: Let $I\sse \okk$ be an ideal. By Claim 2, there is a $m\in I\cap \zz$.
  Now $I/\genby{m}$ is a submodule of $\okk/\genby{m}$, and by Claim 1, $I/\genby{m}$ is finite, so in particular finitely generated. By \cref{lec6:fg-ses}, this already implies that $I$ is finitely generated.
 \end{proof}

\begin{theorem}
  Let $R$ be a Dedekind domain.
  \begin{enumerate}
    \item Let $\idp\in \maxspec R$ be a maximal ideal and $I\sse R$ an ideal. Then $I$ is $\idp$-primary if and only if $I = \idp^k$ for a unique $k\geq 0$.
    \item Every ideal $I$ has a decomposition of the form
    \[
    I  = \idp_1^{k_1}\cdot\hdots\cdot\idp_n^{k_n}
    \]
    where $k_1,\hdots,k_n\geq 1$ and $\idp_1,\hdots,\idp_n = \ass(I)$. This decomposition is unique up to permutation.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item The direction \grqq$\impliedby$\grqq{} is true in any ring. For the other direction, we note that $IR_{\idp}\neq 0$ and that (by definition) $R_{\idp}$ is a discrete valuation ring.
    So by \cref{lec20:dvr-alt-char-ii} and \cref{lec19:dvr-power-in-maximal} there is a unique $k\geq 0$ such that $IR_{\idp} = \left(\idp R_{\idp}\right)^k = \idp^{k}R_{\idp}$.
    Now by \cref{lec18:primary-ideals-and-localization} $I = \idp^k$ follows.
    \item Since $R$ is noetherian, there is a minimal primary decomposition of $I$ such that $I = I_1\cap \hdots I_n$ and $\ass(I) = \lset \idp_1,\hdots,\idp_n\rset$. By part i), there are $k_i\geq 1$ such that $I_i = \idp_i^{k_1}$. Since the $\idp_i$ are maximal and coprime, we get
    \[
    \idp_1^{k_1}\cap \hdots \cap \idp_n^{k_n} = \idp_1^{k_1}\cdot \hdots \cdot \idp_n^{k_n}.
    \]
  \end{enumerate}
\end{proof}

\begin{lem}
  Let $R$ be a Dedekind domain.
  \begin{enumerate}
    \item For all collections $\idp_1,\hdots,\idp_n$ of maximal ideals and natural numbers $k_1,\hdots,k_n,l_1,\hdots,l_n$ it holds that
    \[
    \idp_1^{k_1}\cdot \hdots \cdot \idp_n^{k_n} \sse
    \idp_1^{l_1}\cdot \hdots \cdot \idp_n^{l_n}\text{ if and only if }
    l_i\leq k_i\text{ for all }i.
    \]
  \item Let $a\in R$ be non-zero. Then there is a decomposition of the form
  \[
  \genby{a} = \idp_1^{\nu_1(a)}\cdot \hdots \cdot \idp_n^{\nu_n(a)}
  \]
  where $\lset \idp_1,\hdots,\idp_n\rset = \ass(\genby{a})$ and $\nu_i:R_{\idp_i}\to \quot(R_{\idp_i})$ is the valuation on the localization.
  \end{enumerate}
\end{lem}
\begin{proof}
\leavevmode
  \begin{enumerate}
    \item We first need the following basic facts about localizations
      \begin{fact}\label{lec21:proof-loc-facts}
        Let $R$ be any ring (not necessarily noetherian, ...).
        \begin{enumerate}
        \item[a)] Let $I,J\sse R$ be ideals in an arbitrary ring $R$. Then $I\sse J$ if and only if $IR_{\idm}\sse JR_{\idm}$ for all $\idm\in \maxspec R$.
        \item[b)] Let $R\to R'$ be a ring homomorphism. Then \[(IJ)R' = (IR')(JR').\]
        %\item[c)] Let $\idp\neq \idq\in \maxspec R$. If $\idp\neq \idq$ then $R_{\idq}/{\idp_{\idq}} = 0$ (Here, $\idp_{\idq}\cong R_{\idq}\tensor_R\idp$ is the localization of $\idp$ as an $R$-module).
        \end{enumerate}
      \end{fact}
      Now
      \[
      \idp_1^{k_1}\cdot \hdots \cdot \idp_n^{k_n} \sse
      \idp_1^{l_1}\cdot \hdots \cdot \idp_n^{l_n}\]
      if and only if $\left(\idp_iR_{\idp_i}\right)^{k_i} \sse \left(\idp_iR_{\idp_i}\right)^{l_i}$ if and only if $k_i\leq l_i$ (since $R_{\idp_i}$ is a discrete valuation ring).
      The result now follows from \cref{lec21:proof-loc-facts} a) and b) (and the fact that $\idp_i R_{\idp_j} = R_{\idp_j}$ for $i\neq j$).
      \item Let
      \[
      \genby{a} = \idp_1^{\nu_1(a)}\cdot \hdots \cdot \idp_n^{\nu_n(a)}
      \]
      with $\lset \idp_1,\hdots,\idp_n\rset = \ass(\genby{a})$. Then
      \[
      \genby{a}R_{\idp_1} = \left(\idp_iR_{\idp_i}\right)^{k_i},
      \]
      which is exactly $\nu_i(a)$.
  \end{enumerate}
\end{proof}

\section{The Class Group}
\begin{defn}
  Let $R$ be an integral domain and set $K\defined \quot R$.
  \begin{enumerate}
    \item A \emph{fractional ideal}\index{ideal! fractional} is an $R$-submodule of $K$ such that there is a non-zero $a\in R$ with $aI\sse R$.
    \item Let $a_1,\hdots,a_n\in K$. We denote by
    \[
    \genby{a_1,\hdots,a_n}\defined Ra_1+\hdots+Ra_n
    \]
    the fractional ideal which is generated by $a_1,\hdots,a_n$. We say a fractional ideal $I$ is \emph{finitely generated} if there are $a_1,\hdots,a_n$ such that $I = \genby{a_1,\hdots,a_n}$. We say $I$ is a \emph{principal fractional ideal} if there is a $a\in K$ such that $I = \genby{a}$.
  \end{enumerate}
\end{defn}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item Every ideal of $R$ is a fractional ideal in $K$.
    \item The fractional ideal generated by some elements $a_1,\hdots,a_n$ is indeed a fractional ideal.
  \end{enumerate}
\end{example}

\begin{lem}
  Let $I$ be a fractional ideal such that $aI\sse R$ is finitely generated for an $a\in R$. Then $I$ is finitely generated as fractional ideal.
\end{lem}

\begin{defn}
  Let $I,J\sse R$ be submodules of $K$.
  \begin{enumerate}
    \item Set
    \[
    I\cdot J\defined \lset \sum_{i=1}^na_ib_i\ssp n\geq 0,a\in I,b\in J\rset,\]
    and
    \[
    I\divk J\defined \lset a\in K\ssp aJ\sse I\rset.
    \]
    \item We say an $R$-submodule $I\sse K$ is invertible if there is an $R$-submodule $J\sse K$ such that $I\cdot J = K$.
  \end{enumerate}
\end{defn}

\begin{lem}
  Let $I,J\sse R$ be submodules of $K$.
  \begin{enumerate}
    \item Assume $I\cdot J = R$. Then $J = R\divk I$.
    \item If $I = \genby{a}$ with $a\neq 0$ is a principal fractional ideal then $I$ is invertible.
    \item If $I$ is invertible then $I$ is a fractional ideal.
  \end{enumerate}
\end{lem}

\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Assume $R = IJ$. We now have
    \[
    J \sse R\divk I = \left(R\divk I\right)\left(IJ\right) \sse RJ = J
    \]
    and hence $R\divk I = J$.
    \item If $I = \genby{a}$ then $\genby{a}\cdot\genby{1/a} = R$.
    \item Let $I$ be invertible, i.e. $I\cdot \left(R\divk I\right) = R$, so there are $a_i\in I$ and $b_i \in R\divk I$ such that $\sum a_ib_i =1$. Let now $b\in I$ then $b = \sum a_i \left(b_i b\right)$ with $b_ib\in R$. Let $a$ be the product of the denominators of the $a_i$, then $ab\in R$ and hence $aI\sse R$.
    So $I$ is a fractional ideal.
  \end{enumerate}
\end{proof}
\lec
