\begin{lem}\label{lec15:noeth-alt-char}
  Let $M$ be an $R$-module.
  \begin{enumerate}
    \item The following are equivalent:
    \begin{enumerate}[label = \alph*)]
      \item $M$ is noetherian.
      \item Every non-empty family of submodules of $M$ has an inclusion-maximal element.
      \item Every submodule of $M$ is finitely generated.
    \end{enumerate}
    \item $M$ is artinian if and only if every non-empty family of $M$ has an inclusion-minimal element.
  \end{enumerate}
\end{lem}

\begin{proof}
\leavevmode
  \begin{enumerate}
    \item For a) $\implies$ b), let $(N_i)_i$ be a family of submodules of $M$. Then $(N_i)$ is partially ordered by inclusion, and since $M$ is noetherian, every chain of elements from $(N_i)$ has an upper bound (namely the subspace that terminates the chain). So by Zorn's Lemma, there is an inclusion-maximal subspace of $(N_i)$.\par
     For b) $\implies$ c), let $N$ be a submodule of $M$ and let $(N_i)$ be the family of finitely-generated submodules of $N$.
     Then $(N_i)$ is non-empty, since $\lset 0\rset \sse N$ is finitely-generated.
     So by b), there is an inclusion-maximal subspace $P\in (N_i)$. Assume that $P$ is a proper submodule of $N$, and let $P$ be generated by the elements $p_1,\hdots,p_k$.
     Then there is an element $p_{k+1}\in N\setminus P$. But now the subspace $\genby{p_1,\hdots,p_{k+1}}$ is a finitely-generated submodule of $N$ that has $P$ as a proper subset. This contradicts the maximality of $P$.\par
     Finally, for c) $\implies$ a), let $M_0\ssne M_1\ssne \hdots$ be an ascending chain of submodules of $M$. Consider the subspace $\tilde{M}\defined \cup M_i$. Then by assumption, $\tilde{M}$ is finitely generated. So there is a $i\geq 0$ such that the set of generators is in $M_k$ for all $k\geq i$. So $M_k = M_i$ for all $k\geq i$ follows, and hence the chain terminates.
     \item That $M$ being artinian implies that every non-empty family of subspaces has an inclusion-minimal element can be shown analogous to a) $\implies$ b) in i). For the other direction, note that every descending chain of submodules of $M$ has an inclusion-minimal element which necessarily terminates the chain.
  \end{enumerate}
\end{proof}
\begin{cor}\label{lec15:pid-noetherian}
  Every principal ideal domain is a noetherian ring.
\end{cor}

\begin{rem}
  Let $I\sse R$ be an ideal and $M$ an $R$-module. Then the quotient $M/IM$ is both an $R/I$-module and an $R$-module. Using the definition of a noetherian module, we get that $M/IM$ is noetherian as an $R$-module if and only if it is noetherian as an $R/I$-module (since statements about chains of submodules are independent from the ground ring, and $R$-submodules are precisley $R/I$-submodules).
\end{rem}

\begin{lem}\label{lec15:noetherian-ses}
  Let $M$ be an $R$-module and
  \[
  \begin{tikzcd}[sep = small, cramped]
    0\ar{r}&
    N\ar{r}&
    M\ar{r}&
    N'\ar{r}&
    0
  \end{tikzcd}
  \]
  be a short-exact sequence of $R$-modules.
  \begin{enumerate}
    \item $M$ is noetherian if and only if both $N$ and $N'$ are.
    \item $M$ is artinian if and only if both $N$ and $N'$ are.
  \end{enumerate}
\end{lem}
\begin{proof}
  By duality, it suffices to show i). Without loss of generality, we can assume that $N$ is a submodule of $M$ and that $N'=M/N$. Assume first that $M$ is noetherian. Let $N_0\sse N_1\sse N_2\sse \hdots$ be a chain of submodules of $M$. We can regard this as a chain in $N$ and since $M$ is noetherian, it terminates in $M$ at a module $N_n$. So the original chain in $N$ terminates in $N_n$ too.
  Analogously, let $P_0\sse P_1\sse \hdots$ be a chain in $M/N$ and denote by $q:M\surarr M/N$ the canonical projection to the quotient.
  Set $M_k\defined q^{-1}(P_k)$ for all $k$.
  Then $M_0\sse M_1\sse \hdots$ is an ascending chain in $M$, which terminates in a submodule $M_n$ since $M$ is noetherian. Then the original chain terminates in $P_n = q(M_n)$ (the equality holds since $q$ is surjective.)\par
  For the other direction, let $M_0\sse M_1\sse \hdots$ be an ascending chain in $M$. Set $N_k\defined M_k\cap N$ and $P_k \defined \left(M_k+N\right)/N$. So we get ascending chains $N_0\sse N_1\sse \hdots$ and $P_0\sse P_1\sse \hdots$ in $N$ and $M/N$ respectivley. Since $N$ and $M/N$ are noetherian, there is a $n>0$ such that $P_k=P_n$ and $N_k=N_n$ for all $k\geq n$.
  Then the original chain in $M$ terminates in $n$ too:\par
  Denote by $i:N\inarr M$ the inclusion of $N$ into $M$ and let $x\in M_k$ for a $k\geq n$.
  Then there is a $x'\in M_n$ such that $q(x) = q(x')$ (as the chain terminates in the quotient and $q$ is surjective).
  So $x-x'\in \ker q = \im i$ and hence there is a $y\in N$ such that $i(y) = x-x'$. This implies $y\in i^{-1}(M_k) = i^{-1}(M_n)$.
  Hence $x = i(y) + x'$, which implies $x\in M_n$, as $i(y)$ and $x'$ are.
\end{proof}

\begin{cor}
  Let $R$ be a noetherian ring.
  \begin{enumerate}
    \item Let $M,N$ be $R$-modules. Then $M\oplus N$ is noetherian if and only if $M,N$ are.
    \item Let $M$ be a finitely-generated $R$-module. Then $M$ is noetherian.
  \end{enumerate}
\end{cor}
\begin{proof}
\leavevmode
\begin{enumerate}
  \item This follows from the previous lemma, using the short-exact sequence $0\to M\to M\oplus N\to N\to 0$.
  \item By i), $R^n$ is noetherian for all $n>0$. Since $M$ is finitely-generated as $R$-module, it is isomorphic to a quotient of $R^n$ for a $n>0$. So by the previous lemma, $M$ is noetherian.
\end{enumerate}
\end{proof}

\begin{theorem}[Hilbert's Basissatz, HBS]\label{lec15:hbs}
  Let $R$ be a noetherian ring. Then the polynomial ring $R[\polvariable]$ is also a noetherian ring.
\end{theorem}

\begin{proof}
  We will do this by contradiction - assume $R[\polvariable]$ is not noetherian.
  So by \cref{lec15:noeth-alt-char} there is an ideal $I\sse R[\polvariable]$ which is not finitely generated.
  We can now inductivley choose elements $f_0,f_1,\hdots \in I$ which have the following properties:
  The polynomial $f_0\in I$ has minimal degree among all polynomials in $I$. We then choose $f_{n+1}$ as a polynomial of minimal degree in $I\setminus \genby{f_0,\hdots,f_n}$ for all $n>0$.\par
  In this way, we get an infinite sequence
  \[f_0,f_1,\hdots \in I \text{ such that } f_{n+1}\notin \genby{f_0,\hdots,f_n}.\]
  Set $d_n\defined \deg f_n$. Then, by construction, we have $d_{n+1}\geq d_n$.
  Let now $a_k$ be the leading coefficient of $f_k = a_kX^{d_k}+\left(\text{lower order terms in }X\right)$.
  This yields the ascending chain $\genby{a_0}\sse \genby{a_0,a_1}\sse \hdots$ in $R$.
  Now since $R$ is noetherian, this terminates for a $n$ and hence the leading coefficient of $f_{n+1}$, i.e. $a_{n+1}$, is of the form
  \[
  a_{n+1} = c_0a_0+\hdots+c_na_n,
  \]
  for some $c_0,\hdots,c_n\in R$.\par
  Consider now the polynomial
  \[
  f'_{n+1}\defined f_{n+1} - \sum_{k=0}^n c_k \polvariable^{d_{n+1}-d_k}f_k.
  \]
  Then the coefficient of $\polvariable^{d_{n+1}}$ is
  \[
  a_{n+1} - \sum_{k=0}^n c_ka_k = 0,
  \]
  by the above observation.
  So $f_{n+1}'$ is a polynomial with $\deg f_{n+1}'<\deg f_{n+1}$ and $f'_{n+1}\notin\genby{f_0,\hdots,f_{n}}$ (as otherwise $f_{n+1} = f_{n+1}'+\sum_{k=0}^nc_k\polvariable^{d_{n+1}-d_k}f_k$ would also be in $\genby{f_0,\hdots,f_n}$).
  But this is a contradiction to the minimality of $f_{n+1}$.
\end{proof}

\begin{cor}
  Let $R$ be a noetherian ring and $A$ an $R$-algebra of finite type. Then $A$ is a noetherian ring.
\end{cor}
\begin{proof}
  This is on exercise sheet 8.
\end{proof}
\begin{rem}
  \leavevmode
  \begin{enumerate}
    \item Let $X\sse \aspace_k^n$ be a variety. Then $A(X)$ is noetherian and hence every ideal $I\sse A(X)$ is finitely generated. So every subvariety of $X$ is already determined by finitely many polynomial equations.
    \item By the ascending chain condition for $A(X)$, we get that every chain of subvarities $X_0\supseteq X_1\supseteq\hdots$ terminates.
    \item Assume $X$ has infinitely many points $a_1,a_2,\hdots\in X\sse \aspace_k^n$. This gives an ascending chain of closed subsets, by setting $X_n\defined \bigcup_{k\leq n}\lset a_k\rset$. Then this chain corresponds to a descending chain of ideals in $A(X)$ which does not become stationary. So $A(X)$ cannot be artinian.
  \end{enumerate}
\end{rem}


\begin{prop}\label{lec15:artinian-alt-char}
  \leavevmode
  \begin{enumerate}
    \item If $R$ is an artinian ring, then $R$ has only finitely many maximal ideals and all prime ideals are maximal.
    \item For a ring $R$ the following are equivalent
    \begin{enumerate}[label= \alph*)]
      \item $R$ is artinian.
      \item $R$ is noetherian and every prime ideal is maximal.
    \end{enumerate}
  \end{enumerate}
\end{prop}
\begin{proof}
  This is on exercise sheet 8.
\end{proof}
\begin{prop}
  Let $R$ be a ring and $A$ an $R$-algebra of finite type which is integral over $R$. Then for $p\in \spec R$, there are only finitely many prime ideals in $A$ which lie over $p$. This means that the induced map $\spec A\to \spec R$ has finite fibre.
\end{prop}
\begin{proof}
  This in on exercise sheet 8.
\end{proof}

\lec
