




\begin{defn}
  Let $M$ be an $R$-module.
  \begin{enumerate}
    \item We say $M$ is \emph{locally finitely-generated} if for all prime ideals $\idp\in \spec R$ there is an element $f\in R\setminus \idp$ such that $M_f$ is a finitely-generated $R_f$-module.
    \item We say $M$ is \emph{locally finitely-presented} if for all prime ideals $\idp\in \spec R$ there is an element $f\in R\setminus \idp$ such that $M_f$ is a finitely-presented $R_f$-module.
    We say $M$ is \emph{locally free of finite rank} if for all prime ideals $\idp\in \spec R$ there is an element $f\in R\setminus \idp$ such that $M_f$ is a finitely-generated free $R_f$-module.
    \item We say $M$ is \emph{locally free of rank $n$} if for all prime ideals $\idp\in \spec R$ there is an element $f\in R\setminus \idp$ such that $M_f$ is isomorphic to $R_f^n$.
  \end{enumerate}
\end{defn}

\begin{theorem}
  Let $M$ be an $R$-module. Then the following are equivalent:
  \begin{enumerate}
    \item $M$ is finitely-generated and projective.
%    \item $M$ is finitely-presented and $M_{\idm}$ is a free $R_{\idm}$-module for all maximal ideals $\idm \in \maxspec R$.
    \item $M$ is locally-free of finite rank.
  \end{enumerate}
\end{theorem}

\begin{proof}
    i) $\implies$ ii): Let $\idp\in \spec R$ be a prime ideal. Then $M_{\idp}$ is a finitely-generated, projective $R_{\idp}$-module and hence (by \cref{lec22:projective-local-implies-free}) it is already a finite-free $R_{\idp}$-module. Let $x_1,\hdots,x_n$ be a basis of $M_{\idp}$ with $x_i = a_i/b_i$.
    Then $a_1/1,\hdots,a_n/1$ is also a basis of $M\ploc$. Consider now the $R$-linear map $\pphi:R^n\to M,~e_i\mapsto a_i$.
    Then $\pphi_{\idp}$ is an isomorphism and hence $\left(\coker \pphi\right)\ploc=\coker \left(\pphi\ploc\right) = 0$ which is equivalent to $\idp \notin \supp \left(\coker \pphi\right)$.
    Since $\coker \pphi$ is finitely-generated, we also have that $\supp \coker \pphi$ is a closed subset of $\spec R$ (by \cref{lec6:supp-props}). So there is an open neighbourhood $D(f)\sse \spec R$ of $\idp$ such that $D(f)\cap \supp \coker \pphi = \emptyset$, for an element $f\in R$.
    Now $D(f)\cong \spec R_f$ and since $R_f \tensor  \coker \pphi  = \coker \pphi_f$ we get that $\pphi_f$ is surjective.\par
    We now consider the kernel of $\pphi_f$. Since $M_f$ is still projective and the sequence
    \[
    \begin{tikzcd}[sep = small, cramped]
    0\ar{r}&
    \ker \pphi_f \ar{r}&
    R^n_f\ar{r}[above]{\pphi_f}&
    M_f\ar{r}&
    0
    \end{tikzcd}
    \]
    is exact we have $R^n_f \cong \ker \pphi_f \oplus M_f$. First of all, this implies that $\ker \pphi_f$ is a finitely-generated $R_f$-module. Furthermore, after localizing at $\idp$ we obtain $\left(\ker \pphi_f\right)_{\idp}\oplus M\ploc \cong R^n\ploc$ and hence $\left(\ker \pphi_f\right)\ploc = 0$ (since $M\ploc \cong R^n\ploc$).
    We proceed as we did for the cokernel above:
    We have $\idp \notin \supp_{R_f}\ker \pphi_f$ and since $\ker \pphi_f$ is finitely-generated this implies that there is an open neighborhood $D(f')\sse \spec R$ of $\idp$ such that $D(f')\cap \supp_{R_f}\ker \pphi_f = 0$. Set $a \defined ff'$.
    Then $D(a) = D(f)\cap D(f')$ and $R_a\tensor_{R_f}\ker \pphi_f =0$ and so $\pphi_f:R^n_f\to M_f$ is an isomorphism.\par \par
    For the other direction ii) $\implies$ i), we first note that ii) implies that $M\ploc$ is a free $R\ploc$-module for all prime ideals $\idp \in \spec R$ (since we can just localize further and localization is functorial).

\end{proof}

\begin{defn}
  Let $M$ be an $R$-module. We say $M$ is \emph{invertible}\index{module!invertible} if there is an $R$-module $N$ such that $M\tensor_RN\cong R$.
\end{defn}
\begin{rem}
  If $R$ is an integral domain, then the notions of invertible modules in the above sense and invertible $R$-modules in $\quot R$ do not necessarily coincide.
\end{rem}
\begin{lem}
  Let $M$ be an $R$-module.
  \begin{enumerate}
    \item If $M$ is invertible, then $M$ is already finitely-generated.
    \item If $R$ is a local ring and $M$ is invertible, then $M$ is free of rank 1.
  \end{enumerate}
\end{lem}
\begin{lem}
  Let $M$ be an $R$-module. The following are equivalent:
  \begin{enumerate}
    \item $M$ is invertible.
    \item $M$ is finitely generated and $M_{\idm}\cong R_{\idm}$ for all $\idm\in \maxspec R$.
    \item $R$ is locally free of rank 1.
  \end{enumerate}
  In this case, $M$ is already finitely-presented and for every $R$-module $N$ with $M\otimes_RN\cong R$ it already holds that $N\cong \hom(M,R)$.
\end{lem}
\begin{mdefn}
  The $R$-module $\hom(M,R)$ is the \emph{dual of $M$}\index{dual! of a module} and is also denoted by $M\dmod$.
\end{mdefn}
\begin{mlem}
  If $M$ is a finitely-presented $R$-module, $N$ any $R$-module and $S\sse R$ a multiplicative subset then there is an isomorphism \[\sloc\left(\hom_R(M,N)\right)\isomorphism\hom_{\sloc R}\left(\sloc M,\sloc N\right)\].
  So in particular, if $M$ is a finitely-presented $R$-module, then \[\left(\hom_R(M,R)\right)_{\idm}\cong \hom_{R_{\idm}}\left(M_{\idm},R_{\idm}\right)\]
  for all maximal ideals $\idm \in \maxspec R$.
\end{mlem}
\begin{proof}
  This is \cite[3.3.7]{weibel}.
\end{proof}
\begin{defn}
  Let $R$ be an integral domain and $M\sse \quot R$ a fractional ideal. Then $M$ is a \emph{local principal ideal} if $M_{\idm}$ is fractional principal ideal in $\quot R_{\idm}$ for all $\idm \in \maxspec R$.
\end{defn}
\begin{lem}
  Let $R$ be an integral domain and $M,N\sse \quot R$ fractional ideals.
  \begin{enumerate}
    \item There is a surjective linear map $\pi:M\tensor_R N\surarr M\cdot N$.
    \item If $M$ is a local prinicipal ideal, then $\pi$ is already an isomorphism.
  \end{enumerate}
\end{lem}
\begin{lem}
  Let $R$ be an integral domain and $M$ an invertible fractional ideal. Then $M$ is finitely generated.
\end{lem}

\begin{lem}
  Let $R$ be an integral domain and $M$ a fractional ideal. Then $M$ is invertible if and only if $M$ is a non-zero fractional principal ideal.
\end{lem}

\begin{lem}
  Let $R$ be an integral domain and $M$ a fractional ideal. Then the following are equivalent:
  \begin{enumerate}
    \item $M$ is an invertible fractional ideal.
    \item $M$ is finitely generated a locally principal ideal.
  \end{enumerate}
\end{lem}
\begin{theorem}
  Let $R$ be an intgral domain. Then the following are equivalent:
  \begin{enumerate}
    \item $M$ is an invertible fractional ideal.
    \item $M$ is an invertible module.
    \item $M$ is projective.
  \end{enumerate}
\end{theorem}


\begin{defn}
  Let $R$ be any ring. We define the \emph{Picard group} $\picg(R)$ of $R$\index{picard group} as the set of isomorphism classes of invertible $R$-modules, where the multiplication is given by the tensor product.
\end{defn}
\begin{lem}
  Let $R$ be an integral domain. Then every invertible module is isomorphic to a fractional ideal.
\end{lem}

\begin{theorem}
  If $R$ is a Dedekind Domain, then $\picg(R)$ is isomorphic to the class group $\clr(R)$.
\end{theorem}


\finalend
