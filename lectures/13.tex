\begin{lem}\label{lec13:vanishing-of-ideal-of-subset}
  Let $Y\sse \aspace_k^n$ be a subset. Then $V\left(\iop\left(Y\right)\right) = \overline{Y}$.
\end{lem}

\begin{proof}
  We know that $Y\sse V\left(\iop(Y)\right)$, by \cref{lec12:subvar-prop}. Since $V\left(\iop(Y)\right)$ is closed, we get $\overline{Y}\sse V\left(\iop(Y)\right)$.\par
  On the other hand, $\overline{Y}$ is closed. So by definition, there is an ideal $J\sse \ktn$ such that $\overline{Y} = V(J)$. Since $J\sse \iop(Y)$, we have
  \[
  \overline{Y} = V(J) \sse V\left(\iop(Y)\right).\]
\end{proof}


We now prove yet another Hilbert's Nullstellensatz:
\begin{theorem}[Hilbert's Nullstellensatz]\label{lec13:nullstellensatz}
  Let $J\sse \ktn$ be an ideal. Then $\iop\left(V\left(J\right)\right) = \sqrt{J}$ holds.
\end{theorem}

\begin{proof}
  We have
  \begin{align*}
  V(J) &= \lset a\in \aspace_k^n\ssp f(a) = 0\text{ for all }f\in J\rset\\
       &= \lset a\in \aspace_k^n\ssp J\sse \idm_a\defined \ker \left(\eval_a:\ktn\to k\right)\rset
    \shortintertext{and}
  \iop(V(J)) &= \lset f\in \ktn \ssp f(a)=0\text{ for all }a\in V(J)\rset.
  \shortintertext{
  Hence $f\in \iop(V(J))$ if and only if for all $a\in \aspace_k^n$ with $J\sse \idm_a$ it holds that $J\in \idm_a$, so}
  \iop(V(J))&= \bigcap_{\substack{a\in \aspace_k^n\\J\sse \idm_a}} \idm_a.
  \end{align*}
  By the Weak Nullstellensatz (\cref{lec11:weak-nullstellensatz}) we have
  \[
  \maxspec \ktn = \lset \idm_a\ssp a\in \aspace_k^n\rset,
  \]
  and hence
  \begin{align*}
    \iop(V(J)) &= \bigcap_{\substack{\idm\in \maxspec \ktn\\ J\sse \idm}}\idm\\
              &= \sqrt{J},
  \end{align*}
  as $\ktn$ is a Jacobson ring (\cref{lec11:ghns-for-fields}, and then \cref{lec10:jac-alt-char}).
\end{proof}

\section{The Zariski-Topology on $\aspace_k^n$}

\begin{nonumconvention}
  In the following, $k$ always denote an algebraically closed field.
\end{nonumconvention}

\begin{lem}
Let $I,J$ be ideals in $\ktn$ and $\lset I_l\rset_{l\in L}$ a family of ideals in $\ktn$.
  \begin{enumerate}
    \item It holds that $V\left(\sum_l I_l\right) = \bigcap_l V\left(I_l\right)$.
    \item It holds that $V\left(IJ\right) =  V\left(I\cap J\right) = V\left(I\right)\cup V\left(J\right)$.
  \end{enumerate}
\end{lem}

\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item We have $V\left(\sum_lI_l\right) \sse V(I_l)$ for all $l\in L$, by \cref{lec12:var-basic-props}. So $V\left(\sum_lI_l\right)\sse \bigcap_l V(I_l)$.\par
    Let now $x\in \bigcap_l V(I_l)$. Then for any polynomial $f\in \sum_lI_l$, $f(x) = 0$, so $x\in V\left(\sum_lI_l\right)$.
    \item We have $V(J)\supseteq V(I\cap J)\supseteq V(I)\cup V(J)$. Let $x\in V(IJ)$, with $x\notin V(I)$. Then there is a $f\in I$ such that $f(x)\neq 0$. Now for any $g\in J$, we have $fg\in IJ$ and hence $(fg)(x)=0$. So $g(x) = 0$ and thus $x\in V(J)$.
  \end{enumerate}
\end{proof}

\begin{prop}
  By the above lemma, the subsets of the form $V(I)\sse \aspace_k^n$ satisfy the axioms of closed sets of a topology on $\aspace_k^n$. This topology is the \emph{Zariski-Topology on $\aspace_k^n$}\index{Zariski Topology! on $\aspace_k^n$.}
\end{prop}

\begin{rem}
  The Zariski-Topology on $\aspace_k^n$ is quite weird:
  \begin{enumerate}
    \item Let $X\defined \lset x\in \aspace_{\cc}^1\ssp \abs{x}\leq 1\rset$ be the unit disk in $\cc$. As closed subsets of $\aspace_{\cc}^1$ are finite sets (\cref{lec12:zariski-closed-in-cc}), we get $\overline{X} = \aspace_{\cc}^1$.
    \item Let $\pphi:\aspace_{\cc}^1\to \aspace_{\cc}^1$ be bijective. Then preimages of finite sets are finite. So $\pphi$ is already continous.
  \end{enumerate}
\end{rem}
\begin{mlem}[Prime Avoidance]\label{lec13:prime-avoidance}
  Let $\idp,\idp_1,\hdots,\idp_m\in \spec R$ be prime ideals and $I,I_1,\hdots,I_n\sse R$ ideals.
  \begin{enumerate}
    \item If $I \sse \bigcap_{i=1}^m \idp_i$, then there exists an $1\leq i \leq m$ such that $I\sse \idp_i$.
    \item If $\bigcap_{i=1}^nI_i \sse \idp$, then there is a $1\leq i\leq n$ such that $I_i\sse \idp$.
    \item If $\bigcap_{i=1}^nI_i = \idp$, then there is a $1\leq i\leq n$ such that $I_i = \idp$.
    \item Parts ii) also holds if one of the ideals is not prime.
  \end{enumerate}
\end{mlem}
\begin{proof}
  This is on the seventh exercise sheet. (Part iv) actually not).
\end{proof}
\begin{mdefn}
  Let $I\sse R$ be an ideal. We say $I$ is a r\emph{adical ideal} if $\sqrt{I} = I$ holds\index{ideal!radical}.
\end{mdefn}
\begin{mdefn}
  A topological space $X$ is called \emph{irreducible} if every decomposition $X = X_1\cup X_2$ in closed subsets $X_1,X_2$ implies that  $X_1$ or $X_2$ equals $X$.
\end{mdefn}
\begin{theorem}
  \leavevmode
  \begin{enumerate}
    \item The maps
    \begin{eqnarray*}
      \lset \text{varieties in }\aspace_k^n\rset & \longleftrightarrow & \lset \text{radical ideals in }\ktn\rset\\
      Y & \longmapsto & \iop(Y)\\
      V(J) & \longmapsfrom & J
    \end{eqnarray*}
    are mutually inverse bijections.
    \item The bijection from i) restricts to a bijection
    \begin{eqnarray*}
      \lset \text{closed, irreducible subsets of }\aspace_k^n \rset& \longleftrightarrow & \spec \ktn.
    \end{eqnarray*}
    \item The bijection from i) restricts to a bijection
    \begin{eqnarray*}
      \lset \{x\} \sse \aspace_k^n\rset & \longleftrightarrow & \maxspec \ktn.
    \end{eqnarray*}
  \end{enumerate}
\end{theorem}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Since $k$ is an integral domain, we have $\sqrt{\iop(X)} = \iop(X)$ for all $X\sse \aspace_k^n$. If $X\sse \aspace_k^n$ is a variety then by \cref{lec13:vanishing-of-ideal-of-subset}, we have $V(\iop(X)) = \overline{X}$.
    Since the closed sets in $\aspace_k^n$ are precisley the varieties, we get $V(\iop(X)) = X$.\par
    Let $J\sse \ktn$ be a radical ideal. By the Nullstellensatz (\cref{lec13:nullstellensatz}), we have $\iop(V(J)) = \sqrt{J} = J$.
    \item Let $Y\sse \aspace_k^n$ be closed and irreducible. We want to show that $\iop(Y)$ is a prime ideal. We do this by contradiction:\par
    Assume there are polynomials $f,g\in \ktn \setminus \iop(Y)$ such that $fg\in \iop(Y)$.
    Now $V\left(\iop(Y)+\genby{f}\right),V\left(\iop(Y)+\genby{g}\right)\sse Y$ are proper subsets of $Y$, because if $V\left(\iop(Y)+\genby{f}\right) = Y$, then $f(y) = 0$ would hold for every $y\in Y$ which would mean $f\in \iop(Y)$.\par
    As $Y$ is irreducible it holds that
    \[
    V\left(\iop(Y)+\genby{f}\right) \cup V\left(\iop(Y)+\genby{g}\right)\ssne Y
    \]
    By \cref{lec12:subvar-prop}, we have
    \[
    V\left(\iop(Y)+\genby{f}\right) \cup V\left(\iop(Y)+\genby{g}\right) =
    V\left(\left(\iop(Y)+\genby{f}\right)\cdot \left(\iop(Y)+\genby{g}\right)\right)
    \]
    We also have
    \begin{align*}
    \left(\iop(Y)+\genby{f}\right)\cdot \left(\iop(Y)+\genby{g}\right)
    &\sse \iop(Y) + \genby{fg}\\
    &= \iop(Y),
    \end{align*}
    as we assume $fg\in \iop(Y)$. Putting all of this together, we get
    \begin{align*}
    Y &= V\left(\iop(Y)\right)\\
      &= V\left(\iop(Y)+\genby{fg}\right)\\
      &\sse V\left(\iop(Y)+\genby{f}\right)\cap V\left(\iop(Y)+\genby{g}\right)\\
      &\ssne Y,
    \end{align*}
    which is not possible. This shows that the $\iop(Y)$ is indeed a prime ideal.\par
    For the other map, let $\idp\in \spec \ktn$ be a prime ideal and assume there is a decomposition $V(\idp) = V(I_1)\cup V(I_2) = V(I_1I_2)$ for some ideals $I_1,I_2\sse \ktn$.
    Using part i), we get
    \[
      \idp=\sqrt{\idp} = \sqrt{I_1I_2}\supseteq I_1\cap I_2,
    \]
    which implies $I_1\sse \idp$ or $I_2\sse \idp$ (by Prime Avoidance, \cref{lec13:prime-avoidance}, ii)).
    Thus $V(\idp)\sse V(I_1)$ or $V(\idp) \sse V(I_2)$ and so $V(\idp)$ is indeed irreducible.
    \item Let $\idm\in \maxspec \ktn$ be a maximal ideal. Then by the Weak Nullstellensatz (\cref{lec11:weak-nullstellensatz}) it is of the form $\idm = \idm_a$ for an $a\in \aspace_k^n$. We now have $V(\idm_a) = \lset a\rset$.\par
    For the other direction, let $a\in \aspace_k^n$ be a point. We then have
    \[
    \iop\left(\lset a \rset \right) =
    \lset f\in \ktn \ssp f(a)=0\rset = \idm_a.
    \]
  \end{enumerate}
\end{proof}

\begin{cor}
  Let $X\sse \aspace_k^n$ be a variety.
  \begin{enumerate}
    \item The maps
    \begin{eqnarray*}
      \lset \text{closed subsets of } X\rset & \longleftrightarrow & \lset \text{radical ideals in }A(X)\rset\\
      Y & \longmapsto & \iop_X(Y)\\
      V_X(J) & \longmapsfrom & J
    \end{eqnarray*}
    are mutually inverse bijections.
    \item The bijection from i) restricts to a bijection
    \begin{eqnarray*}
      \lset \text{closed, irreducible subsets of }X \rset& \longleftrightarrow & \spec A(X).
    \end{eqnarray*}
    \item The bijection from i) restricts to a bijection
    \begin{eqnarray*}
      \lset \{x\} \sse X\rset & \longleftrightarrow & \maxspec A(X).
    \end{eqnarray*}
  \end{enumerate}
\end{cor}

\begin{defn}
  Let $X$ be a topological space. A closed, irreducible subset $C\sse X$ is an \emph{irreducible component}\index{irreducible!component} if for all closed subsets $Z\sse X$ with $C\sse Z$ already $C=Z$ follows.
\end{defn}

\begin{cor}
  Let $X\sse \aspace_k^n$ be a variety. Then the maps
  \begin{eqnarray*}
    \lset \text{irreducible components of } X\rset & \longleftrightarrow & \lset \text{minimal prime ideals in }A(X)\rset\\
    Y & \longmapsto & \iop_X(Y)\\
    V_X(J) & \longmapsfrom & J
  \end{eqnarray*}
  are mutually inverse bijections.
\end{cor}

\lec
